<?php 
namespace app\helpers;

class SearchHelper
{

	public static function searchString($models,$attrib,$searchString)
	{
		$pattern = preg_quote($searchString, '/');
	    // finalise the regular expression, matching the whole line
	    $pattern = "/^.*$pattern.*\$/m";
	    $allItems = [];
	    // search, and store all matching occurences in $matches
	    foreach ($models as $modelKey => $model) {
	    	if(preg_match_all($pattern, $model->$attrib, $matches))
		    {
		    	$allItems[$modelKey] = $model;
		    }	
	    }
	    return $allItems;
	}
}
