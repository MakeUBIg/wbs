<?php

namespace app\helpers;
use app\modules\MubAdmin\modules\csvreader\models\Product;

class StringHelper
{
	public static function generateSlug($string)
	{
		$string = str_replace(' ', '-', $string);
		$data = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		return strtolower($data);
	}

	public function getFistLastName($name)
	{

		$clientName	= explode(' ',$name);
		$response = [];
		if(count($clientName) > 1)
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[1];
		}
		else 
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[0];
		}
		return $response;
	}

	public static function getAuthorSlug($allAuthors)
	{	
		$authorLastName = [];

		foreach($allAuthors as $lastName)
		{
			if($lastName != "")
			{
				$lastName = strtolower($lastName);

				if(preg_match('(phd|md|m.d.|group|ed.|ed|dr|club|jr.|jr|pharm.d|m d|phd.|ph d)', $lastName) === 1)
				{	

					$authorLastName[] = self::generateSlug($lastName);
				}
				else
				{
					$authorsLastName = explode(' ', $lastName);
					$authorLastName[] = end($authorsLastName);
				}
			}
			else
			{	
				$authorLastName[] = "na";
			}
		}
		return $authorLastName;
	}

	public static function getIdentifierType(String $identifierType)
	{
		$identifier['01'] = 'Proprietary';
		$identifier['02'] = 'isbn_10';
		$identifier['03'] = 'ean_13';
		$identifier['04'] = 'upc';
		$identifier['05'] = 'ISMN-10';
		$identifier['06'] = 'DOI';
		$identifier['13'] = 'LCCN';
		$identifier['14'] = 'gtin_14';
		$identifier['15'] = 'isbn_13';
		$identifier['17'] = 'Legal deposit number';
		$identifier['22'] = 'URN';
		$identifier['23'] = 'OCLC number';
		$identifier['24'] = 'Co-publisher’s ISBN-13';
		$identifier['25'] = 'ISMN-13';
		$identifier['26'] = 'ISBN-A';
		$identifier['27'] = 'JP e-code';
		$identifier['28'] = 'OLCC number';
		$identifier['29'] = 'JP Magazine ID';
		$identifier['30'] = 'UPC12+5';
		$identifier['31'] = 'BNF Control number';
		$identifier['35'] = 'ARK';

		return $identifier[$identifierType];
	}

	public static function getProductIdentifier($productIdentifier)
	{
		$product = new Product();
		$record = null;
		foreach($productIdentifier as $value){

			$column = self::getIdentifierType((String) $value->b221);
			$productValue = (String) $value->b244;
			$record = $product::find()->where([$column => $productValue,'del_status' => '0'])->one();
			//p($record->createCommand()->rawSql);
			if(!empty($record))
			{
				//print_r($record->createCommand()->rawSql);
				$product = $record;
				break;
			}
			$product->{$column} = (String)$value->b244;
		}
		return $product;
	}

	public static function getAuthorRole($roleCode)
	{	
		
	}

	public static function getContributorKeyName($keyName)
	{	
		$formatAuthor = [
			'NA'   => 'Undefined',
			'b034' => 'Sequence Number',
			'b035' => 'Contributor Role',
			'b252' => 'Language Code',
			'b340' => 'Sequence Number Within Role',
			'b036' => 'Person Name',
			'b037' => 'Person Name Inverted',
			'b038' => 'Title Before Names',
			'b039' => 'Names Before Key',
			'b247' => 'Prefix ToKey',
			'b040' => 'Key Names',
			'b041' => 'Names After Key',
			'b248' => 'Suffix ToKey',
			'b042' => 'Letters After Names',
			'b043' => 'Titles After Names',
			'b390' => 'Person NameID Type',
			'b233' => 'ID Type Name',
			'b244' => 'ID Value',
			'b250' => 'Person Name Type',
			'b036' => 'Person Name',
			'b037' => 'Person Name Inverted',
			'b038' => 'Title Before Names',
			'b039' => 'Names Before Key',
			'b247' => 'Prefix ToKey',
			'b040' => 'Key Names',
			'b041' => 'Names After Key',
			'b248' => 'Suffix To Key',
			'b042' => 'Letters After Names',
			'b043' => 'Titles After Names',
			'b390' => 'Person NameID Type',
			'b233' => 'ID Type Name',
			'b244' => 'ID Value',
			'b387' => 'Person Date Role',
			'j260' => 'Date Format',
			'b306' => 'Date',
			'b045' => 'Professional Position',
			'b046' => 'Affiliation',
			'b047' => 'Corporate Name',
			'b044' => 'Biographical Note',
			'b367' => 'Website Role',
			'b294' => 'Website Description',
			'b295' => 'Website Link',
			'b048' => 'Contributor Description',
			'b249' => 'Unnamed Persons',
			'b049' => 'Contributor Statement',
			'b398' => 'Region Code',
			'n339' => 'No Contributor'
		];

		if(isset($formatAuthor[$keyName]))
		{
			return $formatAuthor[$keyName];
		}
		return $formatAuthor['NA'];
	}

	public function getContributorValue($value)
	{
		$contributorValue = [
		'NA'  => $value,
		'A01' => 'By (author)',
		'A02' => 'With',
		'A03' => 'Screenplay by',
		'A04' => 'Libretto by',
		'A05' => 'Lyrics by',
		'A06' => 'By (composer)',
		'A07' => 'By (artist)',
		'A08' => 'By (photographer)',
		'A09' => 'Created by',
		'A10' => 'From an idea by',
		'A11' => 'Designed by',
		'A12' => 'Illustrated by',
		'A13' => 'Photographs by',
		'A14' => 'Text by',
		'A15' => 'Preface by',
		'A16' => 'Prologue by',
		'A17' => 'Summary by',
		'A18' => 'Supplement by',
		'A19' => 'Afterword by',
		'A20' => 'Notes by',
		'A21' => 'Commentaries by',
		'A22' => 'Epilogue by',
		'A23' => 'Foreword by',
		'A24' => 'Introduction by',
		'A25' => 'Footnotes by',
		'A26' => 'Memoir by',
		'A27' => 'Experiments by',
		'A29' => 'Introduction and notes by',
		'A30' => 'Software written by',
		'A31' => 'Book and lyrics by',
		'A32' => 'Contributions by',
		'A33' => 'Appendix by',
		'A34' => 'Index by',
		'A35' => 'Drawings by',
		'A36' => 'Cover design or artwork by',
		'A37' => 'Preliminary work by',
		'A38' => 'Original author',
		'A39' => 'Maps by',
		'A40' => 'Inked or colored by',
		'A41' => 'Paper engineering by',
		'A42' => 'Continued by',
		'A43' => 'Interviewer',
		'A44' => 'Interviewee',
		'A45' => 'Comic script by',
		'A46' => 'Inker',
		'A47' => 'Colorist',
		'A48' => 'Letterer',
		'A51' => 'Research by',
		'A99' => 'Other primary creator',
		'B01' => 'Edited by',
		'B02' => 'Revised by',
		'B03' => 'Retold by',
		'B04' => 'Abridged by',
		'B05' => 'Adapted by',
		'B06' => 'Translated by',
		'B07' => 'As told by',
		'B08' => 'Translated with commentary by',
		'B09' => 'Series edited by',
		'B10' => 'Edited and translated by',
		'B11' => 'Editor-in-chief',
		'B12' => 'Guest editor',
		'B13' => 'Volume editor',
		'B14' => 'Editorial board member',
		'B15' => 'Editorial coordination by',
		'B16' => 'Managing editor',
		'B17' => 'Founded by',
		'B18' => 'Prepared for publication by',
		'B19' => 'Associate editor',
		'B20' => 'Consultant editor',
		'B21' => 'General editor',
		'B22' => 'Dramatized by',
		'B23' => 'General rapporteur',
		'B24' => 'Literary editor',
		'B25' => 'Arranged by (music)',
		'B26' => 'Technical editor',
		'B27' => 'Thesis advisor or supervisor',
		'B28' => 'Thesis examiner',
		'B29' => 'Scientific editor',
		'B30' => 'Historical advisor',
		'B31' => 'Original editor',
		'B99' => 'Other adaptation by',
		'C01' => 'Compiled by',
		'C02' => 'Selected by',
		'C03' => 'Non-text material selected by',
		'C04' => 'Curated by',
		'C99' => 'Other compilation by',
		'D01' => 'Producer',
		'D02' => 'Director',
		'D03' => 'Conductor',
		'D04' => 'Choreographer',
		'D99' => 'Other direction by',
		'E01' => 'Actor',
		'E02' => 'Dancer',
		'E03' => 'Narrator',
		'E04' => 'Commentator',
		'E05' => 'Vocal soloist',
		'E06' => 'Instrumental soloist',
		'E07' => 'Read by',
		'E08' => 'Performed by (orchestra, band, ensemble)',
		'E09' => 'Speaker',
		'E10' => 'Presenter',
		'E99' => 'Performed by',
		'F01' => 'Filmed/photographed by',
		'F02' => 'Editor (film or video)',
		'F99' => 'Other recording by',
		'Z01' => 'Assisted by',
		'Z02' => 'Honored/dedicated to',
		'Z98' => '(Various roles)',
		'Z99' => 'Other'
		];
		if(isset($contributorValue[$value]))
		{
			return $contributorValue[$value];
		}
		return $contributorValue['NA'];
	}

	public static function getFormatValue($format)
	{

		$formatArray = [];
		$formatArray['00']="Undefined";
		$formatArray['AA']="Audio";
		$formatArray['AB']="Audio cassette";
		$formatArray['AC']="CD-Audio";
		$formatArray['AD']="DAT";
		$formatArray['AE']="Audio disc";
		$formatArray['AF']="Audio tape";
		$formatArray['AG']="MiniDisc";
		$formatArray['AH']="CD-Extra";
		$formatArray['AI']="DVD Audio";
		$formatArray['AJ']="Downloadable audio";
		$formatArray['AK']="Pre-recorded digital audio player";
		$formatArray['AL']="Pre-recorded SD card";
		$formatArray['AZ']="Other audio format";
		$formatArray['BA']="Book";
		$formatArray['BB']="Hardback";
		$formatArray['BC']="Paperback / softback";
		$formatArray['BD']="Loose-leaf";
		$formatArray['BE']="Spiral bound";
		$formatArray['BF']="Pamphlet";
		$formatArray['BG']="Leather / fine binding";
		$formatArray['BH']="Board book";
		$formatArray['BI']="Rag book";
		$formatArray['BJ']="Bath book";
		$formatArray['BK']="Novelty book";
		$formatArray['BL']="Slide bound";
		$formatArray['BM']="Big book";
		$formatArray['BN']="Part-work (fascículo)";
		$formatArray['BO']="Fold-out book or chart";
		$formatArray['BP']="Foam book";
		$formatArray['BZ']="Other book format";
		$formatArray['CA']="Sheet map";
		$formatArray['CB']="Sheet map, folded";
		$formatArray['CC']="Sheet map, flat";
		$formatArray['CD']="Sheet map, rolled";
		$formatArray['CE']="Globe";
		$formatArray['CZ']="Other cartographic";
		$formatArray['DA']="Digital";
		$formatArray['DB']="CD-ROM";
		$formatArray['DC']="CD-I";
		$formatArray['DD']="DVD";
		$formatArray['DE']="Game cartridge";
		$formatArray['DF']="Diskette";
		$formatArray['DG']="Electronic book text";
		$formatArray['DH']="Online resource";
		$formatArray['DI']="DVD-ROM";
		$formatArray['DJ']="Secure Digital (SD) Memory Card";
		$formatArray['DK']="Compact Flash Memory Card";
		$formatArray['DL']="Memory Stick Memory Card";
		$formatArray['DM']="USB Flash Drive";
		$formatArray['DN']="Double-sided CD/DVD";
		$formatArray['DZ']="Other digital";
		$formatArray['FA']="Film or transparency";
		$formatArray['FB']="Film";
		$formatArray['FC']="Slides";
		$formatArray['FD']="OHP transparencies";
		$formatArray['FE']="Filmstrip";
		$formatArray['FF']="Film";
		$formatArray['FZ']="Other film or transparency format";
		$formatArray['MA']="Microform";
		$formatArray['MB']="Microfiche";
		$formatArray['MC']="Microfilm";
		$formatArray['MZ']="Other microform";
		$formatArray['PA']="Miscellaneous print";
		$formatArray['PB']="Address book";
		$formatArray['PC']="Calendar";
		$formatArray['PD']="Cards";
		$formatArray['PE']="Copymasters";
		$formatArray['PF']="Diary";
		$formatArray['PG']="Frieze";
		$formatArray['PH']="Kit";
		$formatArray['PI']="Sheet music";
		$formatArray['PJ']="Postcard book or pack";
		$formatArray['PK']="Poster";
		$formatArray['PL']="Record book";
		$formatArray['PM']="Wallet or folde";
		$formatArray['PN']="Pictures or photographs";
		$formatArray['PO']="Wallchart";
		$formatArray['PP']="Stickers";
		$formatArray['PQ']="Plate (lámina)";
		$formatArray['PR']="Notebook / blank book";
		$formatArray['PS']="Organizer";
		$formatArray['PT']="Bookmark";
		$formatArray['PZ']="Other printed item";
		$formatArray['VA']="Video";
		$formatArray['VB']="Video, VHS, PAL";
		$formatArray['VC']="Video, VHS, NTSC";
		$formatArray['VD']="Video, Betamax, PAL";
		$formatArray['VE']="Video, Betamax, NTSC";
		$formatArray['VF']="Videodisc";
		$formatArray['VG']="Video, VHS, SECAM";
		$formatArray['VH']="Video, Betamax, SECAM";
		$formatArray['VI']="DVD video";
		$formatArray['VJ']="VHS video";
		$formatArray['VK']="Betamax video";
		$formatArray['VL']="VCD";
		$formatArray['VM']="SVCD";
		$formatArray['VN']="HD DVD";
		$formatArray['VO']="Blu-ray";
		$formatArray['VP']="UMD Video";
		$formatArray['VZ']="Other video format";
		$formatArray['WW']="Mixed media product";
		$formatArray['WX']="Multiple copy pack";
		$formatArray['XA']="Trade-only material";
		$formatArray['XB']="Dumpbin – empty";
		$formatArray['XC']="Dumpbin – filled";
		$formatArray['XD']="Counterpack – empty";
		$formatArray['XE']="Counterpack – filled";
		$formatArray['XF']="Poster, promotional";
		$formatArray['XG']="Shelf strip";
		$formatArray['XH']="Window piece";
		$formatArray['XI']="Streamer";
		$formatArray['XJ']="Spinner";
		$formatArray['XK']="Large book display";
		$formatArray['XL']="Shrink-wrapped pack";
		$formatArray['XZ']="Other point of sale";
		$formatArray['ZA']="General merchandise";
		$formatArray['ZB']="Doll";
		$formatArray['ZC']="Soft toy";
		$formatArray['ZD']="Toy";
		$formatArray['ZE']="Game";
		$formatArray['ZF']="T-shirt";
		$formatArray['ZZ']="Other merchandise";
		$formatArray['NA']="NA";

		if(isset($formatArray[$format]))
		{

		return $formatArray[$format];
		}
		return $formatArray['NA'];
	}

}