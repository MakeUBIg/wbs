<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $name
 * @property string $designation
 * @property string $department
 * @property string $organization
 * @property string $address
 * @property string $city
 * @property integer $pin_code
 * @property string $state
 * @property string $country
 * @property integer $landline
 * @property string $email
 * @property string $website
 * @property integer $mobile
 * @property string $message
 * @property string $alt_address
 * @property string $lat
 * @property string $long
 * @property string $booking_item
 * @property integer $amount
 * @property string $time
 * @property string $payment_mode
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'pin_code', 'landline', 'mobile', 'amount'], 'integer'],
            [['address', 'city', 'pin_code', 'email', 'mobile'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'designation', 'department', 'organization', 'address', 'city', 'state', 'country', 'website', 'message', 'alt_address', 'booking_item','quantity','time', 'payment_mode'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['lat', 'long'], 'string', 'max' => 100],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'department' => 'Department',
            'organization' => 'Organization',
            'address' => 'Address',
            'city' => 'City',
            'pin_code' => 'Pin Code',
            'state' => 'State',
            'country' => 'Country',
            'landline' => 'Landline',
            'email' => 'Email',
            'website' => 'Website',
            'mobile' => 'Mobile',
            'message' => 'Message',
            'alt_address' => 'Alt Address',
            'lat' => 'Lat',
            'long' => 'Long',
            'booking_item' => 'Booking Item',
            'quantity' => 'quantity',
            'amount' => 'Amount',
            'time' => 'Time',
            'payment_mode' => 'Payment Mode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
