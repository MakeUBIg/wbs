<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_elements".
 *
 * @property integer $id
 * @property integer $element_id
 * @property integer $page_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubElement $element
 * @property MubUserPage $page
 */
class PageElements extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_elements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_id', 'page_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubElement::className(), 'targetAttribute' => ['element_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'element_id' => Yii::t('app', 'Element ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement()
    {
        return $this->hasOne(MubElement::className(), ['id' => 'element_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MubUserPage::className(), ['id' => 'page_id'])->where(['del_status' => '0']);
    }
}
