<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form about `app\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public $username;

    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'post_date', 'post_parent', 'order', 'comment_count', 'approved_by'], 'integer'],
            [['post_title', 'post_excerpt', 'status', 'url','username', 'post_name', 'post_brief', 'pinned', 'post_type', 'approved_on', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $role = $this->getAuthRole();
        
        $query = Post::find();

        if($role == 'subadmin')
        {
            $mubUserId = \app\models\User::getMubUserId();
            $query = Post::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0'])->orderBy(['id' => SORT_DESC]);
        }
        else 
        {
             $query = Post::find()->where(['del_status' => '0'])->orderBy(['id' => SORT_DESC]);
        }        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'post_date' => $this->post_date,
            'post_parent' => $this->post_parent,
            'order' => $this->order,
            'comment_count' => $this->comment_count,
            'approved_by' => $this->approved_by,
            'approved_on' => $this->approved_on,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'username' => $this->username,
        ]);

        $query->andFilterWhere(['like', 'post_title', $this->post_title])
            ->andFilterWhere(['like', 'post_excerpt', $this->post_excerpt])
            ->andFilterWhere(['=', 'post.status', $this->status])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'post_name', $this->post_name])
            ->andFilterWhere(['like', 'post_brief', $this->post_brief])
            ->andFilterWhere(['like', 'mub_user.username', $this->username])
            ->andFilterWhere(['like', 'pinned', $this->pinned])
            ->andFilterWhere(['like', 'post_type', $this->post_type])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
