<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $email
 * @property string $name
 * @property integer $comment_id
 * @property string $title
 * @property string $type
 * @property string $comment_text
 * @property integer $mub_user_id
 * @property string $status
 * @property string $approved_on
 * @property integer $approved_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $approvedBy
 * @property PostComment $comment
 * @property PostComment[] $postComments
 * @property MubUser $mubUser
 * @property Post $post
 */
class PostComment extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'comment_id', 'mub_user_id', 'approved_by'], 'integer'],
            [['email', 'name','comment_text'], 'required'],
            [['email'],'email'],
            [['read','type', 'comment_text', 'status', 'del_status'], 'string'],
            [['approved_on', 'created_at', 'updated_at'], 'safe'],
            [['email', 'name', 'title'], 'string', 'max' => 255],
            [['approved_by'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['approved_by' => 'id']],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => PostComment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_comment'] = ['email', 'name','comment_text'];
        $scenarios['update_comment'] = ['comment_text'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
            'post_id' => Yii::t('app', 'Post ID'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'comment_id' => Yii::t('app', 'Comment ID'),
            'read' => Yii::t('app', 'Read'),
            'type' => Yii::t('app', 'Type'),
            'comment_text' => Yii::t('app', 'Comment Text'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'status' => Yii::t('app', 'Status'),
            'approved_on' => Yii::t('app', 'Approved On'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'approved_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(PostComment::className(), ['id' => 'comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostComments()
    {
        return $this->hasMany(PostComment::className(), ['comment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
