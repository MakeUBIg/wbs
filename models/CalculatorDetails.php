<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calculator_details".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $saving
 * @property string $language
 * @property string $leaked
 * @property string $cpm
 * @property string $hours
 * @property string $kw
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class CalculatorDetails extends \app\components\Model
{

    public $cpm;
    public $kw;
    public $saving;
    public $leaked;
    public $hours;
    public $language;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'],'required'],
            [['cpm','kw'],'aloneReq','skipOnEmpty' => false,'skipOnError' => false],
            [['cpm','kw','saving','hours'],'integer'],
            [['language'], 'string', 'max' => 50],
        ];
    }

    public function aloneReq($attribute, $params)
    {
       if($this->cpm =='' && $this->kw=='')
       {
        $this->addError($attribute,'At least One value between Kw and CPM is required');
       }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'saving' => 'Saving',
            'language' => 'Language',
            'leaked' => 'Load',
            'cpm' => 'Consumption in Cubic feet per Minute ( CFM)',
            'hours' => 'Hours',
            'kw' => 'Motor Power (kW)',
        ];
    }


}
