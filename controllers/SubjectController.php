<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use yii\helpers\Url;
use app\modules\MubAdmin\modules\csvreader\models\Bisac;

class SubjectController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (
                $action->id == "payment-process" || $action->id == 'payment-cancel'
        ) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->view->params['page']='home';
        return $this->render('index');
    }

    public function actionSignup()
    {
        return $this->renderAjax('signup');
    }
    
    public function actionForgetpass()
    {
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('info@osmstays.com')
                    ->setTo($mubUserContact->email)
                    ->setCc('info@osmstays.com')
                    ->setSubject('Your OSMSTAYS Password')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->renderAjax('forgetpass');
    }


    public function AuthSuccess($client) 
    {
        $userAttributes = $client->getUserAttributes();
        if($client instanceof yii\authclient\clients\Google)
        {
            if(!empty($userAttributes['emails']))
            {
                $currentMail = $userAttributes['emails']['0']['value'];    
            }
        }
        elseif($client instanceof yii\authclient\clients\Facebook)
        {
            $currentMail = $userAttributes['email'];
        }
        if($currentMail != '')
        {
            $userContact = new \app\models\MubUserContact();
            $userExists = $userContact::find()->where(['email' => $currentMail,'del_status' => '0','status' => 'Active'])->one();
            $model = new LoginForm();
            if(empty($userExists)){
                $clientModel = new \app\models\ClientSignup();
                if($client instanceof yii\authclient\clients\Google)
                {
                    if(!empty($userAttributes['emails']))
                    {
                        $clientModel->email = $userAttributes['emails']['0']['value'];
                        $clientModel->first_name = $userAttributes['name']['givenName']; 
                        $clientModel->last_name = $userAttributes['name']['familyName'];
                        $clientModel->username = $userAttributes['emails']['0']['value'];   
                    }
                }
                elseif($client instanceof yii\authclient\clients\Facebook)
                {
                   $clientModel->email = $userAttributes['email'];
                   $clientModel->first_name = $userAttributes['first_name'];
                   $clientModel->last_name = $userAttributes['last_name'];
                   $clientModel->username = $userAttributes['email'];
                }
                $clientModel->password = 'osmstays';
                $user  = $clientModel->signup();
                if(!empty($user))
                {
                    $model->username = $user->username;
                    $model->password = $user->password;
                    $model->rememberMe = 1;
                    if($model->login()) 
                    {
                        $mubUserId = \app\models\User::getMubUserId();
                        $mubUser = MubUser::findOne($mubUserId);
                        $mubUserContact = $mubUser->mubUserContacts;
                         \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setSubject('Your Profile Created')
                        ->setCc('info@osmstays.com')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                       return $this->redirect('/');
                    }
                    else
                   {
                    p(['here' => $model->getErrors()]);
                   }
                }
            }
            else
            {
                $mubUser = MubUser::find()->where(['id' => $userExists->mub_user_id])->one();
                if(!empty($mubUser))
                {
                    $model->username = $mubUser->username;
                    $model->password = $mubUser->password;
                    if($model->login()) {
                       return $this->redirect('/');
                    }
                    else
                    {
                        p('there was a problem Logging you in ! Please contact Support');
                    }
                }
               p('Username Not found In database'); 
            }
        }
        else
        {
            p('Mail Id not found in social account');
        }
    }

    public function actionPolicy()
    {
       
        return $this->render('policy');
    }
    
    public function actionTermcondition()
    {
       
        return $this->render('termcondition');
    }

    public function updateUserRecord($postData)
    {
        $userModel = new \app\models\User();
        $mubUserModel = new \app\models\MubUser();
        $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
        $mubUserContact = $mubUser->mubUserContacts;
        $user = $userModel::findOne($mubUser->user_id);
        $user->first_name = $postData['MubUser']['first_name'];
        $user->last_name = $postData['MubUser']['last_name'];
        $user->setPassword($postData['MubUser']['password']);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        return ($user->save(false)) ? true : p($user->getErrors());
    }
    
    public function actionProfile()
    {   
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            \Yii::$app->mailer->compose('profileUpdated',['profile' => $postData])
                            ->setFrom('info@osmstays.com')
                            ->setTo($postData['MubUserContact']['email'])
                            ->setCc('info@osmstays.com')
                            ->setSubject('Your Profile Updated')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                            Yii::$app->session->setFlash('success', "Your Profile Updated Successfully!");
                            return $this->goBack('/site/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function actionHistory()
    {
        $mubUserId = \app\models\User::getMubUserId();
        $scheduledModel = new \app\models\ScheduledVisits();
        $mubUserModel = new \app\models\MubUser();
        $bookingModel = new \app\models\Booking();
        $currentUser = $mubUserModel::findOne($mubUserId);
        $scheduledDetails = $scheduledModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0','status' => 'scheduled'])->all();
        $bookingDetails = $bookingModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0'])->all(); 
        return $this->render('history',['currentUser' => $currentUser,'scheduledDetails' => $scheduledDetails , 'bookingDetails' => $bookingDetails]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionClientLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('signin', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }

    public function actionClientRegister()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
             
            if ($model->load(Yii::$app->request->post())) 
            {      
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setCc('info@osmstays.com')
                        ->setSubject('Your Profile Created')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                        return $this->goBack('/');
                    }
                }
                else
                {
                   $model->addError('username','This Username Already Exists in Database');
                   return $this->renderAjax('signup',['model' => $model]);
                }
            }
            return $model->getErrors();
        }
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/mub-admin/');
            }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
     public function actionContact()
    {
        $this->view->params['page'] = 'contact';
        // $mubUserModel = new \app\models\MubUser();
        $contactMail = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contactMail->load($params))
        {
            $this->layout = false;
            if($contactMail->save(false))
            {
                \Yii::$app->mailer->compose('contact',['contactMail' => $contactMail])
                    ->setFrom('info@osmstays.com')
                    ->setTo($contactMail->email)
                    ->setCc('info@osmstays.com')
                    ->setSubject('OSMSTAYS Contact Info')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->send();    
                return true;
            }
            else
            {
                p($contactMail->getErrors());
            }
                    
        }
        return $this->render('contact', [
        
            'contactMail' => $contactMail,
        ]);

    }
    public function actionRegister()
    {
        $this->layout = 'admin';
        $model = new SignupForm();
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0','active' => '1'];
        $activeStates = $state->getAll('state_name',$where);
         
        if ($model->load(Yii::$app->request->post())) 
        {      
            if ($user = $model->signup()) 
            {
                if (Yii::$app->getUser()->login($user))
                {

                $mubUserId = \app\models\User::getMubUserId();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($mubUserId);
                $mubUserContact = $mubUser->mubUserContacts;
                \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                    ->setFrom('info@osmstays.com')
                    ->setTo($mubUserContact->email)
                    ->setSubject('Your Profile Created')
                    ->setCc('info@osmstays.com')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->send();
                return $this->goBack('/mub-admin/');
                    
                }
            }
            else
            {
                p('here');
            }
        }
        return $this->render('register', [
            'model' => $model,
            'allStates' => $allStates,
            'activeStates' => $activeStates
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */

    public function actionSuccesspdf($id)
    {
        $this->layout = false;
        $pdf = \Yii::$app->pdf;
        $pdf->content = $this->renderPartial('successpdf');
        return $pdf->render();
    }


    public function actionFailed()
    {
        $this->view->params['page']='failed';
        return $this->render('failed');
    }
    public function actionSuccess()
    {
        $this->view->params['page']='success';
        return $this->render('success');
    }
    public function actionInternational()
    {
        $this->view->params['page']='international';
        return $this->render('international');
    }

    public function actionAbout()
    {
        $this->view->params['page']='about';
        return $this->render('about');
    }
    public function actionServices()
    {
        $this->view->params['page']='services';
        return $this->render('services');
    }

    public function actionClients()
    {
        $this->view->params['page']='clients';
        return $this->render('clients');
    }

    public function actionProducts()
    {
        $this->view->params['page']='blog';
        return $this->render('products');
    }
public function actionCases()
    {
        $this->view->params['page']='cases';
        return $this->render('cases');
    }

    public function actionGallery()
    {
        $this->view->params['page']='gallery';
        return $this->render('gallery');
    }

     public function actionRegisteruser()
    {
         $this->layout=false;
        $this->view->params['page']='registeruser';
        return $this->render('registeruser');
    }
     public function actionWorldbookstore()
    {
        $this->view->params['page']='worldbookstore';
        return $this->render('worldbookstore');
    }
     public function actionBookcategory()
    {
        $this->view->params['page']='bookcategory';
        return $this->render('bookcategory');
    }
     public function actionShipping()
    {
        $this->view->params['page']='shipping';
        return $this->render('shipping');
    }
     public function actionPrivacy()
    {
        $this->view->params['page']='privacy';
        return $this->render('privacy');
    }
    public function actionTermandcundition()
    {
        $this->view->params['page']='termandcundition';
        return $this->render('termandcundition');
    }
    public function actionPublisher()
    {
        $this->view->params['page']='publisher';
        return $this->render('publisher');
    }
    public function actionCatalog()
    {
        $this->view->params['page']='catalog';
        return $this->render('catalog');
    }
     public function actionSubject()
    {
        $this->view->params['page']='subject';
        return $this->render('subject');
    }

     public function actionCategory()
    {
        $bisac = new Bisac();
        $this->view->params['page']='category';
        return $this->render('category',['bisac' => $bisac]);
    }
    public function actionSubscription()
    {
        //$this->layout = 'site_main';
        $request = Yii::$app->request->bodyParams;
        if (isset($request['subscribe'])) {
            $redirectUrl = Url::to(['site/payment-process'], true);
            $cancelUrl = Url::to(['site/payment-cancel'], true);
            $params = [
                'tid' => time(),
                'merchant_id' => 12345,
                'order_id' => 14523,
                'amount' => 2500,
                'currency' => 'INR',
                'redirect_url' => $redirectUrl,
                'cancel_url' => $cancelUrl,
                'language' => 'EN',
            ];
            \app\components\Ccavenue::form($params, 'auto', 'test', 'websites');
        }
        return $this->render('subscription', [
        ]);
    }

    public function actionPaymentProcess()
    {
        $request = Yii::$app->request->bodyParams;
        //for live 
        //$workingKey = '*************************';
        
        //for websites
        $workingKey = '*************************';
        //for local
        //$workingKey = '*************************';
        
        $encResponse = $request["encResp"];
        $rcvdString = decrypt($encResponse, $workingKey);
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        $response = [];
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            $response[$information[0]] = $information[1];
        }
        if ($response['order_status'] === 'Success') {
            print_r($response);
            //Yii::$app->session->setFlash('success', '<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.');
            //return $this->redirect(['subscription']);
        }
        else if ($response['order_status'] === "Aborted") {
            Yii::$app->session->setFlash('error', 'Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail <br/>');
            return $this->redirect(['subscription']);
        }
        else if ($response['order_status'] === "Failure") {
            Yii::$app->session->setFlash('error', "<br>Thank you for shopping with us.However,the transaction has been declined.");
            return $this->redirect(['subscription']);
        }
        else {
            Yii::$app->session->setFlash('error', "<br>Security Error. Illegal access detected");
            return $this->redirect(['subscription']);
        }
    }

    public function actionPaymentCancel()
    {
        //$this->layout = 'site_main';
        $request = Yii::$app->request->bodyParams;
        //for live 
        //$workingKey = '*************************';
        
        //for demo server if any
        $workingKey = '*************************';
        //for local
        //$workingKey = '*************************';
        $encResponse = $request["encResp"];
        $rcvdString = decrypt($encResponse, $workingKey);
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        $response = [];
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            $response[$information[0]] = $information[1];
        }
        //debugPrint($response);
        return $this->render('payment-cancel', [
                    'response' => $response
        ]);
    }
}
    