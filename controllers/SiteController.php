<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use yii\helpers\Url;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use app\modules\MubAdmin\modules\csvreader\models\Bisac;
use app\modules\MubAdmin\modules\csvreader\models\Product;
use app\modules\MubAdmin\modules\csvreader\models\Publisher;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (
                $action->id == "payment-process" || $action->id == 'payment-cancel'
        ) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $subject = new \app\models\Subject();
        $model = $subject::find()->orderBy(['subject_name' => SORT_ASC])->where(['del_status' => '0','status' => 'active'])->all();
        $this->view->params['page']='home';
        return $this->render('index',['model'=>$model]);
    }

    public function actionSignup()
    {
        return $this->renderAjax('signup');
    }
    
    public function actionForgetpass()
    {
        if(Yii::$app->request->post())
        {
            $postData = \Yii::$app->request->post();
            $mubUserModel = new \app\models\MubUser();
            $mubUserContactModel = new \app\models\MubUserContact();
            $mubUserContact = $mubUserContactModel::find()->where(['email' => $postData['ClientSignup']['email']])->one();
            if(!empty($mubUserContact))
            {  
                $mubUser = $mubUserModel::find()->where(['id' => $mubUserContact->mub_user_id, 'del_status' => '0'])->one();
                $userEmail = $mubUserContact['email'];
                \Yii::$app->mailer->compose('forget',['mubUserContact' => $mubUserContact,'mubUser' => $mubUser])
                    ->setFrom('info@osmstays.com')
                    ->setTo($mubUserContact->email)
                    ->setCc('info@osmstays.com')
                    ->setSubject('Your OSMSTAYS Password')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                    return 'mailsent';
            }
        }
        return $this->render('forgetpass');
    }

    public function AuthSuccess($client) 
    {
        $userAttributes = $client->getUserAttributes();
        if($client instanceof yii\authclient\clients\Google)
        {
            if(!empty($userAttributes['emails']))
            {
                $currentMail = $userAttributes['emails']['0']['value'];    
            }
        }
        elseif($client instanceof yii\authclient\clients\Facebook)
        {
            $currentMail = $userAttributes['email'];
        }
        if($currentMail != '')
        {
            $userContact = new \app\models\MubUserContact();
            $userExists = $userContact::find()->where(['email' => $currentMail,'del_status' => '0','status' => 'Active'])->one();
            $model = new LoginForm();
            if(empty($userExists)){
                $clientModel = new \app\models\ClientSignup();
                if($client instanceof yii\authclient\clients\Google)
                {
                    if(!empty($userAttributes['emails']))
                    {
                        $clientModel->email = $userAttributes['emails']['0']['value'];
                        $clientModel->first_name = $userAttributes['name']['givenName']; 
                        $clientModel->last_name = $userAttributes['name']['familyName'];
                        $clientModel->username = $userAttributes['emails']['0']['value'];   
                    }
                }
                elseif($client instanceof yii\authclient\clients\Facebook)
                {
                   $clientModel->email = $userAttributes['email'];
                   $clientModel->first_name = $userAttributes['first_name'];
                   $clientModel->last_name = $userAttributes['last_name'];
                   $clientModel->username = $userAttributes['email'];
                }
                $clientModel->password = 'osmstays';
                $user  = $clientModel->signup();
                if(!empty($user))
                {
                    $model->username = $user->username;
                    $model->password = $user->password;
                    $model->rememberMe = 1;
                    if($model->login()) 
                    {
                        $mubUserId = \app\models\User::getMubUserId();
                        $mubUser = MubUser::findOne($mubUserId);
                        $mubUserContact = $mubUser->mubUserContacts;
                         \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setSubject('Your Profile Created')
                        ->setCc('info@osmstays.com')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                       return $this->redirect('/');
                    }
                    else
                   {
                    p(['here' => $model->getErrors()]);
                   }
                }
            }
            else
            {
                $mubUser = MubUser::find()->where(['id' => $userExists->mub_user_id])->one();
                if(!empty($mubUser))
                {
                    $model->username = $mubUser->username;
                    $model->password = $mubUser->password;
                    if($model->login()) {
                       return $this->redirect('/');
                    }
                    else
                    {
                        p('there was a problem Logging you in ! Please contact Support');
                    }
                }
               p('Username Not found In database'); 
            }
        }
        else
        {
            p('Mail Id not found in social account');
        }
    }

    public function actionPolicy()
    {
       
        return $this->render('policy');
    }
    
    public function actionTermcondition()
    {
       
        return $this->render('termcondition');
    }

    public function updateUserRecord($postData)
    {
        $userModel = new \app\models\User();
        $mubUserModel = new \app\models\MubUser();
        $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
        $mubUserContact = $mubUser->mubUserContacts;
        $user = $userModel::findOne($mubUser->user_id);
        $user->first_name = $postData['MubUser']['first_name'];
        $user->last_name = $postData['MubUser']['last_name'];
        $user->setPassword($postData['MubUser']['password']);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        return ($user->save(false)) ? true : p($user->getErrors());
    }
    
    public function actionProfile()
    {   
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            \Yii::$app->mailer->compose('profileUpdated',['profile' => $postData])
                            ->setFrom('info@osmstays.com')
                            ->setTo($postData['MubUserContact']['email'])
                            ->setCc('info@osmstays.com')
                            ->setSubject('Your Profile Updated')->setBcc('admin@makeubig.com','MakeUBIG ADMIN')->send();
                            Yii::$app->session->setFlash('success', "Your Profile Updated Successfully!");
                            return $this->goBack('/site/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function actionHistory()
    {
        $mubUserId = \app\models\User::getMubUserId();
        $scheduledModel = new \app\models\ScheduledVisits();
        $mubUserModel = new \app\models\MubUser();
        $bookingModel = new \app\models\Booking();
        $currentUser = $mubUserModel::findOne($mubUserId);
        $scheduledDetails = $scheduledModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0','status' => 'scheduled'])->all();
        $bookingDetails = $bookingModel::find()->where(['mub_user_id' => $mubUserId,'del_status' => '0'])->all(); 
        return $this->render('history',['currentUser' => $currentUser,'scheduledDetails' => $scheduledDetails , 'bookingDetails' => $bookingDetails]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionClientLogin()
    {
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if(\Yii::$app->request->post())
        {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
               return $this->redirect(Yii::$app->request->referrer);
            }
            else
            {
                return $this->render('clientsignin', [
                    'model' => $model,
                ]);
            }    
        }
        else
            {
                return $this->render('clientsignin', [
                    'model' => $model,
                ]);
            }   
        //case the normal request for form
        return $this->render('clientsignin', [
            'model' => $model,
        ]);
    }

    public function actionClientRegister()
    {
            $model = new ClientSignup();
             
            if ($model->load(Yii::$app->request->post())) 
            {      
                if ($user = $model->signup()) 
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                    $mubUserId = \app\models\User::getMubUserId();
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                        ->setFrom('info@osmstays.com')
                        ->setTo($mubUserContact->email)
                        ->setCc('info@osmstays.com')
                        ->setSubject('Your Profile Created')
                        ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                        ->send();
                        return $this->goBack(Yii::$app->session->setFlash('success', "Register Successfully."));
                    }
                }
                else
                {
                   $model->addError(Yii::$app->session->setFlash('success', "Username Already Exists."));
                   return $this->render('clientsignup',['model' => $model]);
                }
            }
            else
                {
                   $model->addError('jnujnu');
                   return $this->render('clientsignup',['model' => $model]);
                }
    }

    public function actionClientRegisterValidate()
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new ClientSignup();
            $model->load(Yii::$app->request->post());
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/mub-admin/');
            }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
     public function actionContact()
    {
        $this->view->params['page'] = 'contact';
        // $mubUserModel = new \app\models\MubUser();
        $contactMail = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contactMail->load($params))
        {
            $this->layout = false;
            if($contactMail->save(false))
            {
                \Yii::$app->mailer->compose('contact',['contactMail' => $contactMail])
                    ->setFrom('info@osmstays.com')
                    ->setTo($contactMail->email)
                    ->setCc('info@osmstays.com')
                    ->setSubject('OSMSTAYS Contact Info')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->send();    
                return true;
            }
            else
            {
                p($contactMail->getErrors());
            }
                    
        }
        return $this->render('contact', [
        
            'contactMail' => $contactMail,
        ]);

    }
    public function actionRegister()
    {
        $this->layout = 'admin';
        $model = new SignupForm();
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0','active' => '1'];
        $activeStates = $state->getAll('state_name',$where);
         
        if ($model->load(Yii::$app->request->post())) 
        {      
            if ($user = $model->signup()) 
            {
                if (Yii::$app->getUser()->login($user))
                {

                $mubUserId = \app\models\User::getMubUserId();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($mubUserId);
                $mubUserContact = $mubUser->mubUserContacts;
                \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                    ->setFrom('info@osmstays.com')
                    ->setTo($mubUserContact->email)
                    ->setSubject('Your Profile Created')
                    ->setCc('info@osmstays.com')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->send();
                return $this->goBack('/mub-admin/');
                    
                }
            }
            else
            {
                p('here');
            }
        }
        return $this->render('register', [
            'model' => $model,
            'allStates' => $allStates,
            'activeStates' => $activeStates
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */


     public function actionAddtocart($id)
    {   
        $cart = new ShoppingCart();
        $this->layout = false;
        $model = Product::findOne($id);
        if ($model){
            $cart->put($model, 1);
            return $this->render('showcart', [
                'cart' => $cart,
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionClearCart($id=NULL)
    {
         $cart = new ShoppingCart();
         $this->layout = false;
         if($id == NULL)
         {
            $cart->removeAll();
         }
         else
         {
            $cart->removeById($id);  
         }
         return $this->render('showcart', [
            'cart' => $cart]);
    }

    public function actionUpdateCartItems($id,$quantity)
    {  
        if($quantity >= 0)
        {
            $cart = new ShoppingCart();
            $this->layout = false;
            $model = Product::findOne($id);
            if($model)
            {
                $cart->update($model, $quantity);
            }
            $total = $cart->getCost(); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['total'] = $total;
            return $data;
        } 
    }

    public function actionCheckoutLogin()
    {
        if (\Yii::$app->request->isAjax){
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('checkout-login', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->renderAjax('checkout-login', [
                'model' => $model,
            ]);
        }
        p('Nice Try!! :D');
    }
    
    public function actionCheckout()
    {
        $this->view->params['page'] = 'checkout';
        
       $cart = new ShoppingCart();
       $cartItems = $cart->getPositions();
       return $this->render('checkout',['cartItems' => $cartItems,'cart' => $cart]);
    }

    public function actionPayment()
    {
        $this->view->params['page'] = 'payment';
        return $this->render('payment');
    }

    public function actionCartcount()
    {
        $this->layout = false;
        $this->view->params['page']='cartcount';
        $cart = new ShoppingCart();
        $cartItems = $cart->getPositions();
        $count = count($cartItems);
        return $this->render('cartcount', ['count' => $count]);
    }

    public function actionSuccesspdf($id)
    {
        $this->layout = false;
        $pdf = \Yii::$app->pdf;
        $pdf->content = $this->renderPartial('successpdf');
        return $pdf->render();
    }

    public function actionLoginCheckout()
    {
        $this->view->params['page']='login-checkout';
            $model = new LoginForm();
            if(\Yii::$app->request->post())
            {
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                   return $this->redirect(Yii::$app->request->referrer);
                }
                else
                {
                    return $this->renderAjax('signin', [
                        'model' => $model,
                    ]);
                }    
            }
            //case the normal request for form
            return $this->render('login-checkout', [
                'model' => $model,
            ]);
        
    }

    public function actionShowcart()
    {
        $this->layout = false;
        $cart = new ShoppingCart();
        $items = $cart->getPositions();
        return $this->render('showcart', [
            'items' => $items]);  
    }


    public function actionFailed()
    {
        $this->view->params['page']='failed';
        return $this->render('failed');
    }
    public function actionSuccess()
    {
        $this->view->params['page']='success';
        return $this->render('success');
    }
    public function actionPaymentmethod()
    {
        $this->view->params['page']='paymentmethod';
        return $this->render('paymentmethod');
    }

    public function actionAbout()
    {
        $this->view->params['page']='about';
        return $this->render('about');
    }
    public function actionServices()
    {
        $this->view->params['page']='services';
        return $this->render('services');
    }

    public function actionClients()
    {
        $this->view->params['page']='clients';
        return $this->render('clients');
    }

    public function actionProducts()
    {
        $this->view->params['page']='blog';
        return $this->render('products');
    }

    public function actionRefund()
    {
        $this->view->params['page']='refund';
        return $this->render('refund');
    }

    public function actionGallery()
    {
        $this->view->params['page']='gallery';
        return $this->render('gallery');
    }

     public function actionRegisteruser()
    {
         $this->layout=false;
        $this->view->params['page']='registeruser';
        return $this->render('registeruser');
    }

    public function actionWorldbookstore($name)
    {
        $this->view->params['page']='worldbookstore';
        $name = Yii::$app->getRequest()->getQueryParam('name');
        $oneBisacSql = "select `bisac_code`,`slug`,`bisac_title` from `bisac` where `slug` = '".$name."' limit 1;";
        $oneBisac = \yii::$app->db->createCommand($oneBisacSql)->queryOne();
        return $this->render('worldbookstore',['oneBisac' => $oneBisac]);
    }

    public function actionBookdetail()
    {
        $this->view->params['page']='bookdetail';
        return $this->render('bookdetail');
    }

    public function actionBookdetails()
    {
        $this->view->params['page']='bookdetails';
        return $this->render('bookdetails');
    }

     public function actionBookcategory()
    {
        $this->view->params['page']='bookcategory';
        return $this->render('bookcategory');
    }
     public function actionShipping()
    {
        $this->view->params['page']='shipping';
        return $this->render('shipping');
    }
     public function actionPrivacy()
    {
        $this->view->params['page']='privacy';
        return $this->render('privacy');
    }
    public function actionTerm()
    {
        $this->view->params['page']='term';
        return $this->render('term');
    }
    public function actionPublisher($name)
    {
        $this->view->params['page']='publisher';
        $publisher = new Publisher();
        $allPublish = $publisher::find()->where(['LIKE', 'publisher', $name.'%', false])->orderBy(['publisher' => SORT_ASC])->all();

        $countQuery = count($allPublish); 
        $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 100]);
        $allPublisher =  $publisher::find()->where(['LIKE', 'publisher', $name.'%', false])->orderBy(['publisher' => SORT_ASC])->limit($pages->limit)->offset($pages->offset)->all();

        return $this->render('publisher',['allPublisher' => $allPublisher,'pages' => $pages]);
    }
    public function actionClientsignup()
    {
        $this->view->params['page']='clientsignup';
        return $this->render('clientsignup');
    }

    public function actionFaq()
    {
        $this->view->params['page']='faq';
        return $this->render('faq');
    }

    public function actionPublish()
    {
        $this->view->params['page']='publish';
        return $this->render('publish');
    }

    public function actionLocation()
    {
        $this->view->params['page']='location';
        return $this->render('location');
    }

    public function actionCatalog()
    {
        $this->view->params['page']='catalog';
        return $this->render('catalog');
    }
     public function actionSubject()
    {
        $this->view->params['page']='subject';
        return $this->render('subject');
    }

     public function actionCategory()
    {
        $bisac = new Bisac();
        $name = Yii::$app->getRequest()->getQueryParam('name');

      switch($name)
      {
        case 'antiques--collectibles':
        {
          $backgroundClass =  'category-page-banner-con antiques';
          break;
        }
        case 'architecture':
        {
          $backgroundClass =  'category-page-banner-con architecture';
          break;
        }
        case 'art':
        {
          $backgroundClass =  'category-page-banner-con art';
          break;
        }
        case 'bibles':
        {
          $backgroundClass =  'category-page-banner-con bibles';
          break;
        }
        case 'biography--autobiography':
        {
          $backgroundClass =  'category-page-banner-con biography';
          break;
        }
        case 'business--economics':
        {
          $backgroundClass =  'category-page-banner-con business';
          break;
        }
        case 'comics--graphic-novels':
        {
          $backgroundClass =  'category-page-banner-con comics';
          break;
        }
        case 'cooking':
        {
          $backgroundClass =  'category-page-banner-con cooking';
          break;
        }
        case 'computers':
        {
          $backgroundClass =  'category-page-banner-con computers';
          break;
        }
         case 'crafts--hobbies':
        {
          $backgroundClass =  'category-page-banner-con crafts';
          break;
        }
        case 'design':
        {
          $backgroundClass =  'category-page-banner-con design';
          break;
        }
        case 'drama':
        {
          $backgroundClass =  'category-page-banner-con drama';
          break;
        }

          case 'education':
        {
          $backgroundClass =  'category-page-banner-con education';
          break;
        }
        case 'family--relationships':
        {
          $backgroundClass =  'category-page-banner-con family';
          break;
        }
        case 'fiction':
        {
          $backgroundClass =  'category-page-banner-con fiction';
          break;
        } 

         case 'foreign-language-study':
        {
          $backgroundClass =  'category-page-banner-con foreign';
          break;
        }
        case 'games':
        {
          $backgroundClass =  'category-page-banner-con games';
          break;
        }
        case 'gardening':
        {
          $backgroundClass =  'category-page-banner-con gardening';
          break;
        }

         case 'history':
        {
          $backgroundClass =  'category-page-banner-con history';
          break;
        }
        case 'health--fitness':
        {
          $backgroundClass =  'category-page-banner-con health';
          break;
        }
        case 'house--home':
        {
          $backgroundClass =  'category-page-banner-con house';
          break;
        }

         case 'humor':
        {
          $backgroundClass =  'category-page-banner-con humor';
          break;
        }
        case 'juvenile-nonfiction':
        {
          $backgroundClass =  'category-page-banner-con juvenile-nonfiction';
          break;
        }
        case 'juvenile-fiction':
        {
          $backgroundClass =  'category-page-banner-con juvenile-fiction';
          break;
        }

         case 'language-arts--disciplines':
        {
          $backgroundClass =  'category-page-banner-con language';
          break;
        }
         case 'law':
        {
          $backgroundClass =  'category-page-banner-con law';
          break;
        }
        case 'literary-collections':
        {
          $backgroundClass =  'category-page-banner-con literary';
          break;
        }
        case 'literary-criticism':
        {
          $backgroundClass =  'category-page-banner-con literary-cri';
          break;
        }

         case 'mathematics':
        {
          $backgroundClass =  'category-page-banner-con mathematics';
          break;
        }
         case 'medical':
        {
          $backgroundClass =  'category-page-banner-con medical';
          break;
        }
        case 'recorded-music':
        {
          $backgroundClass =  'category-page-banner-con music';
          break;
        }
        case 'music':
        {
          $backgroundClass =  'category-page-banner-con music';
          break;
        }
         case 'nature':
        {
          $backgroundClass =  'category-page-banner-con nature';
          break;
        }
        case 'performing-arts':
        {
          $backgroundClass =  'category-page-banner-con performing-arts';
          break;
        }

        case 'pets':
        {
          $backgroundClass =  'category-page-banner-con pets';
          break;
        }
        case 'philosophy':
        {
          $backgroundClass =  'category-page-banner-con philosophy';
          break;
        }
         case 'photography':
        {
          $backgroundClass =  'category-page-banner-con photography';
          break;
        }
        case 'poetry':
        {
          $backgroundClass =  'category-page-banner-con poetry';
          break;
        }

         case 'political-science':
        {
          $backgroundClass =  'category-page-banner-con political-science';
          break;
        }
         case 'psychology':
        {
          $backgroundClass =  'category-page-banner-con psychology';
          break;
        }
        case 'reference':
        {
          $backgroundClass =  'category-page-banner-con reference';
          break;
        }

          case 'religion':
        {
          $backgroundClass =  'category-page-banner-con religion';
          break;
        }
         case 'science':
        {
          $backgroundClass =  'category-page-banner-con science';
          break;
        }
         case 'self-help':
        {
          $backgroundClass =  'category-page-banner-con self';
          break;
        }
        case 'social-science':
        {
          $backgroundClass =  'category-page-banner-con social-science';
          break;
        }

         case 'sports--recreation':
        {
          $backgroundClass =  'category-page-banner-con sports';
          break;
        }
         case 'study-aids':
        {
          $backgroundClass =  'category-page-banner-con study';
          break;
        }
         case 'technology--engineering':
        {
          $backgroundClass =  'category-page-banner-con technology';
          break;
        }
        case 'transportation':
        {
          $backgroundClass =  'category-page-banner-con transportation';
          break;
        } 
        case 'true-crime':
        {
          $backgroundClass =  'category-page-banner-con true-crime';
          break;
        }
        case 'travel':
        {
          $backgroundClass =  'category-page-banner-con travel';
          break;
        }
         case 'young-adult-fiction':
        {
          $backgroundClass =  'category-page-banner-con young-adult-fiction';
          break;
        }
        case 'young-adult-nonfiction':
        {
          $backgroundClass =  'category-page-banner-con young-adult-nonfiction';
          break;
        }
        default:
        {
          $backgroundClass =  'category-page-banner-con architecture';
          break;
        }
        }
        $this->view->params['page']='category';
        return $this->render('category',['bisac' => $bisac,'backgroundClass' => $backgroundClass]);
    }
    public function actionSubscription()
    {
        //$this->layout = 'site_main';
        $request = Yii::$app->request->bodyParams;
        if (isset($request['subscribe'])) {
            $redirectUrl = Url::to(['site/payment-process'], true);
            $cancelUrl = Url::to(['site/payment-cancel'], true);
            $params = [
                'tid' => time(),
                'merchant_id' => 12345,
                'order_id' => 14523,
                'amount' => 2500,
                'currency' => 'INR',
                'redirect_url' => $redirectUrl,
                'cancel_url' => $cancelUrl,
                'language' => 'EN',
            ];
            \app\components\Ccavenue::form($params, 'auto', 'test', 'websites');
        }
        return $this->render('subscription', [
        ]);
    }

    public function actionPaymentProcess()
    {
        $request = Yii::$app->request->bodyParams;
        //for live 
        //$workingKey = '*************************';
        
        //for websites
        $workingKey = '*************************';
        //for local
        //$workingKey = '*************************';
        
        $encResponse = $request["encResp"];
        $rcvdString = decrypt($encResponse, $workingKey);
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        $response = [];
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            $response[$information[0]] = $information[1];
        }
        if ($response['order_status'] === 'Success') {
            print_r($response);
            //Yii::$app->session->setFlash('success', '<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.');
            //return $this->redirect(['subscription']);
        }
        else if ($response['order_status'] === "Aborted") {
            Yii::$app->session->setFlash('error', 'Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail <br/>');
            return $this->redirect(['subscription']);
        }
        else if ($response['order_status'] === "Failure") {
            Yii::$app->session->setFlash('error', "<br>Thank you for shopping with us.However,the transaction has been declined.");
            return $this->redirect(['subscription']);
        }
        else {
            Yii::$app->session->setFlash('error', "<br>Security Error. Illegal access detected");
            return $this->redirect(['subscription']);
        }
    }

    public function actionPaymentCancel()
    {
        //$this->layout = 'site_main';
        $request = Yii::$app->request->bodyParams;
        //for live 
        //$workingKey = '*************************';
        
        //for demo server if any
        $workingKey = '*************************';
        //for local
        //$workingKey = '*************************';
        $encResponse = $request["encResp"];
        $rcvdString = decrypt($encResponse, $workingKey);
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        $response = [];
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            $response[$information[0]] = $information[1];
        }
        //debugPrint($response);
        return $this->render('payment-cancel', [
                    'response' => $response
        ]);
    }

    public function actionPublisherBooks($name)
    {
    $this->view->params['page']='publisher-books';
    $name = Yii::$app->getRequest()->getQueryParam('name');
    
    $publisher = new Publisher();
    $publisherDetails = $publisher::find()->where(['slug' =>$name])->one();
    $publisherName = $publisherDetails->publisher;

    $query = "select * from publisher_products where `publisher_key` ='".$name."'";
    $sql = \Yii::$app->db->createCommand($query);
    $publishIng = $sql->queryAll();
    $allEans = [];
    foreach ($publishIng as $key => $PublisherIng) {
       $allEans[] =  $PublisherIng['ing_product_id'];
    }
    $eanSql = implode('\',\'',$allEans);
    $queryProduct = "select `product`.*,`price`.`price`,`price`.`ean_13` from product inner join `price` on `product`.`id`=`price`.`ean_13` where `product`.`id` in ('".$eanSql."') AND `product`.`del_status` = '0'";
    $sqlProduct = \Yii::$app->db->createCommand($queryProduct);
    $product = $sqlProduct->queryAll();
    $countQuery = count($product); 

    $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 40]); 

    $queryPro = "select `product`.*,`price`.`price`,`price`.`ean_13` from product inner join `price` on `product`.`id`=`price`.`ean_13` where `product`.`id` in ('".$eanSql."') AND `product`.`del_status` = '0' LIMIT ".$pages->limit." OFFSET ".$pages->offset."";
    $products = \yii::$app->db->createCommand($queryPro)->queryAll(); 
    
    return $this->render('publisher-books',['pages' => $pages,'products' => $products,'publisherName' => $publisherName,'countQuery' => $countQuery]);
    }

    public function actionGetListing($q)
    {
        $slug = \app\helpers\StringHelper::generateSlug($q);
        $queryProduct = "select * from `product` where `slug` like '".$slug."%' limit 15";
        $sqlProduct = \Yii::$app->db->createCommand($queryProduct);
        $products = $sqlProduct->queryAll();
        $response = [];
        foreach ($products as $resultKey => $result) 
        {
            $response['result'][] = $result['product_name'];
        }
        return json_encode($response);
    }

    public function actionSqlQuery()
    {
        if (\Yii::$app->request->isAjax)
        {
             $data = Yii::$app->request->post();
             $query = $data['query'];
             $sql = \Yii::$app->db->createCommand($query);
             $queryExecute = $sql->queryAll();
             $response = count($queryExecute);
             return $response.' '."Row Updated";
        }
    }

    public function actionGetSortedResult($sortId,$categoryId)
    {
    $this->layout = false;
    
    $oneBisacSql = "select `bisac_code`,`slug`,`bisac_title` from `bisac` where `slug` = '".$categoryId."' limit 1;";
    $oneBisac = \yii::$app->db->createCommand($oneBisacSql)->queryOne(); 
    $productsGen = "select `product`.`id`,`product`.`product_name`,`product`.`format`,`product`.`publish_date`,`product`.`slug`,`product`.`language`,`product`.`bisac`,`product`.`type`,`product`.`description`,`product`.`status`,`product`.`del_status`,`price`.`ean_13`,`price`.`price`,`price`.`weight` from `product` INNER JOIN `price` on `product`.`id` = `price`.`ean_13` where `bisac` = '".$oneBisac['bisac_code']."' AND `product`.`del_status` = '0'";
    $getAll = \yii::$app->db->createCommand($productsGen)->queryAll();
    $countQuery = count($getAll); 

    $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 20]);
    
       $productsGen = "select `product`.`id`,`product`.`product_name`,`product`.`format`,`product`.`publish_date`,`product`.`slug`,`product`.`language`,`product`.`bisac`,`product`.`type`,`product`.`description`,`product`.`status`,`product`.`del_status`,`price`.`ean_13`,`price`.`price`,`price`.`weight` from `product` INNER JOIN `price` on `product`.`id` = `price`.`ean_13` where `bisac` = '".$oneBisac['bisac_code']."' AND `product`.`del_status` = '0'";


        switch($sortId)
        {
            case 'phtl':
            {
                $productsGen .= " ORDER BY `price` DESC";
                break;
            }
            case 'plth':
            {
                $productsGen .= " ORDER BY `price` ASC";
                break;
            }
            case 'nasc':
            {
                $productsGen .= " ORDER BY `product_name` DESC";
                break;
            }
            case 'ndsc':
            {
                $productsGen .= " ORDER BY `product_name` ASC";
                break;
            }

            case 'newest_tab':
            {
                $productsGen .= " ORDER BY `id` ASC";
                break;
            }
            default:
            {

            }

        }
        $productsGen .= " LIMIT $pages->pageSize OFFSET $pages->offset";
        $getAllSubject = \yii::$app->db->createCommand($productsGen)->queryAll();

         return $this->renderAjax('sortdata',['pages' => $pages,'getAllSubject' => $getAllSubject,'oneBisac' => $oneBisac,'countQuery' => $countQuery]);
        
    }

}
