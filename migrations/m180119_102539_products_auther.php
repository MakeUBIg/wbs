<?php

namespace app\migrations;
use app\commands\Migration;

class m180119_102539_products_auther extends Migration
{
   public function getTableName()
    {
        return 'products_author';
    }

    public function getForeignKeyFields()
    {
        return [
            'ing_product_id' => ['product','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'author_key' => 'author_key',
            'ing_product_id' => 'ing_product_id',
            'status' => 'status',
            'del_status'=> 'del_status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'author_key' => $this->string(255)->notNull(),
            'ing_product_id' => $this->string(255)->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
    public function safeUp()
    {
        parent::safeUp();
        $columns = ['author_key','ing_product_id'];
        $this->db->createCommand()->createIndex('unique_author_product', $this->getTableName(), $columns, true)->execute();
    }
}