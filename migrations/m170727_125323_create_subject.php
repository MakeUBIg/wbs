<?php

namespace app\migrations;
use app\commands\Migration;
use app\helpers\StringHelper;

class m170727_125323_create_subject extends Migration
{
    public $allSubjects = [
        ["ANT","Antiques & Collectibles", "Antiques&Collectibles.jpg"],
        ["ARC","Architecture", "Architecture.jpg"],
        ["ART","Art", "Art.jpg"],
        ["BIB","Bibles", ""],
        ["OCC","Body,Mind & Spirit", "Body.jpg"],
        ["BIO","Biography & Autobiography", "Biography&Autobiography.jpg"],
        ["BUS","Business & Economics", "Business&Economics.jpg"],
        ["CGN","Comics & Graphic Novels", "Comics&GraphicNovels.jpg"],
        ["CKB","Cooking", "Cooking.jpg"],
        ["COM","Computers", "Computers.jpg"],
        ["CRA","Crafts & Hobbies", "Crafts&Hobbies.jpg"],
        ["DES","Design", "Design.jpg"],
        ["DRA","Drama", "Drama.jpg"],
        ["EDU","Education", "Education.jpg"],
        ["FAM","Family & Relationships", "Family&Relationships.jpg"],
        ["FIC","Fiction", "Fiction.jpg"],
        ["FOR","Foreign Language Study", "ForeignLanguageStudy.jpg"],
        ["GAM","Games", "Games.jpg"],
        ["GAR","Gardening", "Gardening.jpg"],
        ["HEA","Health & Fitness", "Health&Fitness.jpg"],
        ["HIS","History", "History.jpg"],
        ["HOM","House & Home", "House&Home.jpg"],
        ["HUM","Humor", "Humor.jpg"],
        ["JNF","Juvenile Nonfiction", "JuvenileFiction.jpg"],
        ["JUV","Juvenile Fiction", "JuvenileNonfiction.jpg"],
        ["LAN","Language Arts & Disciplines", "Language-Arts-&-Discipline.jpg"],
        ["LAW","Law", "Law.jpg"],
        ["LCO","Literary Collections", "Literary-Collection.jpg"],
        ["LIT","Literary Criticism", "Literary-Criticism.jpg"],
        ["MAT","Mathematics", "Mathematics.jpg"],
        ["MED","Medical", "Medical.jpg"],
        ["MRC","Recorded Music", "Music.jpg"],
        ["MUS","Music", "Music.jpg"],
        ["NAT","Nature", "Nature.jpg"],
        ["NON","Non-Classifiable", ""],
        ["PER","Performing Arts", "PerformingArts.jpg"],
        ["PET","Pets", "Pets.jpg"],
        ["PHI","Philosophy", "Philosophy.jpg"],
        ["PHO","Photography", "Photography.jpg"],
        ["POE","Poetry", "Poetry.jpg"],
        ["POL","Political Science", "PoliticalScience.jpg"],
        ["PRD","Periodicals", ".jpg"],
        ["PSY","Psychology", "Psychology.jpg"],
        ["REF","Reference", "Reference.jpg"],
        ["REL","Religion", "Religion.jpg"],
        ["SCI","Science", "Science.jpg"],
        ["SEL","Self-Help", "Self-Help.jpg"],
        ["SOC","Social Science", "SocialScience.jpg"],
        ["SPO","Sports & Recreation", "Sports&Recreation.jpg"],
        ["STU","Study Aids", "StudyAids.jpg"],
        ["TEC","Technology & Engineering", "Technology&Engineering.jpg"],
        ["TRA","Transportation", "Transportation2.jpg"],
        ["TRU","True Crime", "TrueCrime.jpg"],
        ["TRV","Travel", "Transportation.jpg.jpg"],
        ["VGM","Video Games", ".jpg"],
        ["VID","Video & DVDGenre", ".jpg"],
        ["YAF","YOUNG ADULT FICTION", "Young-Adult-Fiction.jpg"],
        ["YAN","YOUNG ADULT NONFICTION", "YoungAdultNonFiction.jpg"]
    ];
   public function getTableName()
    {
        return 'subject';
    }

    public function getForeignKeyFields()
    {
        return [];
    }

    public function getKeyFields()
    {
        return [
            'status' => 'status',
            'subject_code' => 'subject_code',
            'subject_name' => 'subject_name',
            'del_status'=> 'del_status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'subject_code' => $this->string(3)->notNull(),
            'subject_name' => $this->string()->notNull(),
            'subject_slug' => $this->string()->notNull(),
            'subject_image' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    } 

    public function safeUp()
    {
        parent::safeUp();
        $allSubs = $this->allSubjects;
        $recordSet = [];
        foreach ($allSubs as $key => $value) {
            $subjectSlug = StringHelper::generateSlug($value[1]);
            $recordSet[$key] =  [$value[0],$value[1],$subjectSlug,$value[2]];
        }
        $count = \Yii::$app->db->createCommand()->batchInsert('subject',
            ['subject_code','subject_name', 'subject_slug', 'subject_image'], $recordSet)->execute();
        echo $count ." rows affected";
    }

}