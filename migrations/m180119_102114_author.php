<?php

namespace app\migrations;
use app\commands\Migration;

class m180119_102114_author extends Migration
{
    public function getTableName()
    {
        return 'author';
    }
    
    public function getKeyFields()
    {
        return [
            'key_name'  => 'key_name'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'f_name' => $this->string(),
            'l_name' => $this->string(),
            'title' => $this->string()->defaultValue(NULL),
            'key_name' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
    public function safeUp()
    {
        parent::safeUp();
        $columns = ['key_name','del_status'];
        $this->db->createCommand()->createIndex('unique_author_fname_keyname', $this->getTableName(), $columns, true)->execute();
        $this->db->createCommand()->createIndex('unique_publisher_key', $this->getTableName(),['key_name'], true)->execute();
    }
}
