<?php

namespace app\migrations;
use app\commands\Migration;

class m180119_102230_publisher extends Migration
{
    
    public function getTableName()
    {
        return 'publisher';
    }

    public function getForeignKeyFields()
    {
        return [
           
        ];
    }

    public function getKeyFields()
    {
        return [
            'publisher'  => 'publisher',
            'status' => 'status',
            'del_status'=> 'del_status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'publisher' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['publisher','del_status'];
        $this->db->createCommand()->createIndex('unique_publisher', $this->getTableName(), $columns, true)->execute();
        $this->db->createCommand()->createIndex('unique_publisher_key', $this->getTableName(),['slug'], true)->execute();
    }
}