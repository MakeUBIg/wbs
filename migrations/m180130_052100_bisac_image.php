<?php

namespace app\migrations;
use app\commands\Migration;

class m180130_052100_bisac_image extends Migration
{
   public function getTableName()
    {
        return 'bisac_image';
    }
    public function getForeignKeyFields()
    {
        return [
            'bisac_id' => ['bisac','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'title' => 'title',
            'status' => 'status',
            'del_status'=> 'del_status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'bisac_id' => $this->integer()->defaultValue(NULL),
            'title' => $this->string(50)->notNull(),
            'description' => $this->string(),
            'alt_tag' => $this->string(),
            'keyword' => $this->string(),
            'caption' => $this->string(),
            'path' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
