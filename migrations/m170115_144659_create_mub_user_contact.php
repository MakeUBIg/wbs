<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144659_create_mub_user_contact extends Migration
{
    public function getTableName()
    {
        return 'mub_user_contact';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $mubUser = new \app\models\MubUser();
        $allUsers = $mubUser::find()->all();
        foreach ($allUsers as $user) 
        {
            $mubUserContacts = new \app\models\MubUserContact();
            $mubUserContacts->mub_user_id = $user->id;
            $mubUserContacts->email = 'admin@mub.com';
            $mubUserContacts->city = 'New Delhi';
            $mubUserContacts->pin_code= "1100089";
            $mubUserContacts->mobile = "0987654321";
            $mubUserContacts->landline = '1234567890';
            $mubUserContacts->address = "House No:, Street, City";
            if($mubUserContacts->save())
            {
                echo 'created user admin Contact \n';
            }
            else
            {
                p($mubUserContacts->getErrors());
            }
        }
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id'  =>  'mub_user_id',
            'city' => 'city',
            'mobile' => 'mobile',
            'email' => 'email'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'designation' => $this->string(),
            'department' => $this->string(),
            'organization' => $this->string(),
            'address' => $this->string(255)->notNull(),
            'city' => $this->string()->notNull(),
            'pin_code' => $this->integer(100)->notNull(),
            'state' => $this->string(),
            'country' => $this->string(),
            'landline' => $this->integer(),
            'email' => $this->string(50)->notNull(),
            'website' => $this->string(),
            'mobile' => $this->integer(100)->notNull(),
            'message' => $this->string(),
            'alt_address' => $this->string(),
            'lat' => $this->string(100)->notNull()->defaultValue('NA'),
            'long' => $this->string(100)->notNull()->defaultValue('NA'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
