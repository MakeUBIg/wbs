<?php

namespace app\migrations;
use app\commands\Migration;

class m170607_060228_create_booking extends Migration
{
    public function getTableName()
    {
        return 'booking';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
                'email' => 'email',
                'mobile' => 'mobile',
                'booking_item' => 'booking_item',
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(),
            'name' => $this->string(),
            'designation' => $this->string(),
            'department' => $this->string(),
            'organization' => $this->string(),
            'address' => $this->string(255)->notNull(),
            'city' => $this->string()->notNull(),
            'pin_code' => $this->integer(100)->notNull(),
            'state' => $this->string(),
            'country' => $this->string(),
            'landline' => $this->integer(),
            'email' => $this->string(50)->notNull(),
            'website' => $this->string(),
            'mobile' => $this->integer(100)->notNull(),
            'message' => $this->string(),
            'alt_address' => $this->string(),
            'lat' => $this->string(100)->notNull()->defaultValue('NA'),
            'long' => $this->string(100)->notNull()->defaultValue('NA'),
            'booking_item' =>  $this->string(),
            'quantity' =>  $this->string(),  
            'amount' => $this->integer(),
            'time' => $this->string(), 
            'payment_mode' => $this->string(),
            'status' => "enum('booking','done','cancelled') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT 'booking'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
