<?php

namespace app\migrations;
use app\commands\Migration;

class m180220_105055_fileUpload extends Migration
{
    public function getTableName()
    {
        return 'fileUpload';
    }

    public function getForeignKeyFields()
    {
        return [
          'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'file_name'  => 'file_name',
            'file_path'  => 'file_path',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'file_name' => $this->string()->notNull(),
            'file_path' => $this->string()->notNull(),
            // 0 = fileupload, 1 = read-product, 2 = read-author, 3 = read-publishers, 4 = link-author, 5 = link-publisher //
            'load_status' =>  "enum('0','1','2','3','4','5') NOT NULL DEFAULT '0'",
            'file_data' =>  "enum('product','price') NOT NULL DEFAULT 'product'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ];
    }
}
