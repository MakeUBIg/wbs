<?php

namespace app\migrations;
use app\commands\Migration;

class m180205_103025_price extends Migration
{
    public function getTableName()
    {
        return 'price';
    }

    public function getForeignKeyFields()
    {
        return [
            'ean_13' => ['product','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'price'  => 'price',
            'ean_13' => 'ean_13',
            'del_status'=> 'del_status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'ean_13' => $this->string()->notNull(),
            'price' => $this->string()->notNull(),
            'weight' => $this->string()->notNull(),
            'discount_per' => $this->string(),
            'discount_amount' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['ean_13','price','weight'];
        $this->db->createCommand()->createIndex('unique_product_price_weight', $this->getTableName(), $columns, true)->execute();
    }
}
