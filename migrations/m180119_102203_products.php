<?php

namespace app\migrations;
use app\commands\Migration;

class m180119_102203_products extends Migration
{
    public function getTableName()
    {
        return 'product';
    }

    public function getKeyFields()
    {
        return [
            'id'  => 'id',
            'product_name'  => 'product_name',
            'bisac' => 'bisac',
            'slug' => 'slug'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->string(255)->notNull(),
            'product_name' => $this->string(700)->notNull(),
            'format' => $this->string()->notNull(),
            'publish_date' => $this->dateTime(),
            'slug' => $this->string(700)->defaultValue(NULL),
            'language' => $this->string()->defaultValue(NULL),
            'bisac' => $this->string()->defaultValue(NULL),
            'type' => $this->string(255)->defaultValue(NULL),
            'description' => "text DEFAULT NULL",
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $this->addPrimaryKey('id', $this->getTableName(), 'id');
    }
}