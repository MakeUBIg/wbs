<style type="text/css">
    .home-header-main-outer-panel{
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel ">

<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>
<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->


<!--FAQ ACCORDION SECTION STRAT-->
<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
        <div class="col-xs-12 pad_none cont-payment-methods">
            <h2>Privacy Policy</h2>
            <div class="col-xs-12 col-md-9 col-sm-9 pad_none payment-list privacy-policy-content">
                <ul>
                <li>This privacy policy covers the use of the website <a href="#"> WWW.WORLDBOOKSTORE.ONLINE</a>
                Your use of the website confirms that you have read, understood and that you expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy. </li>

                <li> 
                If you do not agree please do not use or access <a href="#">WWW.WORLDBOOKSTORE.ONLINE</a>
                We respect your privacy and protecting your personal information is a high priority for us at <a href="#">WWW.WORLDBOOKSTORE.ONLINE</a>. We are governed by the Indian Law regarding collection, use and disclosure of the information collected from you.</li>
                
                <li>
                During the process of making a purchase at <a href="#">WWW.WORLDBOOKSTORE.ONLINE</a> we collect and store your personal information which is provided by you like name, email address, billing and shipping address, etc. which is essential for completing the sale transaction. This information is securely stores in your account until requested by you for removal. </li>
                
                <li>To complete the sale transaction, these details are shared with the seller who is responsible for delivery of the products.</li>
                
                <li>While registering at <a href="#">WWW.WORLDBOOKSTORE.ONLINE</a> you are required to provide your email id. On successful validation of the email id you must choose a new password of your choice which is known only to you.
                </li>
                
                <li>
                We do not disclose your personal information at an individual level to any third party without your consent. As applicable under law we are obliged to share customer specific information when sought by an agency authorized by the Government of India
                WORLD BOOKSTORE ONLINE holds the right to store usage statistics of customers and share it with third parties for marketing analytics and offers. </li>
                
                <li>Your information is adequately protected by security measures commensurate to the sensitivity of such information.
                </li>
                
                </ul>
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3 pad_none privacy-policy-img">
                <img src="/images/privacy-policy1-img.png" alt="" />
            </div>
            
        </div>
    </div>
</section>