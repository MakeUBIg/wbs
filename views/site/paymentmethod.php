<style type="text/css">
    .home-header-main-outer-panel{
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel banner-inner-pages-panel payment-method-banner">
 <div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
 <div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a>
 </div>
 </div>
</section>
<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
        <div class="col-xs-12 pad_none cont-payment-methods">
            <h2>You can make payments by following methods:</h2>
            <div class="col-xs-12 pad_none payment-list">
                <ul>
                    <li>All MasterCard & Visa Credit Cards</li>
                    <li>8 Major bank's EMI options SBI / ICICI / HDFC / Axis / Kotak / IndusInd / HSBC / Central Bank of India</li>
                    <li>100+ Debit Cards MasterCard / Visa / Maestro / RuPay</li>
                    <li>53+ Netbanking</li>
                    <li>4 Cash Cards ITZCash / ICash / Oxicash / PayCash</li>
                    <li>8 Wallets Paytm / MobiKwik / JioMoney / PayZapp / Jana Cash / Freecharge / SBI Buddy / The Mobile Wallet</li>
                    <li>46 Bank's IMPS MasterCard & Visa Credit Cards (International) American Express / Amex EMI, JCB & Diners Club</li>
                    <li>Libraries & Corporate Houses can make payment by Bank Transfer / Demand Draft / Cheque</li>
                </ul>
            </div>
        </div>
    </div>
</section>