
<style type="text/css">
    .home-header-main-outer-panel{
        height: 665px;
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        height: 230px!important;
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel banner-inner-pages-panel publisher-banner-outer">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>

</div>
</section>

<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods">
			<h2>Select Publisher Name Alphabet</h2>
			<div class="col-xs-12 pad_none publisher-content-part">
            	<div class="col-xs-12 pad_none publisher-content-first">
                     <a href="/site/publisher?name=a" target="_blank"><strong>A</strong></a>
                     <a href="/site/publisher?name=b" target="_blank"><strong>B</strong></a>
                     <a href="/site/publisher?name=c" target="_blank"><strong>C</strong></a>
                     <a href="/site/publisher?name=d" target="_blank"><strong>D</strong></a>
                     <a href="/site/publisher?name=e" target="_blank"><strong>E</strong></a>
                     <a href="/site/publisher?name=f" target="_blank"><strong>F</strong></a>
                     <a href="/site/publisher?name=g" target="_blank"><strong>G</strong></a>
                     <a href="/site/publisher?name=h" target="_blank"><strong>H</strong></a>
                     <a href="/site/publisher?name=i" target="_blank"><strong>I</strong></a>
                     <a href="/site/publisher?name=j" target="_blank"><strong>J</strong></a>
                     <a href="/site/publisher?name=k" target="_blank"><strong>K</strong></a>
                     <a href="/site/publisher?name=l" target="_blank"><strong>L</strong></a>
                     <a href="/site/publisher?name=m" target="_blank"><strong>M</strong></a>
                     <a href="/site/publisher?name=n" target="_blank"><strong>N</strong></a>
                     <a href="/site/publisher?name=o" target="_blank"><strong>O</strong></a>
                     <a href="/site/publisher?name=p" target="_blank"><strong>P</strong></a>
                     <a href="/site/publisher?name=q" target="_blank"><strong>Q</strong></a>
                     <a href="/site/publisher?name=r" target="_blank"><strong>R</strong></a>
                     <a href="/site/publisher?name=s" target="_blank"><strong>S</strong></a>
                     <a href="/site/publisher?name=t" target="_blank"><strong>T</strong></a>
                     <a href="/site/publisher?name=u" target="_blank"><strong>U</strong></a>
                     <a href="/site/publisher?name=v" target="_blank"><strong>V</strong></a>
                     <a href="/site/publisher?name=w" target="_blank"><strong>W</strong></a>
                     <a href="/site/publisher?name=x" target="_blank"><strong>X</strong></a>
                     <a href="/site/publisher?name=y" target="_blank"><strong>Y</strong></a>
                     <a href="/site/publisher?name=z" target="_blank"><strong>Z</strong></a>
                </div>
            </div>
		</div>
	</div>
</section>