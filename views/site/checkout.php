<?php
            
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use app\helpers\PriceHelper;
    use yz\shoppingcart\ShoppingCart;
    use app\modules\MubAdmin\modules\csvreader\models\Origin;
    use app\modules\MubAdmin\modules\csvreader\models\MagazinePrice;
    
    $user = new \app\models\MubUser();

?>
<style type="text/css">
	.home-header-main-outer-panel{
		height: 0px!important;
		background: : none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
</style>
<section class="header-main-outer-panel ">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>
<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->


<!--LOGIN CHECKOUT STRAT-->
<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods cont-payment-methods-add">
		<h2>Shopping Cart<samp><a href="/site/index">Continue Shopping</a></samp></h2></div>
			
			  
          <div class="col-xs-12 pad_none shopping-login-checkout-main">
          
          
          <div class="col-xs-12 pad_none shopping-cart-bottom-main">
          <div class="col-xs-12 col-sm-8 col-md-8 pad_none shopping-cart-left-main">
           <?php if(!empty($cartItems)){
                 foreach($cartItems as $item){
                 $itemCartId = $item['id'];
                 $itemCartName = $item['product_name'];
                 $quant = $item->getQuantity();
               ?>

         <div class="col-xs-12 pad_none shopping-cart-item-con">
           <div class="col-xs-3 pad_none shopping-item-pic">
              <img src="/images/logo-main.png" alt="pic" height="130" style="width: 200px!important;"></div>
           <div class="col-xs-4 pad_none shopping-item-details"><h3><?= $itemCartName;?></h3>
          <h4><?= $item['type'];?></h4><br/>

          QTY: <span style="padding-left: 5px; cursor: pointer;"><i class="fa fa-minus decrementcart" id="dec_<?=$itemCartId;?>"></i></span>
          <span id="mgQuant_<?= $itemCartId;?>"><?= $item->getQuantity();?></span>
          <span style="padding-right: 10px; cursor: pointer;"><i class="fa fa-plus incrementcart" id="inc_<?=$itemCartId;?>"></i></span>
                <?php
        $alreadyInCart = $cart->getPositionById($itemCartId);
        ?>
          <a class="btn-updateaddcart cusbutton" id="add-cart_<?= $itemCartId;?>" style="cursor: pointer;">Update</a>
          </div>
          <div class="col-xs-2 pad_none shopping-cart-quantity">$ <?= ceil($item->getPrice());?> x <?= $item->getQuantity();?></div>
          <div class="col-xs-2 pad_none shopping-cart-price">$ <?= $quant*(ceil($item->getPrice()));?><a class="btn-remove-item" id="remove_item_<?= $item['id'];?>"><i class="fa fa-trash pull-right" style="margin-top: 3px;"></i></a></div>
          </div>
          <?php }?>
         
          
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 pad_none shopping-cart-right-main">
          <div class="shopping-summary-outer">
          <h3>Price Details</h3>
          <div class="col-xs-12 pad_none shopping-subtotal-outer">Subtotal: <span>$ <?= ceil($cart->getCost());?></span></div>
          <div class="col-xs-12 pad_none shopping-subtotal-outer">Estimated Shipping: <span>$ 0.00</span></div>
          <div class="col-xs-12 pad_none shopping-subtotal-outer">Estimated tax: <span>$ 0.00</span></div>
          <div class="col-xs-12 pad_none shopping-subtotal-outer">Discount value: <span>$ 0.00</span></div>
          <div class="col-xs-12 pad_none shopping-subtotal-outer">Amount Payable: <span>$ <?= ceil($cart->getCost());?></span></div>
          <div class="col-xs-12 pad_none shopping-checkout-outer"><a href="/site/login-checkout"><i class="fa fa-lock"></i> Checkout</a></div>
          </div>
          </div><br />
         </div>
         </div>
             <?php }  else {?>

        <div class="container" style="background-color: #fff!important;">
          <div class="row">
            <center><h1 style="margin-left: 3em; margin-bottom: 1em;">Your Cart is Empty.</h1></center>
            </div>
        </div>
        <?php }?>

		</div>
	</div>
</section>
<!--LOGIN CHECKOUT SECTION END-->
