
<?php 
	use yii\web\NotFoundHttpException;
	use yii\data\Pagination;
	use yii\widgets\LinkPager;
	use app\modules\MubAdmin\modules\csvreader\models\Price;
	use app\modules\MubAdmin\modules\csvreader\models\ProductsAuthor;
	$authorProducts = new ProductsAuthor();
	use yz\shoppingcart\ShoppingCart;
	$cart = new ShoppingCart();
	$cartItems = $cart->getPositions();

    $name = Yii::$app->getRequest()->getQueryParam('name');

?>
<style type="text/css">
	.home-header-main-outer-panel{
		height: 0px!important;
		background: : none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
</style>
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
<!--  -->
</div>



<!--TABLE START-->
<section class="product-table-main-outer-panel">
<div class="col-xs-12 product-table-main-inner">
<div class="col-xs-12 pad_none product-table-con">

<!--TABS-->
<div class="col-md-12">
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
            
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane fade in active pagination-result  " id="general">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<th>Title</th>
				<th>AUTHOR / CONTRIBUTOR</th>
				<th>Format</th>
				<th>Bind</th>
				<th>PUBLISHER / SUPPLIER</th>
				<th>PUBLISHED DATE</th>
				<th>MRP</th>
			
				<th>&nbsp;</th>
				</tr>
				<?php
					foreach($products as $book)
					{

					$dateAndTime = $book['publish_date'];
					$authorName = $authorProducts::find()->where(['ing_product_id' => $book['id']])->one();

					$date = date('M Y', strtotime($dateAndTime));
					$alreadyInCart = $cart->getPositionById($book['id']);
				?>

				<tr>
				<td class="Product-detail-title-con"><a href="/site/bookdetail?name=<?= $book['slug'];?>"><?= isset($book['product_name'])? $book['product_name']: 'NA';?> 
				<span class="product-tooltip-text"><?= isset($book['product_name'])? $book['product_name']: 'NA';?></span></a> 
				</td>
				<td><?= isset($authorName->author_key)? ucfirst($authorName->author_key): 'NA';?></td>
				<td><?= isset($book['format'])? $book['format']: 'NA';?></td>
				<td><?= isset($book['type'])? $book['type']: 'NA';?></td>
				<td><?= $publisherName;?></td>
				<td><?= isset($date)? $date: 'NA';?></td>
				<td>$ <?= ceil($book['price']);?></td>

				<?php if(empty($alreadyInCart)){?>
				<td class="product-add-to-cart-button"><a style="cursor: pointer;" class="btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $book['id']?>">Add to Cart</a></td>
				<?php } else {?>
				<td class="product-add-to-cart-button"><a href="/site/checkout">Checkout</a></td>
				<?php }?>
				</tr>
				<tr>

				<?php } ?>	

				</table>
				 <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
                 'pagination' => $pages,
                 ]);
                ?></center>
    			</div>
       			</div>
       		</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>