<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
    $user = Yii::$app->user->identity;
?>
<style type="text/css">
.login-register-customer .shopping-cart-left-inner{
padding: 10px!important;}
.has-error .help-block
{
  color: #fff!important;
  margin-top: 10px!important;
}
</style>
<div class="col-xs-12 col-sm-12 col-md-12 nopadding shopping-cart-left-main shopping-login-right-outer login-register-customer">
           <div class="col-xs-12 nopadding shopping-cart-left-inner">

           <?php $form = ActiveForm::begin(['options' => ['id' => 'checkout-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
           <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'shopping-cart-left-main shopping-login-checkout-fields input'])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'shopping-cart-left-main shopping-login-checkout-fields input'])->label(false); ;?>

   
           <div class="col-xs-12 pad_none product-advertise-form-inner login-add-sec"><input type="submit" name="login" value="Sign In"></div>
           <?php ActiveForm::end(); ?>
          </div>
         </div>