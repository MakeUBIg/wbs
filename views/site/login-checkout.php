<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
    $user = Yii::$app->user->identity;
?>
<style type="text/css">
	.home-header-main-outer-panel{
		height: 0px!important;
		background: : none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
  input{
    width: 100%!important;
  }
</style>
<section class="header-main-outer-panel ">

<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>

<!--LOGIN CHECKOUT STRAT-->
<section class="inner-pages-outer login-checkout2">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods cont-payment-methods-add">
			<h2>Sign In <samp> <a href="/site/checkout">Back to Cart</a></samp> </h2>
            
          <div class="col-xs-12 nopadding shopping-login-checkout-main">
          <?php if (Yii::$app->user->isGuest) {?>
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main">
          <div class="col-xs-12 nopadding shopping-cart-left-inner">
          <h3>You must have registered for placing order.</h3>
          
          <div class="shopping-login-con" style="display: none;">
            <input type="radio" id="guest" name="cc" />
            <label for="guest">Checkout as Guest</label>
            </div>
            	
           <div class="shopping-login-con">
            <input type="radio" id="register" name="cc" checked=""/>
            <label for="register">Register</label>
            </div> 
            <a href="/site/client-register"><div class="col-xs-12 pad_none product-advertise-form-inner login-ad2"><input type="submit" name="Register" value="Continue"></div></a>
                      
          </div>
          </div>
           <?php }?>
          <?php if (Yii::$app->user->isGuest) {?>
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main shopping-login-right-outer login-register-customer">
           <div class="col-xs-12 nopadding shopping-cart-left-inner">
           <h3>Registered Customers</h3>
           <p>All Registered customers can login here!</p>
           <?php $form = ActiveForm::begin(['options' => ['id' => 'checkout-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
           <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Username", 'class' => 'shopping-cart-left-main shopping-login-checkout-fields input'])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Password", 'class' => 'shopping-cart-left-main shopping-login-checkout-fields input'])->label(false); ;?>

   
           <div class="col-xs-12 pad_none product-advertise-form-inner login-add-sec"><input type="submit" name="login" value="Sign In"></div>
           <?php ActiveForm::end(); ?>
          </div>
         </div>
         </div>
          <?php } else {?>
          <div class="col-xs-12 col-sm-12 col-md-12 nopadding shopping-cart-left-main shopping-login-right-outer">
             <div class="col-xs-12 nopadding shopping-cart-left-inner">
             <h3 style="text-align: center;">Registered Customers</h3>
             <h2 style="color: #fff; text-align: center;">Logged in as <?= $user->username;?>.</h2><br/>
              <center><a href="/site/payment"><div class="col-xs-12 pad_none product-advertise-form-inner login-add-sec"><input type="submit" name="login" value="Payment" style="width: 30%!important;"></div></a></center>
             <?php }?>

            
            
             
			
		</div>
	</div>
</section>
<!--LOGIN CHECKOUT SECTION END-->

