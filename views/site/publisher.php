<?php
use yii\data\Pagination;
use yii\widgets\LinkPager;


 $name = Yii::$app->getRequest()->getQueryParam('name');
?>
<style type="text/css">
    .home-header-main-outer-panel{
        height: 665px;
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        height: 230px!important;
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel banner-inner-pages-panel publisher-banner-outer">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>

</div>
</section>

<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods">
			<h2>PUBLISHERS</h2>
			<div class="col-xs-12 pad_none publisher-content-part">
            	<div class="col-xs-12 pad_none publisher-content-first">
                     <strong><?= strtoupper($name);?></strong>
                     <ul>
                        <?php foreach($allPublisher as $value){
                            ?>
                        <a href="/site/publisher-books?name=<?= $value['slug'];?>"><li><?= $value['publisher'];?></li></a>
                        <?php }?>
                     </ul>
                 </div>
                        <div class="row"><center><?= LinkPager::widget([
                                 'pagination' => $pages,
                                 ]);
                                ?>  
                        </center>   </div>
            </div>
		</div>
	</div>
</section>