
<?php 
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use yii\widgets\LinkPager;
	use yz\shoppingcart\ShoppingCart;
	$cart = new ShoppingCart();
	$cartItems = $cart->getPositions();

use app\modules\MubAdmin\modules\csvreader\models\PublisherProducts;
$publisherProducts = new PublisherProducts();
use app\modules\MubAdmin\modules\csvreader\models\ProductsAuthor;
$authorProducts = new ProductsAuthor();
$name = Yii::$app->getRequest()->getQueryParam('name');

?>

<input type="hidden" value="<?= $name;?>" id="categoryId">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
<div class="col-md-8 col-sm-8 col-xs-12 product-banner-right-heading"><h2 id="pageHeading"><?= $oneBisac['bisac_title'];?></h2></div>
</div>



<!--TABLE START-->
<section class="product-table-main-outer-panel">
<div class="col-xs-12 product-table-main-inner">
<div class="col-xs-12 pad_none product-table-con">

<!--TABS-->
<div class="col-md-12">
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="pill" href="#newest_tab" id="newest" class="sortby">Newest</a></li>
                <li><a data-toggle="pill" href="#plth_tab" id="plth" class="sortby">Price: Low - High</a></li>
                <li><a data-toggle="pill" href="#phtl_tab" id="phtl" class="sortby">Price: High - Low</a></li>
                <li><a data-toggle="pill" href="#nasc_tab" id="nasc" class="sortby">Name: Z - A</a></li>
                <li><a data-toggle="pill" href="#ndsc_tab" id="ndsc" class="sortby">Name: A - Z</a></li>
            </ul>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane fade in active pagination-result" id="newest_tab">

            </div>

             <div class="tab-pane fade pagination-result sortdata" id="plth_tab">
                
            </div>

             <div class="tab-pane fade pagination-result sortdata" id="phtl_tab">
                
            </div>

             <div class="tab-pane fade pagination-result sortdata" id="nasc_tab">
                
            </div>

             <div class="tab-pane fade pagination-result sortdata" id="ndsc_tab">
                
            </div>

</div>
</div>
</div>
</div>
</div>
</div>
</section>
<?php $this->registerJs("
    $(document).on('click','.pagination li a',function(e){
    e.preventDefault();
    var dataUrl = $(this).attr('href');
    var selectedTab = $(this).parents('.pagination-result').attr('id');
        $.ajax({
         type:'GET',
         url: dataUrl,
         data:{'selectedTab':selectedTab},
         success: function(result)
            {   
               $('#'+selectedTab).html(result);
            }
        });
        return false;
});

$(document).ready(function(){
    $('#newest').click();
});
"); ?>