<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">
  .home-header-main-outer-panel{
    height: 665px;
    background: none!important;
  }
  .text_upper{
     text-transform: uppercase;
  }
  @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
  {
  .home-header-main-outer-panel{
    height: 230px!important;
    background: none!important;
  } 
  }

  @media (min-width: 1200px)
  {
      .col-lg-7 {
          width: 99.333333%!important;
      } 
    }
</style>
<section class="header-main-outer-panel ">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>


<section class="inner-pages-outer register-section2">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
          <div class="col-xs-12 nopadding shopping-login-checkout-main">
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main shopping-login-right-outer login-register-customer">
           <div class="col-xs-12 nopadding shopping-cart-left-inner">
      <img src="/images/register-logo2.png" alt="" />
            <div class="col-xs-12 pad_none other-links" style="margin-top: -22px!important;">
        <ul>
          <li><a style="font-size: 30px;">Forgot Password</a></li>
        </ul>
      </div>
       <div class="col-xs-12 form-section2">

            <?php $form = ActiveForm::begin([
                    'id' => 'frontend-forget',
                    'options' => ['class' => 'form-horizontal login-form'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-12 control-label padbt'],
                    ],
                ]); ?>
       
           <div class="col-xs-12 pad_none product-advertise-form-inner shopping-login-checkout-fields" style="margin-bottom: -1em!important; margin-top: 35px;">

         <?= $form->field($model, 'email')->textInput(['placeholder' => 'Your Registered Email',])->label(false);
                ?>
       </div>
       
           <div class="col-xs-12 pad_none product-advertise-form-inner">
        <input type="submit" value="Submit" class="button-login"/>
       </div>
       <?php ActiveForm::end(); ?>
        <div class="col-xs-12 pad_none other-links">
        <ul>
          <li><a href="/site/client-register">Not a members ? Register</a></li>
        </ul>
      </div>
          </div>
          </div>
         </div>
         </div>
  </div>
</section>