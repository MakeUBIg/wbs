<style type="text/css">
    /*.home-header-main-outer-panel{
        height: 0px;
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }*/
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        height: 0px!important;
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel home-header-main-outer-panel about-us-sec">
    <div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="index.html"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>


</section>



<section class="about-slider">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!-- <ol class="carousel-indicators"> -->
    <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    <!-- <li data-target="#myCarousel" data-slide-to="1"></li> -->
    <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->
  <!-- </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active about-banner01">
      
    </div>
    <div class="item about-banner01 about-banner02">
     
    </div>

    <div class="item about-banner01 about-banner03">
     
    </div>
    <div class="item about-banner01 about-banner04">
     
    </div>
    <div class="item about-banner01 about-banner05">
     
    </div>
    
    
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    
</section>




<section class="wide-range-books-main">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    <strong>Welcome to World Bookstore Online! </strong>
                    <h2>Wide Range Of Books!</h2>
                    <p>Whether you are around the corner or around the world, World Bookstore Online offers more than 10 million titles, speedy delivery, 
                    and unmatched service for all that you need - Reference Books, Text Books, School Books, Dictionaries, Directories, Encyclopedias, Maps, Atlases.
                    You can find Books on wide range of subjects.</p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>

<section class="wide-range-books-main service-online2">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    
                    <h2>24x7 Customer Service Online!</h2>
                    <p>With focus on streamlining customer and distribution services we make sure we deliver publications, whatever you want, wherever you want, whenever you want. When you engage us you not just get Books delivered on time, we make sure you get maximum value as well.</p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>

<section class="wide-range-books-main team-Professionals">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    
                    <h2>Team Of Professionals To Assist You!</h2>
                    <p>To serve you better, we have built a team of professionals with innovative processes and developed a distribution system which thrives on what you want the most - pricing, quickness, efficiency and accuracy.</p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>


<section class="wide-range-books-main we-take-care">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    
                    <h2>We Take Care Of Each Customer!</h2>
                    <p>World Bookstore Online caters to the educational needs of everyone, from Individuals, to public and private educational Institutes, Colleges, Universities, Research Centres, Polytechnics, Schools, Hospitals, Libraries and Corporate; we meet all your educational requirements</p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>

<section class="wide-range-books-main publications-from">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    
                    <h2>Publications From Worldwide Publishers!</h2>
                    <p>World Bookstore Online provides you publications from publishing houses in Algeria, Andorra, Argentina, Australia, Austria, Bangladesh, Barbados, Belgium, Bermuda, Brazil, Bulgaria, Cambodia, Canada, Canary Islands, Chile, China, Colombia, Cote d'Ivoire, Croatia, Cyprus, Czech Republic, Denmark, Ecuador, Egypt, Estonia, Finland, France, French Polynesia, Germany, Ghana, Greece, Guatemala, Hong Kong, Hungary, Iceland, India, Indonesia, Iran, Ireland, Italy, Jamaica, Japan, Jordan, Kenya, Korea, Latvia, Lebanon, Lithuania, Luxembourg, Macedonia, Malaysia, Malta, Mexico, Netherlands, New Zealand, Nigeria, Norway, Peru, Philippines, Poland, Portugal, Romania, Russia, Senegal, Serbia, Singapore, Slovakia, Slovenia, South Africa, Spain, Sri Lanka, Sweden, Switzerland, Taiwan, Thailand, Trinidad and Tobago, Tunisia, Turkey, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States, Uruguay, Venezuela, Zimbabwe. For receiving Catalogue or list of publications from any publishers, from any country, kindly email Customer Service. </p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>


<section class="wide-range-books-main dedicated-customer-services">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    <strong>Dedicated Customer Service Manager</strong>
                    <h2>For Libraries & Corporate Orders</h2>
                    <p>By offering a holistic environment full of challenges and targeting individual growth we help our associates grow not only professionally but personally also. The trust of our customers has helped us move ahead rapidly, confidently and successfully in our mission to evolve into the market leaders in customer and distribution services </p>
                    <a href="#" title="">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
</section>

<section class="wide-range-books-main order-books">
        <div class="container">
            <div class="wide-range-books-inner wow fadeInDown" style="visibility: visible;">
                <div class="wide-range-books-first">
                    <h2>You can order books in various languages!</h2>
                    <p>World Bookstore Online offers publications in English, French, German & Italian Languages, besides these we can fulfil your requirements for publications in other languages i.e. Afrikaans, Afro-asiatic, Amharic, Arabic, Aramaic, Armenian, Bulgarian, Catalan, Chinese, Croatian, Czech, Danish, Dutch, English, Finnish, French, Friesian, German, Greek, Hawaiian, Hebrew, Hungarian, Icelandic, Indonesian, Italian, Japanese, Javanese, Korean, Latin, Norwegian, Polish, Portuguese, Romanian, Russian, Sanskrit, Slovak, Spanish, Swedish, Thai, Urdu, Vietnamese, Welsh.</p>
                    <a href="#" title="">Shop online or email us your requirements for Books!</a>
                    <span class="glad-your-service-text">Glad to be at your service!</span>
                </div>
            </div>
        </div>
</section>





<!--HEADER END-->



<!--Shop online or email us starts-->


	
