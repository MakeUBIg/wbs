<style type="text/css">
	.home-header-main-outer-panel{
		height: 665px;
		background: none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
	@media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
	{
	.home-header-main-outer-panel{
		height: 230px!important;
		background: none!important;
	}	
	}
</style>
<section class="header-main-outer-panel banner-inner-pages-panel contact-banner">
<header>

<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

<!-- <div class="banner-search-cont inner-pages-search"> -->
	<!-- <div class="col-xs-12 pad_none wbo-searchbar-inner"> -->
		<!-- <div class="col-sm-4 col-md-4 col-xs-4 pad_none wbo-searchbar-select"> -->
			<!-- <select> -->
			<!-- <option>Title</option> -->
			<!-- <option>Author</option> -->
			<!-- <option>ISBN</option> -->
			<!-- <option>Publisher</option> -->
			<!-- <option>Subject</option> -->
			<!-- <option>Topic</option> -->
			<!-- </select> -->
		<!-- </div> -->
		<!-- <div class="col-sm-8 col-md-8 col-xs-8 pad_none  wbo-search-con-outer"> -->
			<!-- <div class="col-xs-12 pad_none  wbo-search-con"> -->
				<!-- <input name="" placeholder="Search by Title, Author, ISBN, Publisher" type="text"> -->
				<!-- <button value="submit"><i class="fa fa-search"></i></button> -->
			<!-- </div>  -->
		<!-- </div> -->
	<!-- </div> -->
<!-- </div> -->

<h1><span>&nbsp;</span></h1>
</header>
<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->


<!--FAQ ACCORDION SECTION STRAT-->
<section class="inner-pages-outer contact-page">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods">
			<h2>Write to us!</h2>
			
			<div class="col-xs-12 pad_none register-content-otr">
				<div class="col-md-8 col-xs-12 bookstore-benefits">
					<div class="col-xs-12 pad_none register-form-otr">
						<div class="col-xs-12 pad_none create-world-bookstore-account-inner">
							<form action="#" method="get">
								<input name="" placeholder="Name*" type="text">
								<input name="" placeholder="Designation" type="text">
								<input name="" placeholder="Department" type="text">
								<input name="" placeholder="Organization" type="text">
								<input name="" placeholder="Address*" type="text">
								<input name="" placeholder="City*" type="text">
								<input name="" placeholder="Zip / Pin code*" type="text">
								<input name="" placeholder="State" type="text">
								<select>
									<option>Country</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
								<input name="" placeholder="Telephone" type="text">
								<input name="" placeholder="Email*" type="text">
								<input name="" placeholder="Website" type="text">
								<input name="" placeholder="Mobile*" type="text">
								<textarea placeholder="Write your message"></textarea>
								<button type="submit">Submit Now</button>
							</form>
						</div>
					</div>
				</div>
				

				<div class="col-md-4 col-xs-12 right-selection">
					<div class="col-xs-12 pad_none office-building">
						<img src="/images/category-banner-bg-new.jpg" alt="Image">
					</div>
					<div class="make-selection cont-address">
						<div class="col-xs-12 pad_none contact-address-otr">
							<h6>WORLD BOOKSTORE</h6>
							<p>Level 3 & 7 | The Capital<br> Plot No : C-70, G-Block <br> Bandra Kurla Complex <br> Bandra (East), Mumbai, 400051 <br> Maharashtra, India</p>
							<h6>Customer service</h6>
							<p><a href="tel:9773723591">+91 - 9773723591</a> <br> <a href="tel:9773723592">+91 - 9773723592</a></p>
							<p><a href="callto:Contact@Worldbookstore.online">Contact@Worldbookstore.online</a></p>
						</div>						
					</div>
					<div class="col-xs-12 pad_none social-links">
						<a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a target="_blank" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--FAQ ACCORDION SECTION END-->