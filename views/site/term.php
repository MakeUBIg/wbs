<style type="text/css">
    .home-header-main-outer-panel{
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        background: none!important;
    }   
    }
</style>
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>
<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
        <div class="col-xs-12 pad_none cont-payment-methods">
            <h2>Terms of use</h2>
            <div class="col-xs-12 col-md-9 col-sm-9 pad_none payment-list terms-use">
            <p>Use of <a href="http://www.worldbookstore.online/">WWW.WORLDBOOKSTORE.ONLINE</a> website is bound by the following Terms and Conditions (“Terms”). 
            Your use of the “Service” confirms that you have read, understood and agreed to be bound by these “Terms” and to comply with all applicable laws and regulations. </p>
            

            <p>WORLD BOOKSTORE ONLINE reserves the right to update or modify these “Terms” at any time without prior notice. Your use of the “Service” following any such change constitutes your agreement to follow and be bound by the “Terms” as they are and as and when they the are changed. These “Terms” constitute a binding “Agreement” between you and WORLD BOOKSTORE ONLINE</p>
            
                <p>You may not use the “Service”, if you do not agree to these “Terms”.</p>
                <p>The “Agreement” constitutes the “Terms” as detailed herein including all Annexures, Privacy Policy, Licenses and Disclaimers posted on the “Service” and as amended, supplemented, varied or replaced from time to time.
                The “Service” allows you to register to gain access to restricted content accessible only to registered users. 
                While registering with <a href="http://www.worldbookstore.online/">WWW.WORLDBOOKSTORE.ONLINE</a>, you should provide true and accurate data which includes your address, personal details, billing information, etc. The accountability for maintaining and updating this data lies solely with you. </p>
                <p>WORLD BOOKSTORE ONLINE reserves the right of terminating the account for untrue or inaccurate data at any time if found or proved. 
                You agree to be fully responsible for maintaining the confidentiality of the password used to access the “Service” related account information. </p>
                <p>You agree to immediately notify <a href="http://www.worldbookstore.online/">WWW.WORLDBOOKSTORE.ONLINE</a> of any unauthorized use of your password or account or any other breach of security.</p>
                <p>You certify that you have the full authority to transact on the website and that you are over 18 years of age.
                WORLD BOOKSTORE ONLINE owns the “Service” and shall retain all intellectual property rights of content published on the website which is proprietary property of WORLD BOOKSTORE ONLINE or its content provider. 
                Purchase of products on this site does not transfer any of the intellectual property related to the products, their design or concepts to you. We do not grant you any licenses, express or implied, to the intellectual property of WORLD BOOKSTORE ONLINE.</p>
                <p>You shall be solely responsible for and be subject to legal actions for any illegal activities, frauds, deliberate misrepresentations and omissions done by you while accessing or transacting on the website.
                WORLD BOOKSTORE ONLINE is a distributor and not a publisher or a manufacturer or a service provider of the products and services (collectively referred to as “Products”) sold on the website.  These are delivered by the respective third parties. Hence MAGAZINES WORLD ONLINE takes no responsibility for the accuracy and reliability of the information (such as content, articles, opinions, advice, statements offers etc..) contained in such “Products” or the website and quality of the “Products” sold.</p>
            <p>WORLD BOOKSTORE ONLINE shall not be responsible for any loss or damage or consequences that arise from the use of the “Products” or by your reliance on information obtained through the “Service” or from the “Products”. 
            You agree to be fully responsible for making your own judgment of the safety and risks associated with the use of various “Products” that may be sold and their subsequent use. The content of the “Service” may contain materials that are created by suppliers, vendors and others who sell their products and services. 
            If any of the material is found to be objectionable or is found to have violated any law or copyright, the responsibility of this would be with the producer of the materials and WORLD BOOKSTORE ONLINE will not be liable or responsible for any of the content. Any issues relating to this would need to be dealt with the original producer of the content and materials
            The website may contain links to other Internet sites and third-party resources and WORLD BOOKSTORE ONLINE does not assume any responsibility or liability for communications or materials available at such linked sites. These links are provided for your convenience only. </p>
                <p>You are solely responsible for understanding any terms and conditions that may apply when you visit or place an order through a third-party site.</p>
                <p>While effort is made to ensure accuracy of information related to “Products”, should there be any unintentional errors relating to the “Products”, pricing, availability, shipping and product supply, WORLD BOOKSTORE ONLINE will make good by correcting the amount (if incorrectly charged) and will have the right to make changes to the website to correct the errors without any obligation to supply the product or service at the previously published incorrect price or description
                Delivery of “Products” purchased through the “Service” is the responsibility of the seller on whom the order is finally placed. The shipment method is indicated at the time of placing the order. 
                If for any reason the seller decides to ship the “Products” using an alternate method, no additional charge shall be applicable.</p>
                <p>The visual representation of the “Products” on the website are indicative and may vary from the actual product’s color, size and content.</p>
                <p>“Sellers” may offer special promotional offers from time to time. Such offers are governed by the offer specific terms and conditions of the respective seller / respective offer. </p>
                <p>Purchaser shall pay all sales related taxes as applicable.
                Your satisfaction is very important to us. However, cancellations and refunds are not permitted by most sellers and we are bound by their policies. </p>
                <p>We will try to offer you pro-rated refund for cancellation, subject to the seller’s policy, only if the cancellation request is received by us within 24 hours of placing the order. 
                No cancellation shall be permitted after 24 hours. </p>
                <p>All “Products” are provided on an “as is” and “as available” basis. WORLD BOOKSTORE ONLINE expressly disclaims all warranties of any kind, whether express or implied, including but not limited to the implied warranties of merchantability, fitness for a purpose and non-infringement. </p>
                <p>Without limiting the foregoing, WORLD BOOKSTORE ONLINE does not make any warranty that (i) the service will meet your requirements, (ii) the service will be uninterrupted, timely, secure, or error-free, (iii) the information that may be obtained from the use of the “Service” or that may be contained in the “Products” will be accurate or reliable, or (iv) the quality of any “Products” will meet your expectations. </p>
                <p>Any material downloaded or otherwise obtained through the use of the service is done at your own discretion and risk and you will be solely responsible for any damage to your computer system or mobile device or loss of data that results from the download of any such material. </p>
                <p>No advice or information, whether oral or written, obtained by you from WORLD BOOKSTORE ONLINE or from the website shall create any warranty.</p>
                <p>The liability of WORLD BOOKSTORE ONLINE under this agreement is limited to the amount paid by you to WORLD BOOKSTORE ONLINE for the purchase of the “Products” giving rise to the claim.  
                Under no circumstances shall WORLD BOOKSTORE ONLINE be liable for any indirect, incidental, punitive, or consequential damages incurred by you for any reason related to the purchase or intended purchase of “Products” on the website.</p>
                <p>WORLD BOOKSTORE ONLINE will not be responsible for any liability arising out of the conduct of its employees, associates, contractors and suppliers that may cause any financial or other loss to you. 
                WORLD BOOKSTORE ONLINE has the right to assign or transfer all or part of its rights or obligations under this Agreement without prior notification
                In the event of any disputes or differences between the Parties hereto, the Parties shall explore all possibilities for an amicable settlement. </p>
                <p>All notices, demands or other communications required or permitted to be given or made under or about this “Agreement “shall be in writing and shall be sufficiently given or made (i) if delivered by hand or (ii) sent by pre-paid registered post addressed to WORLD BOOKSTORE ONLINE.</p>
                
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3 pad_none privacy-policy-img"> 
                <img src="/images/terms-of-use-img.png" alt="" /> 
             </div>
            
        </div>
    </div>
</section>