<?php 
use app\modules\MubAdmin\modules\csvreader\models\Product;
use app\modules\MubAdmin\modules\csvreader\models\Price;
use yz\shoppingcart\ShoppingCart;
use app\modules\MubAdmin\modules\csvreader\models\PublisherProducts;
$publisherProducts = new PublisherProducts();
use yii\widgets\LinkPager;
$cart = new ShoppingCart();
use app\modules\MubAdmin\modules\csvreader\models\ProductsAuthor;
$authorProducts = new ProductsAuthor();
use app\helpers\PriceHelper;

$cartItems = $cart->getPositions();

?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <th>Title</th>
        <th>AUTHOR / CONTRIBUTOR</th>
        <th>Format</th>
        <th>Bind</th>
        <th>PUBLISHER / SUPPLIER</th>
        <th>PUBLISHED DATE</th>
        <th>MRP</th>
        <th>&nbsp;</th>
        </tr>
        <?php
          foreach($getAllSubject as $book)
          {
          $dateAndTime = $book['publish_date'];
          $date = date('M Y', strtotime($dateAndTime));
          $ean = $book['id'];
          $publisherName = $publisherProducts::find()->where(['ing_product_id' => $ean])->one();
          $publisher = str_replace('-', ' ', $publisherName->publisher_key);
          $alreadyInCart = $cart->getPositionById($book['id']);
          $authorName = $authorProducts::find()->where(['ing_product_id' => $ean])->one();
        ?>

        <tr>
        <td class="Product-detail-title-con"><a href="/site/bookdetail?name=<?= $book['slug'];?>"><?= isset($book['product_name'])? $book['product_name']: 'NA';?> 
        <span class="product-tooltip-text"><?= isset($book['product_name'])? $book['product_name']: 'NA';?></span></a> 
        </td>
        <td><?= isset($authorName->author_key)? ucfirst($authorName->author_key): 'NA';?></td>
        <td><?= isset($book['format'])? $book['format']: 'NA';?></td>
        <td><?= isset($book['type'])? $book['type']: 'NA';?></td>
        <td><?= ucfirst($publisher);?></td>
        <td><?= isset($date)? $date: 'NA';?></td>
        <td>$ <?= ceil($book['price']);?></td>
        
        <?php if(empty($alreadyInCart)){?>
        <td class="product-add-to-cart-button"><a style="cursor: pointer;" class="btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $book['id'];?>">Add to Cart</a></td>
        <?php } else {?>
        <td class="product-add-to-cart-button"><a href="/site/Checkout">Checkout</a></td>
        <?php }?>
        </tr>
        <tr>

        <?php } ?> 

        </table>
         <div class="row"><div class="col-md-2"></div><div class="col-md-8"><center><?= LinkPager::widget([
            'pagination' => $pages,
          ]);
          ?></center>
          </div>
            </div>