<style type="text/css">
	.home-header-main-outer-panel{
		height: 665px;
		background: none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
	@media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
	{
	.home-header-main-outer-panel{
		height: 230px!important;
		background: none!important;
	}	
	}
</style>
<section class="header-main-outer-panel banner-inner-pages-panel register-banner">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>

</div>
</section>
<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->


<!--LOGIN CHECKOUT STRAT-->


<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods">
			<h2>Register with World Bookstore for all your books requirements!</h2>
			<h5 class="position-center">I would like to receive Catalog / List of books, please keep me updated with latest information on following subjects area:</h5>
			<div class="col-xs-12 pad_none register-content-otr">
				<div class="col-md-8 col-xs-12 bookstore-benefits">
					<h3>Benefits of World Book Store</h3>
					<p>Whether you are around the corner or around the world, World Bookstore Online offers more than 10 million titles, speedy delivery, and unmatched service for all that you need………… Reference Books, Text Books, School Books, Dictionaries, Directories, Encyclopaedias, Maps, Atlases…………….Log on to www.worldbookstore.online</p>
					<p>With focus on streamlining customer and distribution services we make sure we deliver publications, whatever you want, wherever you want, whenever you want. When you engage us you not just get Books delivered on time, we make sure you get maximum value as well.</p>
					<p>To serve you better, we have built a team of professionals with innovative processes and developed a distribution system which thrives on what you want the most - pricing, quickness, efficiency and accuracy.</p>
					<p>World Bookstore Online caters to the educational needs of everyone, from Individuals, to public and private educational Institutes, Colleges, Universities, Research Centres, Polytechnics, Schools, Hospitals, Libraries and Corporate; we meet all your educational requirements.</p>
					<p>By offering a holistic environment full of challenges and targeting individual growth we help our associates grow not only professionally but personally also. The trust of our customers has helped us move ahead rapidly, confidently and successfully in our mission to evolve into the market leaders in customer and distribution services.</p>
					<p>World Bookstore Online offers publications in English, French, German & Italian Languages, besides these we can fulfil your requirements for publications in other languages i.e Afrikaans, Afro-asiatic, Amharic, Arabic, Aramaic, Armenian, Bulgarian, Catalan, Chinese, Croatian, Czech, Danish, Dutch, English, Finnish, French, Friesian, German, Greek, Hawaiian, Hebrew, Hungarian, Icelandic, Indonesian, Italian, Japanese, Javanese, Korean, Latin, Norwegian, Polish, Portuguese, Romanian, Russian, Sanskrit, Slovak, Spanish, Swedish, Thai, Vietnamese, Welsh.</p>
					<div class="col-xs-12 pad_none register-form-otr">
						<div class="col-xs-12 pad_none create-world-bookstore-account-inner">
							<form action="#" method="get">
								<input name="" placeholder="Name*" type="text">
								<input name="" placeholder="Designation" type="text">
								<input name="" placeholder="Department" type="text">
								<input name="" placeholder="Organization" type="text">
								<input name="" placeholder="Address*" type="text">
								<input name="" placeholder="City*" type="text">
								<input name="" placeholder="Zip / Pin code*" type="text">
								<input name="" placeholder="State" type="text">
								<select>
									<option>Select Country</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
								<input name="" placeholder="Telephone" type="text">
								<input name="" placeholder="Email*" type="text">
								<input name="" placeholder="Website" type="text">
								<input name="" placeholder="Mobile*" type="text">
								<textarea placeholder="Write your message"></textarea>
								<button type="submit">Submit Now</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 right-selection">
					<div class="col-xs-12 pad_none make-selection add-checkbox">
						<h5>Please make your selection</h5>
						<p><input id="c1" name="cc" type="checkbox"><label for="c1">Antiques & Collectibles</label></p>
						<p><input id="c2" name="cc" type="checkbox"><label for="c2">Architecture</label></p>
						<p><input id="c3" name="cc" type="checkbox"><label for="c3">Art</label></p>
						<p><input id="c4" name="cc" type="checkbox"><label for="c4">Biography & Autobiography</label></p>
						<p><input id="c5" name="cc" type="checkbox"><label for="c5">Body, Mind & Spirit</label></p>
						<p><input id="c6" name="cc" type="checkbox"><label for="c6">Business & Economics</label></p>
						<p><input id="c7" name="cc" type="checkbox"><label for="c7">Comics & Graphic Novels</label></p>
						<p><input id="c8" name="cc" type="checkbox"><label for="c8">Computers</label></p>
						<p><input id="c9" name="cc" type="checkbox"><label for="c9">Cooking</label></p>
						<p><input id="c10" name="cc" type="checkbox"><label for="c10">Crafts & Hobbies</label></p>
						<p><input id="c11" name="cc" type="checkbox"><label for="c11">Design</label></p>
						<p><input id="c12" name="cc" type="checkbox"><label for="c12">Drama</label></p>
						<p><input id="c13" name="cc" type="checkbox"><label for="c13">Education</label></p>
						<p><input id="c14" name="cc" type="checkbox"><label for="c14">Family & Relationships</label></p>
						<p><input id="c15" name="cc" type="checkbox"><label for="c15">Fiction</label></p>
						<p><input id="c16" name="cc" type="checkbox"><label for="c16">Foreign Language Study</label></p>
						<p><input id="c17" name="cc" type="checkbox"><label for="c17">Games</label></p>
						<p><input id="c18" name="cc" type="checkbox"><label for="c18">Gardening</label></p>
						<p><input id="c19" name="cc" type="checkbox"><label for="c19">Health & Fitness</label></p>
						<p><input id="c20" name="cc" type="checkbox"><label for="c20">History</label></p>
						<p><input id="c21" name="cc" type="checkbox"><label for="c21">House & Home</label></p>
						<p><input id="c22" name="cc" type="checkbox"><label for="c22">Humor</label></p>
						<p><input id="c23" name="cc" type="checkbox"><label for="c23">Juvenile Fiction</label></p>
						<p><input id="c24" name="cc" type="checkbox"><label for="c24">Juvenile Nonfiction</label></p>
						<p><input id="c25" name="cc" type="checkbox"><label for="c25">Language Arts & Disciplines</label></p>
						<p><input id="c26" name="cc" type="checkbox"><label for="c26">Literary Collections</label></p>
						<p><input id="c27" name="cc" type="checkbox"><label for="c27">Literary Criticism</label></p>
						<p><input id="c28" name="cc" type="checkbox"><label for="c28">Mathematics</label></p>
						<p><input id="c29" name="cc" type="checkbox"><label for="c29">Medical</label></p>
						<p><input id="c30" name="cc" type="checkbox"><label for="c30">Music</label></p>
						<p><input id="c31" name="cc" type="checkbox"><label for="c31">Nature</label></p>
						<p><input id="c32" name="cc" type="checkbox"><label for="c32">Performing Arts</label></p>
						<p><input id="c33" name="cc" type="checkbox"><label for="c33">Pets</label></p>
						<p><input id="c34" name="cc" type="checkbox"><label for="c34">Philosophy</label></p>
						<p><input id="c35" name="cc" type="checkbox"><label for="c35">Poetry</label></p>
						<p><input id="c36" name="cc" type="checkbox"><label for="c36">Political Science</label></p>
						<p><input id="c37" name="cc" type="checkbox"><label for="c37">Psychology</label></p>
						<p><input id="c38" name="cc" type="checkbox"><label for="c38">Reference</label></p>
						<p><input id="c39" name="cc" type="checkbox"><label for="c39">Religion</label></p>
						<p><input id="c40" name="cc" type="checkbox"><label for="c40">Science</label></p>
						<p><input id="c41" name="cc" type="checkbox"><label for="c41">Self-Help</label></p>
						<p><input id="c42" name="cc" type="checkbox"><label for="c42">Social Science</label></p>
						<p><input id="c43" name="cc" type="checkbox"><label for="c43">Sports & Recreation</label></p>
						<p><input id="c44" name="cc" type="checkbox"><label for="c44">Study Aids</label></p>
						<p><input id="c45" name="cc" type="checkbox"><label for="c45">Technology & Engineering</label></p>
						<p><input id="c46" name="cc" type="checkbox"><label for="c46">Travel</label></p>
						<p><input id="c47" name="cc" type="checkbox"><label for="c47">True Crime</label></p>
						<p><input id="c48" name="cc" type="checkbox"><label for="c48">Young Adult Fiction</label></p>
						<p><input id="c49" name="cc" type="checkbox"><label for="c49">Young Adult NonFiction</label></p>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>