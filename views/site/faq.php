<style type="text/css">
    .home-header-main-outer-panel{
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel banner-inner-pages-panel faq-page-banner-con ">
	<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>

</div>
</section>
<section class="faq-acordion-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
    	
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">       
       <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
						<i class="short-full glyphicon glyphicon-plus"></i>
						What are the main products that can be delivered to my home, office or Institute?
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false">
				<div class="panel-body">
					<img src="/images/faq-acordion-img1.jpg" alt="">
					<div class="accordion-content">
						<p>Reference Books, Text Books, School Books, Dictionaries, Atlases, Maps, Educational DVDs, EBooks, Directories and lot more!</p>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
							On what subjects, I can find Books?																				
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
				<div class="panel-body">
                  <img src="/images/faq-acordion-img2.jpg" alt="">
					<div class="accordion-content"><br/>
						<p>You can find Books on Antiques & Collectibles, Architecture, Art, Biography & Autobiography, Body, Mind & Spirit, Business & Economics, Comics & Graphic Novels, Computers, Cooking, Crafts & Hobbies, Design, Drama, Education, Family & Relationships, Fiction, Foreign Language Study, Games, Gardening, Health & Fitness, History, House & Home, Humor, Juvenile Fiction, Juvenile Nonfiction, Language Arts & Disciplines, Law, Literary Collections, Literary Criticism, Mathematics, Medical, Music, Nature, Performing Arts, Pets, Philosophy, Photography, Poetry, Political Science, Psychology, Reference, Religion, Science, Self-Help, Social Science, Sports & Recreation, Study Aids, Technology & Engineering, Travel, True Crime, Young Adult Fiction, Young Adult NonFiction</p>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						How do I place an order?														
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
				<div class="panel-body">
                	<img src="/images/faq-acordion-img3.jpg" alt="">
					<div class="accordion-content">
						<p>Simply either by viewing, selecting and purchasing online, or by emailing us the list of books you need, to Contact@WorldBookStore.Online, on receiving your email, customer service will send you the quotation, with applicable discount, based on your requirement.</p>
					 </div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						Is ordering easy?									
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
				<div class="panel-body">
                	<img src="/images/faq-acordion-img4.jpg" alt="">
					<div class="accordion-content">
						<p>Yes! sit back and relax, when you order books from us, we guarantee you'll get it on time, incase any of the books are not available, we will also provide you with the substitute titles, so that you can choose from it. We can supply books from thousands of International publishers.</p>
					</div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFive">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						How can I make payment for my order?																									
					</a>
				</h4>
			</div>
			<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false">
				<div class="panel-body">
                	<img src="/images/faq-left-payments.jpg" alt="">
					<div class="accordion-content">
						<p>You can pay online using your credit card, through payment gateway, or can pay by Bank Draft / Cheque or Bank Transfer. Please get in touch with Customer Support for more details at Contact@WorldBookStore.Online</p>
					</div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSix">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How can I cancel my order and request a refund?																													
					</a>
				</h4>
			</div>
			<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix" aria-expanded="false">
				<div class="panel-body">
                	<img src="/images/faq-left-support-img.jpg" alt="">
					<div class="accordion-content">
						<p>All orders are fully cancelable if the cancel request is received within 24 hours of the time the order is placed. After that, cancellations will not be accepted. There's a good reason for this policy. All orders are pre-paid to the publishers and processed promptly to ensure that the books get shipped as quickly as possible. We do recognize that special situations may arise when a book is out of print or not available, in such events, please contact us at Contact@WorldBookStore.Online, all such cases will be handled on an individual, case-by-case basis.</p>
					</div>
				</div>
			</div>
		</div>
        
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSeven">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						What are the delivery charges?
					</a>
				</h4>
			</div>
			<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" aria-expanded="false">
				<div class="panel-body">
                <img src="/images/faq-acordion-img5.jpg" alt="">
				<div class="accordion-content">
					<p>We offer free shipping on all the books! </p>
					<p>There will be no extra delivery charges. The deliveries are handled by each of our network partners. If you are concerned about a delivery please contact us at <a href="mailto:Contact@WorldBookStore.Online">Contact@WorldBookStore.Online</a> and we will be delighted to look after your request.</p>
				</div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingEight">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How long will it take to receive my book?																								
					</a>
				</h4>
			</div>
			<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight" aria-expanded="false">
				<div class="panel-body">
                	<img src="/images/faq-acordion-img5.jpg" alt="">
					<div class="accordion-content">
						<p>Orders are processed immediately, upon receipt. It will take less than 2 to 3 weeks to receive you book by normal post.
                     Sometimes, there can be postal delay, based on your region, that may affect your service, but we pledge to stand by and guarantee its delivery. 
                     If you have any questions about delivery, please call our Customer Service or send an email to Contact@WorldBookStore.Online. 
                     We provide proper tracking number for each delivery.</p>
					</div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingNine">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How do i change my mailing address?																						
					</a>
				</h4>
			</div>
			<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine" aria-expanded="false">
				<div class="panel-body">
                	 <img src="/images/faq-acordion-img6.jpg" alt="">
					<div class="accordion-content">	
						<p>Simply by sending an email to Customer Service at <a href="mailto:Contact@Worldbookstore.online">Contact@Worldbookstore.online</a></p>
					</div>
				</div>
			</div>
		</div>
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTen">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							Which all countries you ship to?																		
					</a>
				</h4>
			</div>
			<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen" aria-expanded="false">
			   	<div class="panel-body">
                    <img src="/images/faq-acordion-img7.jpg" alt="">
					<div class="accordion-content">
						<p>We ship to all the countries, please go ahead and make your selection of book and buy it, it will get delivered!!</p>
					</div>
				</div>
			</div>
		</div>
        
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingEleven">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						How do I find a specific book?																	
					</a>
				</h4>
			</div>
			<div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img8.jpg" alt="">
					<div class="accordion-content">
						<p>A search box is available at the top of every page throughout the site. You may also browse for a title by subject and topic. </p>
					</div>
				</div>
			</div>
		</div>
        
        
       <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTweleve">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTweleve" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						If I do not find a book I am looking for, will you help us in procuring it?																																												
					</a>
				</h4>
			</div>
			<div id="collapseTweleve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTweleve" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img9.jpg" alt="">
					<div class="accordion-content">
						<p>If there is a book you'd like to buy that you don't find here, Write to us at <a href="mailto:Contact@WorldBookStore.Online">Contact@WorldBookStore.Online.</a> Upon receipt of your request, Customer service will check the availability of that particular title, and will inform you asap.</p>
					</div>
				</div>
			</div>
		</div>
        
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThirty">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirty" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How many books do you have?																	
					</a>
				</h4>
			</div>
			<div id="collapseThirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirty" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img2.jpg" alt="">
					<div class="accordion-content">
						<p>We offer more than 10 Million + Books on a wide range of subjects i.e, Antiques & Collectibles, Architecture, Art, Biography & Autobiography, Body, Mind & Spirit, Business & Economics, Comics & Graphic Novels, Computers, Cooking, Crafts & Hobbies, Design, Drama, Education, Family & Relationships, Fiction, Foreign Language Study, Games, Gardening, Health & Fitness, History, House & Home, Humor, Juvenile Fiction, Juvenile Nonfiction, Language Arts & Disciplines, Law, Literary Collections, Literary Criticism, Mathematics, Medical, Music, Nature, Performing Arts, Pets, Philosophy, Photography, Poetry, Political Science, Psychology, Reference, Religion, Science, Self-Help, Social Science, Sports & Recreation, Study Aids, Technology & Engineering, Travel, True Crime, Young Adult Fiction, Young Adult NonFiction. </p>
					</div>
                    <div class="col-xs-12 pad_none faq-acordion-books-img"><img src="/images/books_disc.jpg" alt=""></div>
				</div>
			</div>
		</div>
        
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingForty">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseForty" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How a Library can place their order?																					
					</a>
				</h4>
			</div>
			<div id="collapseForty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingForty" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img10.jpg" alt="">
					<div class="accordion-content">
						<p>Ordering for Libraries is very easy! Libraries can place their order by emailing us there requirement, and customer service will check the availability and provide quotation for the same. Libraries can also request for subject catalog for making selection.</p>
					</div>
				</div>
			</div>
		</div>
        
        <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFivty">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFivty" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						Do you also provide Books to Corporate Houses?																														
					</a>
				</h4>
			</div>
			<div id="collapseFivty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFivty" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img11.jpg" alt="">
					<div class="accordion-content">
						<p>Yes, we process Corporate Orders, too! If you are a corporate house, please feel free to write to <a href="mailto:Contact@WorldBookStore.online">Contact@WorldBookStore.online</a> and send your selection of Books, upon receipt, our customer service representative will get in touch with you, with further details.</p>
					</div>
				</div>
			</div>
		</div>
        
        
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSixty">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixty" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						How can I get my Book listed on the site and promote it?																																				
					</a>
				</h4>
			</div>
			<div id="collapseSixty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixty" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img12.jpg" alt="">
					<div class="accordion-content">
						<p>If you're a publisher anywhere in the world, we invite you to join us.</p>
						<p>We welcome your inquiries and participation, so drop us a line at <a href="mailto:Contact@WorldBookStore.Online">Contact@WorldBookStore.Online</a> and we'll get back to you to add to your sales thru our efforts.</p>
					</div>
				</div>
			</div>
		</div>
        
        
          <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSeventy">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventy" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						Are there any vacancies?																																																		
					</a>
				</h4>
			</div>
			<div id="collapseSeventy" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventy" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img13.jpg" alt="">
					<div class="accordion-content">
						<p>We are always on the lookout for fresh, energetic talent to join our growing team. We offer competitive salaries and benefits, as well as an easygoing work environment. Most importantly, we offer the challenge of working with an entrepreneurial team that encourages independence and project ownership while helping build a unique company. You can write to us at <a href="mailto:Contact@WorldBookStore.Online">Contact@WorldBookStore.Online</a> to know more about Vacancies.</p>
					</div>
				</div>
			</div>
		</div>
        
        
         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSeventy">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseeithy" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
							How we can advertise on World Bookstore?																																																																											
					</a>
				</h4>
			</div>
			<div id="collapseeithy" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingeithy" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img14.jpg" alt="">
					<div class="accordion-content">
						<p>Advertise on World Bookstore, to reach all parts of the world!!                                                                             
                    If you wish to advertise on World Bookstore, send an email to <a href="mailto:Contact@WorldBookStore.Online">Contact@Worldbookstore.online, </a>
                    we will get in touch with you with various options to suit your needs.</p>
					</div>
				</div>
			</div>
		</div>
        
        
          <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSeventy">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseninety" aria-expanded="false" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						When can I contact Customer Service?																																																																																																
					</a>
				</h4>
			</div>
			<div id="collapseninety" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingeithy" aria-expanded="false">
			   	<div class="panel-body">
                	<img src="/images/faq-acordion-img9.jpg" alt="">
					<div class="accordion-content">
                    <p>We love our Customers!! Customer Service is available to assist you with your requirements of Books 24 x 7, just send an email to
                    <a href="mailto:Contact@WorldBookStore.Online">Contact@Worldbookstore.online</a></p>
					</div>
				</div>
			</div>
		</div>
        
        
        

	</div>
  </div>
</section>