<?php 

	$name = Yii::$app->getRequest()->getQueryParam('name');
	$subject = new \app\models\Subject();
    $model = $subject::find()->where(['subject_slug' => $name])->one();

    $bisac = new \app\modules\MubAdmin\modules\csvreader\models\Bisac;
    $allbisac = $bisac::find()->where(['subject_id' => $model['id']])->all();

?>
<style type="text/css">
	.home-header-main-outer-panel{
		height: 665px;
		background: none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
	@media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
	{
	.home-header-main-outer-panel{
		height: 230px!important;
		background: none!important;
	}	
	}
</style>

<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->
<?php if(!empty($model)){?>
<section class="header-main-outer-panel banner-inner-pages-panel <?= $backgroundClass;?>">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

<h1><span><?= $model->subject_name;?></span></h1>
</section>
</section>

<!--ARCHITECTURE BOOK SECTION START-->
<section class="architecture-book-main-outer">
<div class="col-xs-12 main-padding architecture-book-main-inner">
<h2 class="text_upper">BOOKS ON <?= $model->subject_name;?> <span>Select from a wide range of books on <?= $model->subject_name;?>, click on a<br> topic to see complete listing!</span></h2>
</div>
<!-- <div class="col-xs-12 search-form-in-inner">
	<div class="col-xs-12 pad_none wbo-searchbar-inner">
		<div class="col-sm-4 col-md-4 col-xs-4 pad_none wbo-searchbar-select">
			<select>
			<option>Title</option>
			<option>Author</option>
			<option>ISBN</option>
			<option>Publisher</option>
			<option>Subject</option>
			<option>Topic</option>
			</select>
		</div>
		<div class="col-sm-8 col-md-8 col-xs-8 pad_none  wbo-search-con-outer">
			<div class="col-xs-12 pad_none  wbo-search-con">
				<input name="" type="text" Placeholder="Search by Title, Author, ISBN, Publisher">
				<button value="submit"><i class="fa fa-search"></i></button>
			</div> 
		</div>
	</div>
</div> -->
</section>
<!--ARCHITECTURE BOOK SECTION END-->

<!--ADAPTIVE REUSE SECTION START-->
<section class="adaptive-resues-main-outer-panel">
<div class="col-xs-12 main-padding architecture-book-main-inner">
<!--<h3>Adaptive Reuse &amp; Renovation(208)</h3>-->

<ul>
<?php 
	foreach ($allbisac as $value) { 
?>
<li><a href="/site/worldbookstore?name=<?= $value['slug']; ?>"><?= $value['bisac_title'];?></a></li>
<?php }?>
</ul>

</div>

<!-- <div class="col-xs-12 main-padding category-bottom-buttons">
<a href="#">EMAIL THIS LIST</a> <a href="#">PRINT THIS PAGE</a> <a href="#">DOWNLOAD LIST</a>
</div> -->

</section>
<?php }?>