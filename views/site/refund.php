<style type="text/css">
    .home-header-main-outer-panel{
        background: none!important;
    }
    .text_upper{
       text-transform: uppercase;
    }
    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
    {
    .home-header-main-outer-panel{
        background: none!important;
    }   
    }
</style>
<section class="header-main-outer-panel ">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>
<!--HEADER END-->
<!--WELCOME TO BOOK STORE SECTION START
<section class="banner-inner-pages-panel product-page-banner-con cate">
<h1><span>Architecture</span></h1>
</section>
WELCOME TO BOOK STORE SECTION END-->


<!--FAQ ACCORDION SECTION STRAT-->
<section class="inner-pages-outer">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
		<div class="col-xs-12 pad_none cont-payment-methods">
			<h2>Refund Policy</h2>
			<div class="col-xs-12 col-md-9 col-sm-9 pad_none payment-list terms-use">
			<p>All Orders Are Fully Cancelable If The Cancel Request Is Received Within 24 Hours Of The Time The Order Is Placed. After That, In Most Cases Cancellations Will Not Be Accepted. There’s A Good Reason For This Policy.</p>
			

			<p>We Negotiate With Publishers For The Lowest Rates Available By Pledging That We Will Only Process Legitimate, Pre-Paid Orders From Consumers Who Are Sure Of What They Want When Placing Orders. This Saves Time And Money For All Parties And Seems To Work Well.</p>
			
		<p>
		We Do Recognize That Special Situations May Arise When A Book is out of stock or out of print, Please Contact Us At  <a href="mailto:Contact@worldbookstore.online">Contact@worldbookstore.online</a>. All Such Cases Will Be Handled On An Individual, Case-By-Case Basis.
		</p>
			</div>
			<div class="col-xs-12 col-md-3 col-sm-3 pad_none privacy-policy-img refund-img-right2">
				<img src="/images/refund-policy-img.png" alt="" /> 
			</div> 
			
		</div>
	</div>
</section>