
<section class="rangebox">
        <div class="container">
            <div class="parallel fadeInDown" style="visibility: visible;">
                Welcome to World Bookstore Online!
<h1 class="com-mar-bot-30">Wide Range Of Books!</h1>
                <p class="parallel">Whether you are around the corner or around the world, World Bookstore Online offers more than 10 million titles, speedy delivery, and unmatched service for all that you need - Reference Books, Text Books, School Books, Dictionaries, Directories, Encyclopedias, Maps, Atlases. You can find Books on wide range of subjects.
                 </p>

                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
             </div>
        </div>
</section>
<section class="customersupport">
        <div class="container">
            <div class="parallel2 fadeInDown" style="visibility: visible;">
                <h1 class="com-mar-bot-30">24x7 Customer Service Online!</h1>
                <p class="parallel2">With focus on streamlining customer and distribution services we make sure we deliver publications, whatever you want, wherever you want, whenever you want. When you engage us you not just get Books delivered on time, we make sure you get maximum value as well.
                </p>
                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </section>
    <section class="professional">
        <div class="container">
            <div class="col-md-12">
            <div class="parallel2 fadeInDown" style="visibility: visible;">
                <h1 class="com-mar-bot-30">Team Of Professionals To Assist You!</h1>
                <p class="parallel2">To serve you better, we have built a team of professionals with innovative processes and developed a distribution system which thrives on what you want the most - pricing, quickness, efficiency and accuracy.
                 </p>
                 
                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
             </div>
            </div>
        </div>
</section>
<section class="becare">
        <div class="container">
            <div class="parallel fadeInDown" style="visibility: visible;">
                <h1 class="com-mar-bot-30">We Take Care Of Each Customer!</h1>
                <p class="parallel">World Bookstore Online caters to the educational needs of everyone, from Individuals, to public and private educational Institutes, Colleges, Universities, Research Centres, Polytechnics, Schools, Hospitals, Libraries and Corporate; we meet all your educational requirements
</p>
                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </section>
    <section class="public">
        <div class="container">
            <div class="parallel2 fadeInDown" style="visibility: visible;">
                <h1 class="com-mar-bot-30">Publications From Worldwide Publishers!</h1>
                <p class="parallel2">World Bookstore Online provides you publications from publishing houses in Algeria, Andorra, Argentina, Australia, Austria, Bangladesh, Barbados, Belgium, Bermuda, Brazil, Bulgaria, Cambodia, Canada, Canary Islands, Chile, China, Colombia, Cote d'Ivoire, Croatia, Cyprus, Czech Republic, Denmark, Ecuador, Egypt, Estonia, Finland, France, French Polynesia, Germany, Ghana, Greece, Guatemala, Hong Kong, Hungary, Iceland, India, Indonesia, Iran, Ireland, Italy, Jamaica, Japan, Jordan, Kenya, Korea, Latvia, Lebanon, Lithuania, Luxembourg, Macedonia, Malaysia, Malta, Mexico, Netherlands, New Zealand, Nigeria, Norway, Peru, Philippines, Poland, Portugal, Romania, Russia, Senegal, Serbia, Singapore, Slovakia, Slovenia, South Africa, Spain, Sri Lanka, Sweden, Switzerland, Taiwan, Thailand, Trinidad and Tobago, Tunisia, Turkey, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States, Uruguay, Venezuela, Zimbabwe. For receiving Catalogue or list of publications from any publishers, from any country, kindly email Customer Service.
                 </p>
                 
                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
             </div>
        </div>
    </section>
    <section class="liberaries">
        <div class="container">
            <div class="col-md-12">
            <div class="parallel2 fadeInDown" style="visibility: visible;">
                <p class="parallel2">Dedicated Customer Service Manager</p>
                <h1 class="com-mar-bot-30">For Libraries & Corporate Orders</h1>
                <p class="parallel2">By offering a holistic environment full of challenges and targeting individual growth we help our associates grow not only professionally but personally also. The trust of our customers has helped us move ahead rapidly, confidently and successfully in our mission to evolve into the market leaders in customer and distribution services
                 </p>
                 
                <div class="readmore">
                    <a href="#" title="" class="btncolor">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
             </div>
            </div>
        </div>
</section>
<section class="languages">
        <div class="container">
            <div class="col-md-12">
            <div class="parallel fadeInDown" style="visibility: visible;">
                <h1 class="com-mar-bot-30">You can order books in various languages!</h1>
                <p class="parallel">World Bookstore Online offers publications in English, French, German & Italian Languages, besides these we can fulfil your requirements for publications in other languages i.e. Afrikaans, Afro-asiatic, Amharic, Arabic, Aramaic, Armenian, Bulgarian, Catalan, Chinese, Croatian, Czech, Danish, Dutch, English, Finnish, French, Friesian, German, Greek, Hawaiian, Hebrew, Hungarian, Icelandic, Indonesian, Italian, Japanese, Javanese, Korean, Latin, Norwegian, Polish, Portuguese, Romanian, Russian, Sanskrit, Slovak, Spanish, Swedish, Thai, Urdu, Vietnamese, Welsh.<br><br>
<div class="col-md-3"></div><div class="col-md-6"><p class="backcolor">SHOP ONLINE OR EMAIL US YOUR REQUIREMENTS FOR BOOKS!</p><br></div><br/><br/><br/><br/>
 <p class="cur"><i>Glad to be at your service!</i></p>
                 </p>
                 
               
             </div>
            </div>
        </div>
</section>