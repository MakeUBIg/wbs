<?php 

    $bisac = new \app\modules\MubAdmin\modules\csvreader\models\Bisac;
    $allbisac = $bisac::find()->where(['del_status' => '0'])->all();
 
?>
<?php foreach ($allbisac as  $value) {
  $result = substr($value['bisac_code'], 0, 3);
  }
?>
     <link rel="stylesheet" href="/css/jquery.typeahead.css">
   <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="/js/jquery.typeahead.js"></script>

<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="images/logo-main.png" alt="logo"></a></div>
</div>
</header>

<div class="banner-search-cont">
	<div class="col-xs-12 pad_none wbo-searchbar-inner">
		<div class="col-sm-4 col-md-4 col-xs-4 pad_none wbo-searchbar-select">
			<select>
			<option>Title</option>
			</select>
		</div>
		<form>
		<div class="col-sm-8 col-md-8 col-xs-8 pad_none typeahead__container wbo-search-con-outer">
			<div class="col-xs-12 pad_none typeahead__field wbo-search-con">
			  <span class="typeahead__query">
				<input class="js-typeahead" name="q" type="search" autofocus autocomplete="off" Placeholder="Search by Title, Author, ISBN, Publisher"  style="border: none!important; font-size: 15px!important;">
        </span>
				 <span class="typeahead__button">
          <button type="submit">
              <span class="typeahead__search-icon"></span>
          </button>
         </span>
			</div> 
		</div>
		</form>
	</div>
</div>
</section>
<!--HEADER END-->

<!--4 BOXES SECTION-->
<section class="wbo-boxes-main-outer boxes-on-home">
<div class="col-xs-12 main-padding">
<div class="col-sm-3 col-md-3 col-xs-3 pad_none wbo-boxes-main-inner">
<div class="col-xs-12 pad_none wbo-boxes-main">
<img src="images/wbo_box_icon01.png" alt="icon">
<h3>10 MILLION + BOOKS</h3>
<p>Choose from millions of titles!!</p>
<a href="#">explore now!</a>
</div>
</div>
<div class="col-sm-3 col-md-3 col-xs-3 pad_none wbo-boxes-main-inner">
<div class="col-xs-12 pad_none wbo-boxes-main">
<img src="images/wbo_box_icon02.png" alt="icon">
<h3>WIDE RANGE</h3>
<p>Find Books on Wide range of Subjects &amp; Topics</p>
<a href="#">explore now!</a>
</div>
</div>
<div class="col-sm-3 col-md-3 col-xs-3 pad_none wbo-boxes-main-inner">
<div class="col-xs-12 pad_none wbo-boxes-main">
<img src="images/wbo_box_icon03.png" alt="icon">
<h3>WORLDWIDE PUBLISHERS</h3>
<p>Books from Publishers - 	all around the world!</p>
<a href="#">explore now!</a>
</div>
</div>
<div class="col-sm-3 col-md-3 col-xs-3 pad_none wbo-boxes-main-inner">
<div class="col-xs-12 pad_none wbo-boxes-main">
<img src="images/wbo_box_icon04.png" alt="icon">
<h3>SHIPPING WORLDWIDE</h3>
<p>We ship everywhere!</p>
<a href="#">explore now!</a>
</div>
</div>
</div>
</section>
<!--4 BOXES SECTION-->
	
<!--WELCOME TO BOOK STORE SECTION START-->
<section  class="welcome-bookstore-main-outer">
<div class="col-xs-12 main-padding welcome-bookstore-bg-outer">
<h1><span>WELCOME TO WORLD</span>  BOOKSTORE!</h1>
<p><strong>Whether you are around the corner or around the world, World Bookstore Online offers more than 10 million titles, speedy delivery, and unmatched service for all that you need - Reference Books, Text Books, School Books, Dictionaries, Directories, Encyclopaedias, Maps or Atlases from publishers worldwide.</strong></p>
<p> World Bookstore Online caters to the educational needs of everyone, from Individuals, to public and private educational Institutes, Colleges, Universities, Research Centres, Polytechnics, Schools, Hospitals, Libraries and Corporate; we meet all your educational requirements. Shop online or email us your requirements of Books! </p>
</div>
</seclect>

<section class="bookstore-picture-main-panel main-padding">

	<?php
	foreach ($model as $value){
	?>
	<div class="col-sm-3 col-md-3 col-xs-3 pad_none bookstore-picture-outer">
		<a href="/site/category?name=<?= $value['subject_slug']; ?>" target="_blank">
			<div class="col-xs-12 pad_none bookstore-picture-inner">
				<div class="bookstore-pic-con"><img src="/images/<?= $value['subject_image']; ?>" alt="<?= $value['subject_slug']; ?>"></div>
				<h4><span><?= $value['subject_name']; ?></span></h4>
			</div>
		</a>
	</div>
	<?php }?>

</section>
<!--WELCOME TO BOOK STORE SECTION END-->

<!--Shop online or email us starts-->
<section>
<div class="shop-online-email-us">


<div class="slider-main-outer">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <!--li data-target="#myCarousel" data-slide-to="4"></li-->
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active"><img src="images/img_slider_pic01.jpg"  alt="slider pic"></div>
    <div class="item"><img src="images/img_slider_pic02.jpg"  alt="slider pic"></div>
    <!--div class="item"><img src="images/img_slider_pic03.jpg"  alt="slider pic"></div-->
    <div class="item"><img src="images/img_slider_pic04.jpg"  alt="slider pic"></div>
    <div class="item"><img src="images/img_slider_pic05.jpg"  alt="slider pic"></div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>


<!--	<div class="books-lib-img"><img src="images/books_disc.jpg" alt="pic"></div>-->
	<div class="book-lib-heading">
		<div class="container">
			<div class="row">
				<div class="lib-heading-otr"><span>Shop online</span> or email us your requirements for books!</div>
			</div>
		</div>
	</div>
</div>
</section>
<!--Shop online or email us ends-->


<!--CREATE A WORLD BOOKSTORE ACCOUNT-->
<section class="create-world-bookstore-account-panel">
<h2>Create a World Bookstore Account <span>For all your books requirement, Join World Bookstore Online!!</span></h2>
<div class="col-xs-12 main-padding create-world-bookstore-account-inner">

<div class="col-md-6 col-sm-6 col-xs-6 pad_none create-world-bookstore-account-leftbar">
<h3>Benefits of World Bookstore</h3>
<ul>
<li>Whether you are around the corner or around the world, World Bookstore Online offers more than 10 million titles, speedy delivery, and unmatched service for all that you need………… Reference Books, Text Books, School Books, Dictionaries, Directories, Encyclopaedias, Maps, Atlases…………….Log on to www.worldbookstore.online</li>
<li>With focus on streamlining customer and distribution services we make sure we deliver publications, whatever you want, wherever you want, whenever you want. When you engage us you not just get Books delivered on time, we make sure you get maximum value as well.</li>
<li>To serve you better, we have built a team of professionals with innovative processes and developed a distribution system which thrives on what you want the most - pricing, quickness, efficiency and accuracy.</li>
<li>World Bookstore Online caters to the educational needs of everyone, from Individuals, to public and private educational Institutes, Colleges, Universities, Research Centres, Polytechnics, Schools, Hospitals, Libraries and Corporate; we meet all your educational requirements.</li>
<li>By offering a holistic environment full of challenges and targeting individual growth we help our associates grow not only professionally but personally also. The trust of our customers has helped us move ahead rapidly, confidently and successfully in our mission to evolve into the market leaders in customer and distribution services.</li>
<li>World Bookstore Online offers publications in English, French, German & Italian Languages, besides these we can fulfil your requirements for publications in other languages i.e Afrikaans, Afro-asiatic, Amharic, Arabic, Aramaic, Armenian, Bulgarian, Catalan, Chinese, Croatian, Czech, Danish, Dutch, English, Finnish, French, Friesian, German, Greek, Hawaiian, Hebrew, Hungarian, Icelandic, Indonesian, Italian, Japanese, Javanese, Korean, Latin, Norwegian, Polish, Portuguese, Romanian, Russian, Sanskrit, Slovak, Spanish, Swedish, Thai, Vietnamese, Welsh.</li>
</ul>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad_none create-world-bookstore-account-rightbar">
<form action="#" method="get">
<input name="" type="text" Placeholder="Name*">
<input name="" type="text" Placeholder="Designation">
<input name="" type="text" Placeholder="Department">
<input name="" type="text" Placeholder="Organization">
<input name="" type="text" Placeholder="Address*">
<input name="" type="text" Placeholder="City*">
<input name="" type="text" Placeholder="Zip / Pin code*">
<input name="" type="text" Placeholder="State">
<select>
	<option>Select Country</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	<option>4</option>
</select>
<input name="" type="text" Placeholder="Telephone">
<input name="" type="text" Placeholder="Email*">
<input name="" type="text" Placeholder="Website">
<input name="" type="text" Placeholder="Mobile*">
<button type="submit">Submit Now</button>
<p><input type="checkbox" id="c1" name="cc" /><label for="c1">By signing up, you agree to the Terms and Conditions and Privacy Policy. You also agree to receive product-related emails.</label></p>
</form>
</div>

</div>
</section>
<script type="text/javascript">

        typeof $.typeahead === 'function' && $.typeahead({
            input: ".js-typeahead",
            minLength: 2,
            maxItem: 15,
            order: "asc",
            hint: true,
            group: {
                template: "{{result}} Books!"
            },
            maxItemPerGroup: 10,
            backdrop: {
                "background-color": "#fff"
            },
            href: "/site/bookdetails?name={{display}}",
            emptyTemplate: 'No result for "{{query}}"',
            source: {
				    users: {
				        ajax: {
				            url: '/site/get-listing',
				            data: {
				                q: '{{query}}'
				            },
				            path: 'result'
				        }
				    }
				},
            callback: {
                onReady: function (node) {
                    this.container.find('.' + this.options.selector.dropdownItem + '.group-ale a').trigger('click')
                },
                onDropdownFilter: function (node, query, filter, result) {
                    console.log(query)
                    console.log(filter)
                    console.log(result)
                }
            },
            debug: true
        });

    </script>
