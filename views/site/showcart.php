<?php

	$items = $cart->getPositions();
	$total = $cart->getCost();
	p($total);
	foreach($items as $item){
?>
<div class="title-row"><b><?=$item->product_name;?></b>
<a class="btn-remove-item" id="remove_item_<?= $item->id;?>"><i class="fa fa-trash pull-right"></i></a></div>
<div class="form-group row no-gutter">
    <div class="col-xs-8">
      <b>₹ <?=$item->price;?></b>
    </div>
    <div class="col-xs-4">
       <input class="form-control cart_quantity" type="number" value="<?=$item->getQuantity();?>" id="cart_quantity_<?= $item->id; ?>"> 
    </div>
</div>
<?php }?>
<input type="hidden" id="total-price" value="<?= $total;?>">