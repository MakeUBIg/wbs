
<?php 

	use yz\shoppingcart\ShoppingCart;
	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$link = substr("$actual_link", 45);
	$link2 = str_replace("%20", " ", $link);
	$link3 = str_replace("%", " ", $link2);

	$product = new \app\modules\MubAdmin\modules\csvreader\models\Product();
    $getAllSubject = $product::find()->where(['product_name' => $link3])->one();    

    $dateAndTime = $getAllSubject->publish_date;
	$date = date('M Y', strtotime($dateAndTime));

	$subject = substr($getAllSubject->bisac,0,3);
	$product = new \app\models\Subject();
	$getSubject = $product::find()->where(['subject_code' => $subject])->one();
	$cart = new ShoppingCart();
	$alreadyInCart = $cart->getPositionById($getAllSubject->id);
?>

<style type="text/css">
	.home-header-main-outer-panel{
		height: 0px!important;
		background: : none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
</style>
<section class="header-main-outer-panel">


<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-5 col-md-5 col-xs-5 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>

<div class="col-md-7 col-sm-7 col-xs-12 product-banner-right-heading"><h2><?= $getAllSubject->product_name;?></h2></div>
</div>
</section>

<section class="product-table-main-outer-panel">

<div class="col-xs-12 main-padding wbo-product-detail-main-outer">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none wbo-product-detail-left"><img src="/images/World_Books_store.png" alt="pic"></div>
<div class="col-sm-8 col-md-8 col-xs-8 pad_none wbo-product-detail-right">

	<?php if(!empty($getAllSubject->product_name)){?>
		<div class="col-xs-12 wbo-product-detail-outer">
			<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">TITLE:</div>
			<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><?= $getAllSubject->product_name; ?></div>
		</div>
	<?php }?>

	<?php if(!empty($getAllSubject->isbn_13)){?>
		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">ISBN:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><?= $getAllSubject->isbn_13; ?></div>
		</div>
	<?php }?>	

		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">AUTHOR / CONTRIBUTOR:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text">Robuck & Co, Sears</div>
		</div>

	<?php if(!empty($getAllSubject->format)){?>
		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">FORMAT:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><?= $getAllSubject->format; ?></div>
		</div>
	<?php }?>

	<?php if(!empty($getAllSubject->type)){?>
		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">BIND:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><?= $getAllSubject->type;?></div>
		</div>
	<?php }?>

		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">PUBLISHER & SUPPLIER:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text">Skyhorse Publishing</div>
		</div>
	<?php if(!empty($date)){?>	
		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">	Published Date</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><?= $date; ?></div>
		</div>
	<?php }?>	

		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">MRP:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text">$ <?= $getAllSubject->price; ?></div>
		</div>

	<?php if(!empty($getAllSubject->description)){?>
		<div class="col-xs-12 wbo-product-detail-outer">
		<div class="col-sm-4 col-md-4 col-xs-4 wbo-product-detail-heading">DESCRIPTION:</div>
		<div class="col-sm-8 col-md-8 col-xs-8 wbo-product-detail-text"><p><?= $getAllSubject->description;?></p></div>
		</div>
	<?php }?>	

<script>
  $(document).ready(function() {
$(".more_content").click(function(){
	  $(this).hide();
    $('.minus_content').show();
    $(".more_content_show").toggle();
});

  $(".minus_content").click(function(){
    $(this).hide();
    $('.more_content').show();
    $(".more_content_show").hide();
});
});
</script>



<div class="col-xs-12 main-padding category-bottom-buttons">
	<?php if(empty($alreadyInCart)){?>
	<a style="cursor: pointer;" class="btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $getAllSubject->id;?>">Add to Cart</a>
	<?php } else {?>
	<a href="/site/checkout">Checkout</a>
	<?php }?>
</div>

</div>
</div>



</div>
</section>
<!--TABLE SECTION END-->


<!--Shop online or email us starts-->
