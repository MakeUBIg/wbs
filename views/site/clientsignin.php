<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
?>

<style type="text/css">
	.home-header-main-outer-panel{
		height: 665px;
		background: none!important;
	}
	.text_upper{
	   text-transform: uppercase;
	}
	@media only screen and (min-device-width : 375px) and (max-device-width : 667px) 
	{
	.home-header-main-outer-panel{
		height: 230px!important;
		background: none!important;
	}	
	}
</style>
<section class="header-main-outer-panel ">
<div class="col-xs-12 pad_none wbo-searchbar-outer main-padding">
<div class="col-sm-4 col-md-4 col-xs-4 pad_none  logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>
</div>

</section>


<section class="inner-pages-outer register-section2">
    <div class="col-xs-12 main-padding faq-acordion-inner">      
          <div class="col-xs-12 nopadding shopping-login-checkout-main">
          <div class="col-xs-12 col-sm-6 col-md-6 nopadding shopping-cart-left-main shopping-login-right-outer login-register-customer">
           <div class="col-xs-12 nopadding shopping-cart-left-inner">
			<img src="/images/register-logo2.png" alt="" />
           
		   <div class="col-xs-12 form-section2">

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-12'],
                ],
            ]); ?>
		   
           <div class="col-xs-12 pad_none product-advertise-form-inner shopping-login-checkout-fields">
				<!-- <label>Login</label> -->
				 <?= $form->field($model, 'username')->textInput(['placeholder' =>'Username'])->label(false);?>
		   </div>
		   
           <div class="col-xs-12 pad_none product-advertise-form-inner shopping-login-checkout-fields">
				<!-- <label>Password</label> -->
                <?= $form->field($model, 'password')->passwordInput(['placeholder' =>'Password'])->label(false);?>
		   </div>
		   
           <div class="col-xs-12 pad_none product-advertise-form-inner">
				<input type="submit" name="Register" value="Login" class="button-login"/>
		   </div>
		   <?php ActiveForm::end(); ?>
		    <div class="col-xs-12 pad_none other-links">
				<ul>
					<li><a href="/site/forgetpass">Forgot Password?</a></li>
					<li><a href="/site/client-register">Not a members ? Register</a></li>
				</ul>
			</div>
           </form>
          </div>
          </div>
         </div>
         </div>
	</div>
</section>