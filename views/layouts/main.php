<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

$url_name = yii::$app->controller->action->id;

AppAsset::register($this);
 $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Property', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.'
   ]);

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>World BookStore Private Limited</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <?= Html::csrfMetaTags() ?>
   <?php $this->head() ?>
</head>

<body>

   <?php 
  switch($url_name)
  {
    case 'services':
    {
      $backgroundClass =  'category-page-banner-con image_background_services';
      break;
    }
    case 'about':
    {
      $backgroundClass =  'category-page-banner-con images_background_about';
      break;
    } 
        case 'contact':
    {
      $backgroundClass =  'category-page-banner-con images_background_contact';
      break;
    } 
        case 'publisher':
    {
      $backgroundClass =  'category-page-banner-con images_background_publisher';
      break;
    } 
       case 'catalog':
    {
      $backgroundClass =  'category-page-banner-con images_background_catalog';
      break;
    } 
       case 'subject':
    {
      $backgroundClass =  'category-page-banner-con images_background_subject';
      break;
    } 
      default :
    {
      $backgroundClass =  'category-page-banner-con images_background_home';
      break;
    } 
  }
  ?>
 

<section class="header-main-outer-panel home-header-main-outer-panel">
<header>
<div class="col-xs-12 pad_none header-main-inner main-padding">
<!--<div class="col-sm-3 col-md-3 col-xs-4 pad_none logo-con"><a href="/site/index"><img src="/images/logo-main.png" alt="logo"></a></div>-->
<div class="col-xs-12 pad_none navbar-main-outer nopadding">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/site/index">Home</a></li> 
                <li><a href="/site/about">About Us</a></li>
                <li id="wbo-dropdown-click-con"><a href="#">CATEGORIES</a>
        <div class="wbo-subjects-drop-down" id="wbo-subjects-drop">
          <ul>
            <li><a href="/site/category?name=antiques--collectibles">Antiques & Collectibles</a></li>
            <li><a href="/site/category?name=architecture">Architecture</a></li>
            <li><a href="/site/category?name=art">Art</a></li>
            <li><a href="/site/category?name=biography--autobiography">Biography & Autobiography</a></li>
            <li><a href="/site/category?name=body-mind--spirit">Body, Mind & Spirit</a></li>
            <li><a href="/site/category?name=business--economics">Business & Economics</a></li>
            <li><a href="/site/category?name=comics--graphic-novels">Comics & Graphic Novels</a></li>
            <li><a href="/site/category?name=computers">Computers</a></li>
            <li><a href="/site/category?name=cooking">Cooking</a></li>
            <li><a href="/site/category?name=crafts--hobbies">Crafts & Hobbies</a></li>
            <li><a href="/site/category?name=design">Design</a></li>
          </ul>
          <ul>
            <li><a href="/site/category?name=drama">Drama</a></li>
            <li><a href="/site/category?name=educationd">Education</a></li>
            <li><a href="/site/category?name=family--relationships">Family & Relationships</a></li>
            <li><a href="/site/category?name=fiction">Fiction</a></li>
            <li><a href="/site/category?name=foreign-language-study">Foreign Language Study</a></li>
            <li><a href="/site/category?name=games">Games & Activities</a></li>
            <li><a href="/site/category?name=gardening">Gardening</a></li>
            <li><a href="/site/category?name=health--fitness">Health & Fitness</a></li>
            <li><a href="/site/category?name=history">History</a></li>
            <li><a href="/site/category?name=house--home">House & Home</a></li>
            <li><a href="/site/category?name=humor">Humor</a></li>
          </ul>
          <ul>
            <li><a href="/site/category?name=juvenile-fiction">Juvenile Fiction</a></li>
            <li><a href="/site/category?name=juvenile-nonfiction">Juvenile Nonfiction</a></li>
            <li><a href="/site/category?name=language-arts--disciplines">Language Arts & Disciplines</a></li>
            <li><a href="/site/category?name=law">Law</a></li>
            <li><a href="/site/category?name=literary-collections">Literary Collections</a></li>
            <li><a href="/site/category?name=literary-criticism">Literary Criticism</a></li>
            <li><a href="/site/category?name=mathematics">Mathematics</a></li>
            <li><a href="/site/category?name=medical">Medical</a></li>
            <li><a href="/site/category?name=music">Music</a></li>
            <li><a href="/site/category?name=nature">Nature</a></li>
            <li><a href="/site/category?name=performing-arts">Performing Arts</a></li>
          </ul>
          <ul>
            <li><a href="/site/category?name=pets">Pets</a></li>
            <li><a href="/site/category?name=philosophy">Philosophy</a></li>
            <li><a href="/site/category?name=photography">Photography</a></li>
            <li><a href="/site/category?name=poetry">Poetry</a></li>
            <li><a href="/site/category?name=political-science">Political Science</a></li>
            <li><a href="/site/category?name=psychology">Psychology</a></li>
            <li><a href="/site/category?name=reference">Reference</a></li>
            <li><a href="/site/category?name=religion">Religion</a></li>
            <li><a href="/site/category?name=science">Science</a></li>
            <li><a href="/site/category?name=self-help">Self-Help</a></li>
            <li><a href="/site/category?name=social-science">Social Science</a></li>
          </ul>
          <ul>
            <li><a href="/site/category?name=sports--recreation">Sports & Recreation</a></li>
            <li><a href="/site/category?name=study-aids">Study Aids</a></li>
            <li><a href="/site/category?name=technology--engineering">Technology & Engineering</a></li>
            <li><a href="/site/category?name=transportation">Transportation</a></li>
            <li><a href="/site/category?name=travel">Travel</a></li>
            <li><a href="/site/category?name=true-crime">True Crime</a></li>
            <li><a href="/site/category?name=young-adult-fiction">Young Adult Fiction</a></li>
            <li><a href="/site/category?name=young-adult-nonfiction">Young Adult Nonfiction</a></li>
          </ul>
                </div>
          </li>
         <li><a href="/site/catalog">Catalog</a></li>
         <li><a href="/site/publish">Publisher</a></li> <li><a href="/site/contact">Contact Us</a> </li>
         <li><a href="#"><i class="fa fa-user"></i> My Account</a></li>
         <li class="checkout-nav-icon"><a href="/site/checkout"><i class="fa fa-share"></i> Checkout</a> </li>
          <li class="worldwide-ship-con-pic"><a href="/site/location"><img src="/images/img_shipping_worldwide_pic.png" alt="pic">WORLDWIDE SHIPPING</a> </li>
          
         <li class="worldwide-ship-con-pic">
           <select class="currency-type2">
          <option>DOLLAR</option>
          </select>
          </li>
                <li><a href="/site/checkout"><img src="/images/img_cart_icon.png" alt="pic"><span class="cart-product-con"><?php echo $this->render('/site/cartcount');?></span> Cart  <span class="wbo-cart-separator">
                |</span> <span class="price-wbo-con" style="display: none;">$0.00</span></a> </li> 
                <?php  if (Yii::$app->user->isGuest) {?>
                <li class="wbo-login-button-nav"><a href="/site/client-login" class="wbo-login-button">Login</a></li> 
                <li class="wbo-login-button-nav"><a href="/site/client-register" class="wbo-register-button">Register</a></li>
                <?php }
                else{?>
                <li class="checkout-nav-icon addback" id="wbo-dropdown-click-con"><a href="<?= Url::to(['/site/logout'])?>" data-method="post" class="addcol">logout</a> </li>
                <?php }?>
          </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

</div>
</div>

<?= $content ?>

<section>
<div class="shop-online-email-us">
  <div class="books-lib-img"><img src="/images/books_disc.jpg" alt="pic"></div>
</div>
</section>
<!--Shop online or email us ends-->

<!--WBO WIDE RANGE SECTION-->
<section>
  <div class="wbo-listing-wide-main-panel">
    <div class="col-xs-12 main-padding wbo-search-keywords-main">
      <ul>
        <li><a>10 Million + Titles</a></li>
        <li><a>Wide Range</a></li>
        <li><a>Great prices</a></li>
        <li><a>Best Discounts</a></li>
        <li><a>Quick Delivery</a></li>
        <li><a>24x7 Customer Care</a></li>
        <li><a>Worldwide Shipping</a></li>
      </ul>
    </div>
  </div>
</section>

<footer>
<div class="col-xs-12 main-padding wbo-footer-listing-outer">
  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
      <li><a href="/site/about">About Us</a></li>
      <li><a href="/site/faq">FAQ's</a></li>
      <li><a href="/site/shiping">Shipping Policy</a></li>
      <li><a href="/site/refund/">Refund Policy</a></li>
      <li><a href="/site/term/">Terms of use</a></li>
      <li><a href="/site/paymentmethod">Payment Methods</a></li>
      <li><a href="/site/privacy">Privacy Policy</a></li>
      <li><a href="/site/location">Locations we Ship To</a></li>
      <li><a href="/site/contact">Contact Us</a></li>
      <li><a href="/site/client-register">Register</a></li>
      <li><a href="/site/client-login">Login</a></li>
    </ul>
  </div>

  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
      <li><a href="/site/category?name=antiques--collectibles">Antiques & Collectibles</a></li>
      <li><a href="/site/category?name=architecture">Architecture</a></li>
      <li><a href="/site/category?name=art">Art</a></li>
      <li><a href="/site/category?name=biography--autobiography">Biography & Autobiography</a></li>
      <li><a href="/site/category?name=body-mind--spirit">Body, Mind & Spirit</a></li>
      <li><a href="/site/category?name=business--economics">Business & Economics</a></li>
      <li><a href="/site/category?name=comics--graphic-novels">Comics & Graphic Novels</a></li>
      <li><a href="/site/category?name=computers">Computers</a></li>
      <li><a href="/site/category?name=cooking">Cooking</a></li>
      <li><a href="/site/category?name=crafts--hobbies">Crafts & Hobbies</a></li>
    </ul>
  </div>

  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
      <li><a href="/site/category?name=design">Design</a></li>
      <li><a href="/site/category?name=drama">Drama</a></li>
      <li><a href="/site/category?name=educationd">Education</a></li>
      <li><a href="/site/category?name=family--relationships">Family & Relationships</a></li>
      <li><a href="/site/category?name=fiction">Fiction</a></li>
      <li><a href="/site/category?name=foreign-language-study">Foreign Language Study</a></li>
      <li><a href="/site/category?name=games">Games & Activities</a></li>
      <li><a href="/site/category?name=gardening">Gardening</a></li>
      <li><a href="/site/category?name=health--fitness">Health & Fitness</a></li>
      <li><a href="/site/category?name=history">History</a></li>
      <li><a href="/site/category?name=house--home">House & Home</a></li>
    </ul>
  </div>

  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
    <li><a href="/site/category?name=humor">Humor</a></li>
    <li><a href="/site/category?name=juvenile-fiction">Juvenile Fiction</a></li>
    <li><a href="/site/category?name=juvenile-nonfiction">Juvenile Nonfiction</a></li>
    <li><a href="/site/category?name=language-arts--disciplines">Language Arts & Disciplines</a></li>
    <li><a href="/site/category?name=law">Law</a></li>
    <li><a href="/site/category?name=literary-collections">Literary Collections</a></li>
    <li><a href="/site/category?name=literary-criticism">Literary Criticism</a></li>
    <li><a href="/site/category?name=mathematics">Mathematics</a></li>
    <li><a href="/site/category?name=medical">Medical</a></li>
    <li><a href="/site/category?name=music">Music</a></li>
    </ul>
  </div>

  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
      <li><a href="/site/category?name=nature">Nature</a></li>
      <li><a href="/site/category?name=performing-arts">Performing Arts</a></li>
      <li><a href="/site/category?name=pets">Pets</a></li>
      <li><a href="/site/category?name=philosophy">Philosophy</a></li>
      <li><a href="/site/category?name=photography">Photography</a></li>
      <li><a href="/site/category?name=poetry">Poetry</a></li>
      <li><a href="/site/category?name=political-science">Political Science</a></li>
      <li><a href="/site/category?name=psychology">Psychology</a></li>
      <li><a href="/site/category?name=reference">Reference</a></li>
      <li><a href="/site/category?name=religion">Religion</a></li>
      <li><a href="/site/category?name=science">Science</a></li>
    </ul>
  </div>


  <div class="col-xs-2 wbo-footer-listing-con">
    <ul>
      <li><a href="/site/category?name=self-help">Self-Help</a></li>
      <li><a href="/site/category?name=social-science">Social Science</a></li>
      <li><a href="/site/category?name=sports--recreation">Sports & Recreation</a></li>
      <li><a href="/site/category?name=study-aids">Study Aids</a></li>
      <li><a href="/site/category?name=technology--engineering">Technology & Engineering</a></li>
      <li><a href="/site/category?name=transportation">Transportation</a></li>
      <li><a href="/site/category?name=travel">Travel</a></li>
      <li><a href="/site/category?name=true-crime">True Crime</a></li>
      <li><a href="/site/category?name=young-adult-fiction">Young Adult Fiction</a></li>
      <li><a href="/site/category?name=young-adult-nonfiction">Young Adult Nonfiction</a></li>
    </ul>
  </div>

<div class="footer-bottom-part">
  <div class="col-md-3 col-xs-4 col-xs-4 footer-bott-part">
    <h4>Payment Options</h4>
    <div class="options-otr">
      <a href="#"><img src="/images/CreditCards01.png" alt="Card Options"></a>
      <a href="#"><img src="/images/CreditCards02.png" alt="Card Options"></a>
      <a href="#"><img src="/images/CreditCards03.png" alt="Card Options"></a>
      <a href="#"><img src="/images/CreditCards04.png" alt="Card Options"></a>
    </div>
  </div>
  <div class="col-md-5 col-xs-4 col-xs-4 footer-bott-part">
    <h4>WORLD BOOKSTORE</h4>
    <div class="bottom-footer-cont">
      <p>Write to us!</p>
      <p><a href="mailto:Contact@worldbookstore.online">Contact@worldbookstore.online</a></p>
    </div>
  </div>
  <div class="col-md-4 col-xs-4 col-xs-4 footer-bott-part">
    <h4>Follow us!</h4>
    <div class="bottom-footer-cont">
      <p>Experience a World Class Service!!</p>
      <div class="footer-social-cont">
        <ul>
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
          <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
          <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
          <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
          <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

<div class="col-xs-12 pad_none wbo-copyright-outer">Copyright &copy; 2018 World BookStore. All rights are reserved</div>

</footer>
<!--FOOTER END-->

<a href="javascript:void(0);" class="back-to-top"><i class="fa fa-angle-double-up"></i></a>

</body>
<?php $this->endBody();?>
</html>
<?php $this->endPage();?>