<?php

use yii\base\Model;


namespace app\modules\MubAdmin\modules\csvreader\models;
use app\components\Model;
use app\helpers\HtmlHelper;

class CsvForm extends Model{
    public $file;
    
    public function rules(){
        return [
            [['file'],'required'],
            [['file'],'file','extensions'=>'csv','maxSize'=>1024 * 1024 * 5],
        ];
    }
    
    public function attributeLabels(){
        return [
            'file'=>'Select File',
        ];
    }
}
