<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "publisher_products".
 *
 * @property integer $id
 * @property integer $publisher_id
 * @property integer $product_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Product $product
 * @property Publisher $publisher
 */
class PublisherProducts extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher_key', 'ing_product_id'], 'required'],
            [['publisher_key', 'ing_product_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher_key' => 'Publisher Key',
            'ing_product_id' => 'Product ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
