<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "author".
 *
 * @property integer $id
 * @property string $f_name
 * @property string $l_name
 * @property string $title
 * @property string $key_name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property ProductsAuthor[] $productsAuthors
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_name'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['f_name', 'l_name', 'key_name'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            'title' => 'Title',
            'key_name' => 'Key Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsAuthors()
    {
        return $this->hasMany(ProductsAuthor::className(), ['author_id' => 'id']);
    }
}
