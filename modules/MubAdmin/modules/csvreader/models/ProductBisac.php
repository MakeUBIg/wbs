<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "product_bisac".
 *
 * @property integer $id
 * @property integer $bisac_id
 * @property integer $sub_bisac_id
 * @property integer $product_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Bisac $bisac
 * @property Product $product
 */
class ProductBisac extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_bisac';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bisac_id', 'sub_bisac_id', 'product_id'], 'required'],
            [['bisac_id', 'sub_bisac_id', 'product_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['bisac_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bisac::className(), 'targetAttribute' => ['bisac_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bisac_id' => 'Bisac ID',
            'sub_bisac_id' => 'Sub Bisac ID',
            'product_id' => 'Product ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBisac()
    {
        return $this->hasOne(Bisac::className(), ['id' => 'bisac_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
