<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

use \app\models\MubUser;

/**
 * This is the model class for table "fileUpload".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $file_path
 * @property string $mub_user_id
 * @property string $created_at
 * @property string $updated_at
 */
class FileUploaded extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fileUpload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'file_path', 'mub_user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'file_path' => 'File Path',
            'mub_user_id' => 'Mub User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

   public function getMubUser() 
   { 
       return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']); 
   } 
}
