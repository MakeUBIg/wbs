<?php 

namespace app\modules\MubAdmin\modules\csvreader\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class CsvProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $this->models = [''];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [];
        return $this->relatedModels;
    }

    public function getCategoryId($subCategoryId)
    {

    }

    public function saveCategory($categories)
    {
        $uniqueCategories = array_unique($categories);
        foreach ($uniqueCategories as $category)
        {
            $newCategory = new Category();
            $categorySlug = StringHelper::generateSlug($category);
            $count = $newCategory::find()->where(['slug' => $categorySlug])->count();
            if($count == 0)
            {
              $newCategory->category_name = $category;
              $newCategory->slug = $categorySlug;
              // p($newCategory);
              if(!$newCategory->save())
              {
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
              }
            }
        }
        return true;
    }


    public function saveSubCategory($subCategory,$categories)
    {
        $uniqueSubCategories = array_unique($subCategory);
        foreach($uniqueSubCategories as $catKey => $subCategoryName)
        {
          //checking if subcategory alreay exists
          $subCategoryModel = new SubCategory();
          $subcategorySlug = StringHelper::generateSlug($subCategoryName);
          $subCatCount = $subCategoryModel::find()->where(['slug' => $subcategorySlug])->count();
          
          if($subCatCount == 0)
          {
            //getting Category id
            $categoryName = $categories[$catKey];
            $categoryModel = new Category();
            $categorySlug = StringHelper::generateSlug($categoryName);
            $categoryId = $categoryModel::find()->where(['slug' => $categorySlug])->one()->id;
            
            $subCategoryModel->category_id = $categoryId;
            $subCategoryModel->sub_category_name = $subCategoryName;
            $subCategoryModel->slug = $subcategorySlug;
            if(!$subCategoryModel->save())
            {
              // p($subCategoryModel->getErrors());
              throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }
          }
        }
      return true; 
    }



    public function saveProducts($products, $price, $discount, $isbn, $publishedDate,$productFormat,$productType,$sellPrice,$description){
      if(count($isbn) == count(array_unique($isbn)))
      {
        $masterData = [];
        $now = new \DateTime();
        $currentDateTime = $now->format('Y-m-d h:m:s');
        foreach ($products as $masterKey => $product) 
        {

            $productName = str_replace('_',',',$product);
            $masterData[] = [
            $productName,
            $price[$masterKey],
            $sellPrice[$masterKey],
            $discount[$masterKey],
            $productFormat[$masterKey],
            Date('Y-m-d',strtotime($publishedDate[$masterKey])),
            $isbn[$masterKey],
            StringHelper::generateSlug($productName),
            $productType[$masterKey],
            $description[$masterKey],
            $currentDateTime];
        }

        $rowsAffected = \Yii::$app->db->createCommand()->batchInsert('product',
            [
            'product_name',
            'price',
            'sale_price',
            'discount_per',
            'format',
            'publish_date',
            'isbn',
            'slug',
            'type',
            'description',
            'created_at'], $masterData)->execute();
        return $rowsAffected;
      }
      return false;
    }

    public function saveAuthors($authors)
    {
        $authorsMaster = [];
        foreach ($authors as $masterKey => $author) 
        {
            $authorsRow = explode(';',$author);
            $authorsMaster = array_merge_recursive($authorsMaster,$authorsRow);
        }
        $authorsMaster = array_map('trim',$authorsMaster);
        $authorsLastName = StringHelper::getAuthorSlug($authorsMaster);
        //$allAuthors = array_unique($authorsLastName);
        p($authorsLastName);
    }




    public function saveData($csvFile)
    {
        if(!empty($csvFile))
        {
           unset($csvFile[0]);
           foreach ($csvFile as $rowKey => $row) {
               $dataSet = explode(',',$row);
               // p($dataSet);
               $categories[$rowKey] = $dataSet[0];
               $subCategories[$rowKey] = $dataSet[1];
               $isbn[$rowKey] = $dataSet[2];
               $products[$rowKey] = $dataSet[3];
               $authors[$rowKey] = $dataSet[4];
               $productType[$rowKey] = $dataSet[5];
               $productFormat[$rowKey] = $dataSet[6];
               $publisher[$rowKey] = $dataSet[7];
               $publishedDate[$rowKey] = $dataSet[8];
               $price[$rowKey] = $dataSet[9];
               $discount[$rowKey] = $dataSet[10];
               $sellPrice[$rowKey] = $dataSet[11];
               $description[$rowKey] = $dataSet[12];
            }
           // p($categories);
           // $categoryIds = $this->saveCategory($categories);
           // $subCategoryIds = $this->saveSubCategory($subCategories,$categories);
           // $products = $this->saveProducts($products, $price, $discount,$isbn,$publishedDate,$productFormat,$productType,$sellPrice,$description);
           // if($products)
           // {
           //  p($authors);
           // }
            $authorsCount = $this->saveAuthors($authors);
            p($authors);
        }

    }
}