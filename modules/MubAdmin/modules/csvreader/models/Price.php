<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property string $ean_13
 * @property string $price
 * @property string $weight
 * @property string $discount_per
 * @property string $discount_amount
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class Price extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ean_13', 'price', 'weight'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['ean_13', 'price', 'weight', 'discount_per', 'discount_amount'], 'string', 'max' => 255],
            [['ean_13', 'price', 'weight'], 'unique', 'targetAttribute' => ['ean_13', 'price', 'weight'], 'message' => 'The combination of Ean 13, Price and Weight has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ean_13' => 'Ean 13',
            'price' => 'Price',
            'weight' => 'Weight',
            'discount_per' => 'Discount Per',
            'discount_amount' => 'Discount Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}
