<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;

/**
 * This is the model class for table "bisac".
 *
 * @property integer $id
 * @property string $bisac_code
 * @property string $bisac_title
 * @property string $slug
 * @property integer $priority
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property BisacImage[] $bisacImages
 * @property ProductBisac[] $productBisacs
 * @property SubBisac[] $subBisacs
 */
class Bisac extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bisac';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bisac_code', 'bisac_title', 'slug'], 'required'],
            [['priority'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['bisac_code', 'bisac_title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bisac_code' => 'Bisac Code',
            'bisac_title' => 'Bisac Title',
            'slug' => 'Slug',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBisacImages()
    {
        return $this->hasMany(BisacImage::className(), ['bisac_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBisacs()
    {
        return $this->hasMany(ProductBisac::className(), ['bisac_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBisacs()
    {
        return $this->hasMany(SubBisac::className(), ['bisac_id' => 'id']);
    }
}
