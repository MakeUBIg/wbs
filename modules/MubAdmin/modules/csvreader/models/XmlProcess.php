<?php 

namespace app\modules\MubAdmin\modules\csvreader\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class XmlProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $this->models = [''];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [];
        return $this->relatedModels;
    }

    public function getCategoryId($subCategoryId)
    {

    }

    public function saveProducts(){

    }

    public function saveAuthors($node){

        $authorModel = new \app\modules\MubAdmin\modules\csvreader\models\Author;
        $contributor = (Array) $node->contributor;
        foreach($contributor as $contKey => $contValue)
        {
          $keyName[] = StringHelper::getContributorKeyName($contKey);
          $keyValue[] = StringHelper::getContibutorValue($contValue);
        }
        p($keyName);
        $getAuthor = StringHelper::getAuthorValue($node->contributor->b035);
        $saveAuthors->first_name = (String)$node->contributor->b039;
        $saveAuthors->last_name = (String)$node->contributor->b040;
        $saveAuthor = $saveAuthors->save();

    }



    public function saveData($csvFile)
    {
        if(!empty($csvFile))
        {
           unset($csvFile[0]);
           foreach ($csvFile as $rowKey => $row) {
               $dataSet = explode(',',$row);
               // p($dataSet);
               $categories[$rowKey] = $dataSet[0];
               $subCategories[$rowKey] = $dataSet[1];
               $isbn[$rowKey] = $dataSet[2];
               $products[$rowKey] = $dataSet[3];
               $authors[$rowKey] = $dataSet[4];
               $productType[$rowKey] = $dataSet[5];
               $productFormat[$rowKey] = $dataSet[6];
               $publisher[$rowKey] = $dataSet[7];
               $publishedDate[$rowKey] = $dataSet[8];
               $price[$rowKey] = $dataSet[9];
               $discount[$rowKey] = $dataSet[10];
               $sellPrice[$rowKey] = $dataSet[11];
               $description[$rowKey] = $dataSet[12];
            }
           // p($categories);
           // $categoryIds = $this->saveCategory($categories);
           // $subCategoryIds = $this->saveSubCategory($subCategories,$categories);
           // $products = $this->saveProducts($products, $price, $discount,$isbn,$publishedDate,$productFormat,$productType,$sellPrice,$description);
           // if($products)
           // {
           //  p($authors);
           // }
            $authorsCount = $this->saveAuthors($authors);
            p($authors);
        }

    }
}