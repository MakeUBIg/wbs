<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;
use app\modules\MubAdmin\modules\csvreader\models\Product;
use app\modules\MubAdmin\modules\csvreader\models\Price;
/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $ing_product_id
 * @property string $product_name
 * @property string $format
 * @property string $publish_date
 * @property string $ean_13
 * @property string $upc
 * @property string $gtin_14
 * @property string $isbn_10
 * @property string $isbn_13
 * @property string $slug
 * @property string $language
 * @property string $type
 * @property string $description
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Price[] $prices
 * @property Price[] $prices0
 * @property ProductsAuthor[] $productsAuthors
 * @property PublisherProducts[] $publisherProducts
 */

class Product extends \app\components\Model implements CartPositionInterface
{
    
    use CartPositionTrait;

    public function getPrice()
    {
       $productId = $this->id;
       $productPrice = Price::find()->where(['ean_13' => $productId])->one();
       //discount percentage $discount = $productPrice->discount_percentage;
       if(!empty($productPrice))
       {
         $price = ceil($productPrice->price);
       }
       else
       {
        $price = 50;//Default Price for a book
       }

       return $price;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
       return [
            [['ing_product_id', 'product_name', 'format', 'slug', 'language'], 'required'],
            [['publish_date', 'created_at', 'updated_at'], 'safe'],
            [['description', 'status', 'del_status'], 'string'],
            [['ing_product_id', 'format', 'ean_13', 'upc', 'gtin_14', 'isbn_10', 'isbn_13','language', 'bisac', 'type'], 'string', 'max' => 255],
            [['product_name','slug',], 'string', 'max' => 700],
            [['ean_13', 'status', 'del_status'], 'unique', 'targetAttribute' => ['ean_13', 'status', 'del_status'], 'message' => 'The combination of Ean 13, Status and Del Status has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ing_product_id' => 'Ing Product ID',
            'product_name' => 'Product Name',
            'format' => 'Format',
            'publish_date' => 'Publish Date',
            'ean_13' => 'Ean 13',
            'upc' => 'Upc',
            'gtin_14' => 'Gtin 14',
            'isbn_10' => 'Isbn 10',
            'isbn_13' => 'Isbn 13',
            'slug' => 'Slug',
            'language' => 'Language',
            'bisac' => 'BISAC',
            'type' => 'Type',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsAuthors()
    {
        return $this->hasMany(ProductsAuthor::className(), ['products_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherProducts()
    {
        return $this->hasMany(PublisherProducts::className(), ['product_id' => 'id']);
    }
}
