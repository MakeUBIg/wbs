<?php

use yii\base\Model;
namespace app\modules\MubAdmin\modules\csvreader\models;
use app\components\Model;
use app\helpers\HtmlHelper;

class XmlForm extends Model{
    public $file;
    
    public function rules(){
        return [
            [['file'],'required'],
            [['file'],'file','extensions'=>'xml','maxSize'=>1024 * 1024 * 10],
        ];
    }
    
    public function attributeLabels(){
        return [
            'file'=>'Select File',
        ];
    }
}
