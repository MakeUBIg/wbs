<?php

namespace app\modules\MubAdmin\modules\csvreader\controllers;
use app\modules\MubAdmin\modules\csvreader\models\CsvForm;

use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\MubAdmin\modules\csvreader\models\CsvProcess;
use Yii;

/**
 * Default controller for the `csvreader` module
 */
class CsvController extends Controller
{
     public $layout = "@app/views/layouts/admin";
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex($state = NULL){
        $model = new CsvForm;
        if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'file');
            $filename = 'Data.'.$file->extension;
            $upload = $file->saveAs('uploads/'.$filename);
            if($upload){
                define('CSV_PATH','uploads/');
                $csv_file = CSV_PATH . $filename;
                $fileCsv = file($csv_file);
                $csvProcess = new CsvProcess();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $rowsInserted = $csvProcess->saveData($fileCsv);
                    $transaction->commit();
                }
                catch (\Exception $e) 
                {
                    $transaction->rollBack();
                    throw $e;
                }
                unlink('uploads/'.$filename);
                return $this->redirect(['site/index']);
            }
        }else{
            return $this->render('index',['model'=>$model]);
        }
    } 

}
