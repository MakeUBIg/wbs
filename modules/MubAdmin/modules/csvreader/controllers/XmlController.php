<?php

namespace app\modules\MubAdmin\modules\csvreader\controllers;
use app\modules\MubAdmin\modules\csvreader\models\XmlForm;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\helpers\StringHelper;
use app\modules\MubAdmin\modules\csvreader\models\Product;
use app\modules\MubAdmin\modules\csvreader\models\XmlProcess;
use app\modules\MubAdmin\modules\csvreader\models\FileUploaded;
use app\modules\MubAdmin\modules\csvreader\models\ProductBisac;
use \app\modules\MubAdmin\modules\csvreader\models\Bisac;
use Yii;
use DateTime;

class XmlController extends Controller
{
  public $layout = "@app/views/layouts/admin";
  /**
   * Renders the index view for the module
   * @return string
   */
  public function actionFileUpload()
  { 
    if(\Yii::$app->request->isAjax) {
    $model = new XmlForm();

    $xmlFile = UploadedFile::getInstance($model, 'file');

    $directory = \Yii::getAlias('@app/uploads') . DIRECTORY_SEPARATOR;
    if(!is_dir($directory)) {
        FileHelper::createDirectory($directory);
    }
    if($xmlFile) 
    {
      $uid = uniqid(time(), true);
      $fileName = $uid . '.' . $xmlFile->extension;
      $filePath = $directory . $fileName;
      if($xmlFile->saveAs($filePath))
      {
       $fileUploaded  = new FileUploaded();
       $fileUploaded->file_name = $uid . '.' . $xmlFile->extension;
       $fileUploaded->file_path = $filePath;
       $fileUploaded->load_status = '0';
       $mubUserId = \app\models\User::getMubUserId();
       $fileUploaded->mub_user_id = $mubUserId;
      if(!$fileUploaded->save())
      {
        p($fileUploaded->getErrors());
      }
      return true;
      }
    }
    return false;
    }
    else
    {
      return 'go away';
    }
  }

  public function actionChangeDataLoadStatus($id)
  {
    if(\Yii::$app->request->isAjax) 
    {
      $model = new FileUploaded();
      $nameId = explode('_',$id);
      $id2 = end($nameId);
      $file = $model::find()->where(['id' => $id2])->one();
      
      if($id == 'read_data_'.$id2)
      {
        $sql = Yii::$app->db->createCommand()->update('fileUpload', ['load_status' => 1], 'id = "'.$id2.'"')->execute();

         echo "<script>console.log('added ".$sql." records')</script>";
      }

      if($id == 'read_authors_'.$id2)
      {
        $sql = Yii::$app->db->createCommand()->update('fileUpload', ['load_status' => 2], 'id = "'.$id2.'"')->execute();

         echo "<script>console.log('added ".$sql." records')</script>";
      }

      if($id == 'read_publishers_'.$id2)
      {
        $sql = Yii::$app->db->createCommand()->update('fileUpload', ['load_status' => 3], 'id = "'.$id2.'"')->execute();

         echo "<script>console.log('added ".$sql." records')</script>";
      }

      if($id == 'link_publishers_'.$id2)
      {
        $sql = Yii::$app->db->createCommand()->update('fileUpload', ['load_status' => 4], 'id = "'.$id2.'"')->execute();

         echo "<script>console.log('added ".$sql." records')</script>";

      }

      if($id == 'link_authors_'.$id2)
      {
        $sql = Yii::$app->db->createCommand()->update('fileUpload', ['load_status' => 5], 'id = "'.$id2.'"')->execute();

         echo "<script>console.log('added ".$sql." records')</script>";

      }
    }
  }

  public function actionIndex()
  {
    $showFileUpload = new FileUploaded();
    $allFileUploaded = $showFileUpload::find()->all();
    return $this->render('index',['allFileUploaded' => $allFileUploaded]);
  }

  public function saveProductBisac($model)
  {
    $bisac = $model->bisac;
    $bisacId = Bisac::find()->where(['bisac_code' => $bisac])->one()->id;   
    $productBisacModel = new ProductBisac();
  }

  public function actionReadUploadedFile($id)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    $z = new \XMLReader;
    $z->open($path);
    while ($z->read() && $z->name !== 'product');
    $i=0;

    $fields = '(`id`,`product_name`,`format`,`publish_date`,`slug`,`language`,`bisac`,`type`,`description`,`created_at`)';

    $i=1;
    while ($z->name === 'product')
    {
      try
      {
        $node = new \SimpleXMLElement($z->readOuterXML());
      }
      catch (Exception $e)
      {
        p([[$e],[$node]]);
      }
      //Skipp Empty Records
      if(((String)$node->title->b203 !== "")&&((String)$node->b064!=''))
      { 
      $title = (String) $node->title->b203;
      $pi = null;
      $ingProductId = null;
      foreach ($node->productidentifier as $key => $identifier) 
      {
          $pi[$key] = (Array) $identifier;
          if($pi[$key]['b221'] == '03')
          {
            $ingProductId = (String) $pi[$key]['b244'];    
          }
      }
      if(!$ingProductId)
      {
        p($pi);
      }
      $productName = str_replace("\n","",str_replace("'","\'",$title));
      $format = (String) $node->b012;
      $ts = (String) $node->b394[0]->attributes();
      $date = new DateTime("@$ts"); 
      $publishDate = $date->format('Y-m-d');
      $bisac = (String) $node->b064;
      $language = (String) $node->language->b252;
      $slug = StringHelper::generateSlug($title).'_'.$ingProductId."_".$language;
      $type = StringHelper::getFormatValue((String)$node->b012);
      $description = str_replace("\n","",str_replace("'","\'",strip_tags(str_replace("\\", "",(String)$node->othertext->d104)))); 
      $recordSet[$i] = [$ingProductId,$productName,$format,$publishDate,$slug,$language,$bisac,$type,$description];
      if($i%5000 == 0)
      {
        try
        {   
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $values = "";
            foreach ($recordSet as $record) 
            {
              $record[] = $date;
              $values .= "('".implode("','",$record)."')";
              if($loop !== 5000)
              {
                $values .= ",";
              }
              $loop++;
            }
          $command = "INSERT INTO `product` ".$fields." VALUES ".$values." ON DUPLICATE KEY UPDATE `id`=`id`,`product_name`=`product_name`,`format`=`format`,`publish_date`=`publish_date`,`slug`=`slug`,`language`=`language`,`bisac`=`bisac`,`type`=`type`,`description`=`description`,`created_at`=`created_at`";
          $sql = \Yii::$app->db->createCommand($command)->execute();       
          $recordSet = [];
        }
        catch (Exception $e)
        {
            p([['exception' => $e],['at' => $i]]);
        }
        echo "<script>console.log(".$sql." records inserted)</script>";
        sleep(2); 
      }
      $i++;
    }
      $z->next('product');
    }
    if(!empty($recordSet))
    {
      try{
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $count = count($recordSet);
            $values = "";
            foreach ($recordSet as $record) 
            {
              $record[] = $date;
              $values .= "('".implode("','",$record)."')";
              if($loop !== $count)
              {
                $values .= ",";
              }
              $loop++;
            }
         $command = "INSERT INTO `product` ".$fields." VALUES ".$values." ON DUPLICATE KEY UPDATE `id`=`id`,`product_name`=`product_name`,`format`=`format`,`publish_date`=`publish_date`,`slug`=`slug`,`language`=`language`,`bisac`=`bisac`,`type`=`type`,`description`=`description`,`created_at`=`created_at`";
        $sql = \Yii::$app->db->createCommand($command)->execute();
        }
        catch (Exception $e){
          p([['exception' => $e],['at' => $i]]);
        }
        $this->redirect(['/mub-admin/csvreader/xml?product=read_authors_'.$fileId]);
    }
  }

  public function actionTest()
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $sql = "select ing_product_id from `publisher_products` where 1 limit 100";
    $sqlCommand = \Yii::$app->db->createCommand($sql);
    $allPublisher = $sqlCommand->queryAll();
    $price = [];
    foreach ($allPublisher as $key => $value) 
    {
      $sql = 'select * from `price` where `ean_13` = "'.$value["ing_product_id"].'";';
      $sqlCommand = \Yii::$app->db->createCommand($sql);
      $price[] = $sqlCommand->queryAll();
    }
    p($price);
  }

  public function actionLinkAuthors($id)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    // $xmlProcess = new app\modules\MubAdmin\modules\csvreader\models\XmlProcess;
    $z = new \XMLReader;
    $z->open($path);
    while ($z->read() && $z->name !== 'product');
    $i = 1;
    while ($z->name === 'product')
    {
      try
      {
        $node = new \SimpleXMLElement($z->readOuterXML());
      }
      catch (Exception $e)
      {
        p([[$e],[$node]]);
      }
      //get Current Book Title
      if(((String)$node->title->b203 !== "") && ((String)$node->b064!=''))
      { 
      $title = (String) $node->title->b203;
      //getting product EAN13
      $pi = null;
      $ingProductId = null;
      foreach ($node->productidentifier as $key => $identifier) 
      {
          $pi[$key] = (Array) $identifier;
          if($pi[$key]['b221'] == '03')
          {
            $ingProductId = (String) $pi[$key]['b244'];      
          }
      }
      if(!$ingProductId)
      {
        p($pi);
      }
      //Getting Author details
      $contributor = (Array) $node->contributor;
      $arrayValues = array_values($contributor);
      if(in_array('A01', $arrayValues))
      {
        $name = Null;
        $keyName = Null;
        $lastName = Null;
        $salutation = Null;
        foreach($contributor as $contKey => $contValue)
        {
          if($contKey == 'b039')
          {
            $firstName = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b038') 
          {
            $salutation = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b047')
          {
            $firstName = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b040')
          {
            if(isset($firstName))
            {
              $keyName = StringHelper::generateSlug($firstName."_".str_replace("'","\'",$contValue));
            }
            else
            {
              $firstName = '';
              $keyName = StringHelper::generateSlug(str_replace("'","\'",$contValue));
            }
            
            if($lastName == Null)
            {
              $lastName = str_replace("'","\'",$contValue);
            }
          }
          elseif($contKey == 'b041')
          {
            $lastName = str_replace("'","\'",$contValue);
          }
        }
        if($keyName == '')
        {
          $keyName = $firstName."_".$lastName;
        }

        if($keyName == '_')
        {
          p($contributor);
        }

      
      $relations[$i] = [$keyName,$ingProductId];
        
        if($i%5000 == 0)
        {
          try
          {
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $values = "";
            foreach ($relations as $relation) 
            {
              $relation[] = $date;
              $values .= "('".implode("','",$relation)."')";
              if($loop !== 5000)
              {
                $values .= ",";
              }
              $loop++;
            }
          $command = "INSERT INTO `products_author` (`author_key`,`ing_product_id`,`created_at`) VALUES ".$values." ON DUPLICATE KEY UPDATE `author_key`=`author_key`,`ing_product_id`=`ing_product_id`, `created_at`=`created_at`";
            $sql = \Yii::$app->db->createCommand($command)->execute();
            unset($relations);
            unset($contributor);
            unset($arrayValues);
            unset($values);

          }
          catch (Exception $e)
          {
            p([['exception' => $e],['at' => $i]]);
          }
        }
      $i++;
      }
      }//if productname = "" skipp
      $z->next('product');
    }
    $nextFileId = intval($fileId)+1;
    if(!empty($relations))
    {
      try
          {
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $count = count($relations);
            $values = "";
            foreach ($relations as $relation) 
            {
              $relation[] = $date;
              $values .= "('".implode("','",$relation)."')";
              if($loop !== $count)
              {
                $values .= ",";
              }
              $loop++;
            }
          $command = "INSERT INTO `products_author` (`author_key`,`ing_product_id`,`created_at`) VALUES ".$values." ON DUPLICATE KEY UPDATE `author_key`=`author_key`, `ing_product_id`=`ing_product_id`, `created_at`=`created_at`";
            $sql = \Yii::$app->db->createCommand($command)->execute();
            $relations = [];
          $this->redirect(['/mub-admin/csvreader/xml?product=read_data_'.$nextFileId]);
          }
          catch (Exception $e)
          {
            p([['exception' => $e],['at' => $i]]);
          }
    }
    $this->redirect(['/mub-admin/csvreader/xml?product=read_data_'.$nextFileId]);
  }

  public function actionReadAuthors($id)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    // $xmlProcess = new app\modules\MubAdmin\modules\csvreader\models\XmlProcess;
    $z = new \XMLReader;
    $z->open($path);
    while ($z->read() && $z->name !== 'product');
    $i = 0;
    $fields = ['title','f_name','l_name','key_name'];
    $authors = [];
    while ($z->name === 'product')
    {
      try{
        $node = new \SimpleXMLElement($z->readOuterXML());
      }
      catch (Exception $e){
        p([[$e],[$node]]);
      }
      if(((String)$node->title->b203 !== "") && ((String)$node->b064!=''))
      {
      $contributor = (Array) $node->contributor;
      $i = 0;
      $arrayValues = array_values($contributor);
      if(in_array('A01', $arrayValues))
      {
        $name = Null;
        $keyName = Null;
        $lastName = Null;
        $salutation = Null;
        $corporateName = Null;
        foreach($contributor as $contKey => $contValue)
        {
          if($contKey == 'b039')
          {
            $firstName = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b038') 
          {
            $salutation = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b047')
          {
            $firstName = str_replace("'","\'",$contValue);
          }
          elseif($contKey == 'b040')
          {
            if(isset($firstName))
            {
              $keyName = StringHelper::generateSlug($firstName."_".str_replace("'","\'",$contValue));
            }
            else
            {
              $firstName = '';
              $keyName = StringHelper::generateSlug(str_replace("'","\'",$contValue));
            }
            
            if($lastName == Null)
            {
              $lastName = str_replace("'","\'",$contValue);
            }
          }
          elseif($contKey == 'b041')
          {
            $lastName = str_replace("'","\'",$contValue);
          }
        }

        //if keyname is still blank it is corporate name
        
        if($keyName == '')
        {
          $keyName = $firstName."_".$lastName;
        }
        
        if($keyName != Null)
        {
            $authors[] = [$salutation,$firstName,$lastName,$keyName];
            $i++;
        }        
        
        if(((count($authors))%3000 == 0) && (!empty($authors)))
        {
          try
          {
            $loop = 1;
            $values = "";
            foreach ($authors as $authorDetails) 
            {
              $values .= "('".implode("','",$authorDetails)."')";
              if($loop !== 3000)
              {
                $values .= ",";
              }
              $loop++;
            }
          $sql = \Yii::$app->db->createCommand(
          "INSERT INTO `author` (`title`,`f_name`,`l_name`,`key_name`) VALUES ".$values."ON DUPLICATE KEY UPDATE `title`=`title`,`f_name`=`f_name`,`l_name`=`l_name`,`key_name`=`key_name`")->execute();
            $authors = [];
   
          }
          catch (Exception $e)
          {
            p([['exception' => $e],['at' => $i]]);
          }
        }
      }
      }
      $z->next('product');
    }
    try
        {
          if(!empty($authors))
          {
            $count = count($authors);
            $loop = 1;
            $values = "";
            foreach ($authors as $authorDetails) 
            {
              $values .= "('".implode("','",$authorDetails)."')";
              if($loop !== $count)
              {
                $values .= ",";
              }
              $loop++;
            }
            $sql = \Yii::$app->db->createCommand(
            "INSERT INTO `author` (`title`,`f_name`,`l_name`,`key_name`) VALUES ".$values."ON DUPLICATE KEY UPDATE `title`=`title`,`f_name`=`f_name`,`l_name`=`l_name`,`key_name`=`key_name`")->execute();
        $this->redirect(['/mub-admin/csvreader/xml?product=read_publishers_'.$fileId]);
         
          }
        }
        catch (Exception $e)
        {
            p([['exception' => $e],['at' => $i]]);
        }
  }


  public function actionReadPublishers($id)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    // $xmlProcess = new app\modules\MubAdmin\modules\csvreader\models\XmlProcess;
    $z = new \XMLReader;
    $z->open($path);
    while ($z->read() && $z->name !== 'product');
    $i = 1;
    $allImprints = [];
    while ($z->name === 'product')
    {
      try{
        $node = new \SimpleXMLElement($z->readOuterXML());
      }
      catch (Exception $e){
        p([[$e],[$node]]);
      }
      if(((String)$node->title->b203 !== "") && ((String)$node->b064!=''))
      {
      $imprint = (Array) $node->imprint;
      foreach ($imprint as $impKey => $impValue) 
      {
        if($impKey == 'b079')
        {
          $slug = StringHelper::generateSlug($impValue);
          $title = (String) $impValue;
          //handling case 
          $title = preg_replace('/^\s*\/\/<!\[CDATA\[([\s\S]*)\/\/\]\]>\s*\z/','$1',str_replace("'","\'",$title));
          $allImprints[] = [$title,$slug];          
        }
        else
        {
          p($impKey);
        }
      }
      if($i%10000 == 0)
      {
        $date = date('Y-m-d H:i:s');
        try
        {
          if(!empty($allImprints))
          {
            $loop = 1;
            $values = "";
            foreach ($allImprints as $imprint) 
            {
              $imprint[] = $date;
              $values .= "('".implode("','",$imprint)."')";
              if($loop !== 10000)
              {
                $values .= ",";
              }
              $loop++;
            }
            $sql = \Yii::$app->db->createCommand(
            "INSERT INTO `publisher` (`publisher`,`slug`,`created_at`) VALUES ".$values."ON DUPLICATE KEY UPDATE `publisher`=`publisher`,`slug`=`slug`,`created_at`=`created_at`")->execute();
            $allImprints = [];
            
          }
        }
        catch (Exception $e)
        {
            p([['exception' => $e],['at' => $i]]);
        }
      }
      $i++;
    }//publisher for book with BISAC and Title
      $z->next('product');
    }
      
    if(!empty($allImprints))
    {
      $count = count($allImprints);
        $date = date('Y-m-d H:i:s');
        try
        {
          $loop = 1;
          $values = "";
          foreach ($allImprints as $imprint) 
          {
            $imprint[] = $date;
            $values .= "('".implode("','",$imprint)."')";
            if($loop !== $count)
            {
              $values .= ",";
            }
            $loop++;
          }
          $sql = \Yii::$app->db->createCommand(
          "INSERT INTO `publisher` (`publisher`,`slug`,`created_at`) VALUES ".$values."ON DUPLICATE KEY UPDATE `publisher`=`publisher`,`slug`=`slug`,`created_at`=`created_at`")->execute();
           $allImprints = [];
          $this->redirect(['/mub-admin/csvreader/xml?product=link_publishers_'.$fileId]);
        }
        catch (Exception $e)
        {
            p([['exception' => $e],['at' => $i]]);
        }
    }    
  }

  public function actionLinkPublishers($id)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    // $xmlProcess = new app\modules\MubAdmin\modules\csvreader\models\XmlProcess;
    $z = new \XMLReader;
    $z->open($path);
    while ($z->read() && $z->name !== 'product');
    $i = 1;
    $allImprints = [];
    while ($z->name === 'product')
    {
      try{
        $node = new \SimpleXMLElement($z->readOuterXML());
      }
      catch (Exception $e){
        p([[$e],[$node]]);
      }
      if(((String)$node->title->b203 !== "") && ((String)$node->b064!=''))
      {
      //get publisher Details
      $title = (String) $node->title->b203;
      $imprint = (Array) $node->imprint;
      $impTitle = (String) $imprint['b079'];
      $pubSlug = StringHelper::generateSlug($impTitle);
     
      //Getting EAN13 for Items 
      $pi = null;
      $ingProductId = null;
      foreach ($node->productidentifier as $key => $identifier) 
      {
          $pi[$key] = (Array) $identifier;
          if($pi[$key]['b221'] == '03')
          {
            $ingProductId = (String) $pi[$key]['b244'];      
          }
      }
      if(!$ingProductId)
      {
        p($pi);
      }
      
      $relations[$i] = [$pubSlug,$ingProductId];
        if($i%5000 == '0')
        {
          try
          {
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $values = "";
            foreach ($relations as $relation) 
            {
              $relation[] = $date;
              $values .= "('".implode("','",$relation)."')";
              if($loop !== 5000)
              {
                $values .= ",";
              }
              $loop++;
            }

          $command = "INSERT INTO `publisher_products` (`publisher_key`,`ing_product_id`,`created_at`) VALUES ".$values." ON DUPLICATE KEY UPDATE `publisher_key`=`publisher_key`,`ing_product_id`=`ing_product_id`, `created_at`=`created_at`";
            $sql = \Yii::$app->db->createCommand($command)->execute();
            $relations = [];
          }
          catch (Exception $e)
          {
            p([['exception' => $e],['at' => $i]]);
          }
        }
      $i++;
      }//if No title for product found
      $z->next('product');
      }
     if(!empty($relations))
     {
      try
          {
            $date = date('Y-m-d H:i:s');
            $loop = 1;
            $count = count($relations);
            $values = "";
            foreach ($relations as $relation) 
            {
              $relation[] = $date;
              $values .= "('".implode("','",$relation)."')";
              if($loop !== $count)
              {
                $values .= ",";
              }
              $loop++;
            }
           $command = "INSERT INTO `publisher_products` (`publisher_key`,`ing_product_id`,`created_at`) VALUES ".$values." ON DUPLICATE KEY UPDATE `publisher_key`=`publisher_key`, `ing_product_id`=`ing_product_id`, `created_at`=`created_at`";
            $sql = \Yii::$app->db->createCommand($command)->execute();
            $relations = [];
          $this->redirect(['/mub-admin/csvreader/xml?product=link_authors_'.$fileId]);
          }
          catch (Exception $e)
          {
            p([['exception' => $e],['at' => $i]]);
          }
    }
    $this->redirect(['/mub-admin/csvreader/xml?product=link_authors_'.$fileId]);
  }

  public function actionReadPrice($id,$price=null,$product=null)
  {
    ini_set('max_execution_time', 200000);
    ini_set('memory_limit', '-1');
    $model = new XmlForm();
    $fileUploaded = new FileUploaded();
    $fileId = explode('_',$id)[2];
    $fileInstance = $fileUploaded::findOne($fileId);
    $path = $fileInstance->file_path;
    $model->file = $path;
    $handle = fopen("$path", "r");
      $i=0;
      $recordSet = NULL;

      if ($handle)
       {
          while (($line = fgets($handle)) !== false)
           {
             $Price = substr($line,151,6);
             $Weight = substr($line,239,6);
             $conPrice = substr($Price, -2);
             $conWeight = substr($Weight, -2);
             $addPrice = strlen($Price) > 4 ? substr($Price,0,4)."." : $Price;
             $addWeight = strlen($Weight) > 3 ? substr($Weight,0,3)."." : $Weight;
             $ingEan = substr($line,1,13);
             $ingTotalprice = floatval($addPrice.$conPrice);
             $ingPrice = ceil($ingTotalprice);
             $ingWeight = floatval($addWeight.$conWeight);
  
             $recordSet[] = [$ingEan,$ingPrice,$ingWeight]; 

               if(intval($i)==10000)
               {
                if(!empty($recordSet))
                  {
                  $count = count($recordSet);
                  $loop = 1;
                  $values = "";
                  foreach ($recordSet as $productPrice) 
                  {
                    $values .= "('".implode("','",$productPrice)."')";
                    if($loop !== $count)
                    {
                      $values .= ",";
                    }
                    $loop++;
                  }
                   $sql = \Yii::$app->db->createCommand("INSERT IGNORE INTO `price` (`ean_13`,`price`,`weight`) VALUES ".$values."ON DUPLICATE KEY UPDATE `ean_13`=`ean_13`,`price`=`price`,`weight`=`weight`")->execute();
                    echo "<script>console.log('added ".$sql." records')</script>";
                   }
                 }
                $i++;
               }

              if(!empty($recordSet))
              {
              $count = count($recordSet);
              $loop = 1;
              $values = "";
              foreach ($recordSet as $productPrice) 
              {
                $values .= "('".implode("','",$productPrice)."')";
                if($loop !== $count)
                {
                  $values .= ",";
                }
                $loop++;
              }
               $sql = \Yii::$app->db->createCommand("INSERT IGNORE INTO `price` (`ean_13`,`price`,`weight`) VALUES ".$values."ON DUPLICATE KEY UPDATE `ean_13`=`ean_13`,`price`=`price`,`weight`=`weight`")->execute();
                echo "<script>console.log('added ".$sql." records')</script>";
               }
               else{
                p("Reached else case of empty recorset");
               }
          fclose($handle);
          $this->redirect(['/mub-admin/csvreader/xml?price='.$fileId]);
      } 
      else
      {
          p("Dat file not found!");
      } 
  }
}
