<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use dosamigos\fileupload\FileUploadUI;
// use app\helpers\ImageUploader;
// use dosamigos\tinymce\TinyMce;
use limion\jqueryfileupload\JQueryFileUpload;
use app\modules\MubAdmin\modules\csvreader\models\XmlForm;
$xmlForm = new XmlForm();

?>
<div class="property-form">

    <?php $form = ActiveForm::begin(); ?>
    <h3>Product Upload</h3>
        <?= JQueryFileUpload::widget([
            'model' => $xmlForm,
            'attribute' => 'file',
            'url' => ['xml/file-upload'],
            'appearance'=>'ui', 
            'gallery'=>false, 
            'formId'=>$form->id,
            'options' => [
                'accept' => 'xml/*'
            ],
            'clientOptions' => [
                'maxFileSize' => 3000000000,
                'dataType' => 'json',
                'autoUpload'=>false
            ]
        ]);
    ?>
    <?php ActiveForm::end(); ?>

</div>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                    <tr class="headings">
                    <th>
                      <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </th>
                    <th class="column-title">Id</th>
                    <th class="column-title">File Name</th>
                    <th class="column-title">File Path</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                      <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($allFileUploaded as $value) { 
                 $newstring = substr($value['file_name'], -3);
                    if($newstring == 'xml'){ ?>

                  <tr class="even pointer">
                    <td class="a-center ">
                      <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" "><?= $value['id']; ?></td>
                    <td class=" "><?= $value['file_name']; ?></td>
                    <td class=" "><?= $value['file_path']; ?></td>
                    
                    <?php if($value['load_status'] == '0'){?>
                    <td class="last">
                        <button type="button" id="read_data_<?= $value['id']; ?>" class="btn btn-round btn-primary read_data">
                            Read Products
                        </button>
                    </td>
                    <?php }?>
                    <?php if($value['load_status'] == '1'){?>
                    <td class="last">
                        <button type="button" id="read_authors_<?= $value['id']; ?>" class="btn btn-round btn-success authors_read">
                            Read Authors
                        </button>
                    </td>
                    <?php }?>
                    <?php if($value['load_status'] == '2'){?>
                    <td class="last">
                        <button type="button" id="read_publishers_<?= $value['id']; ?>" class="btn btn-round btn-danger publishers_read">
                            Read Publishers
                        </button>
                    </td>
                    <?php }?>
                    <?php if($value['load_status'] == '3'){?>
                     <td class="last">
                        <button type="button" id="link_publishers_<?= $value['id']; ?>" class="btn btn-round btn-primary publishers_link">
                            Link Publishers
                        </button>
                    </td>
                    <?php }?>
                    <?php if($value['load_status'] == '4'){?>
                     <td class="last">
                        <button type="button" id="link_authors_<?= $value['id']; ?>" class="btn btn-round btn-success authors_link">
                            Link Authors
                        </button>
                    </td>
                    <?php }?>
                    <?php if($value['load_status'] == '5'){?>
                     <td class="last">
                        <button type="button" id="all_update_<?= $value['id']; ?>" class="btn btn-round btn-success ">
                            All Updated
                        </button>
                    </td>
                    <?php }?>
                  </tr>
                <?php } } ?>
                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>


<div class="property-form" style="margin-top: 13em;">
    <h3>Product Price Upload</h3>
        <?php $form = ActiveForm::begin(); ?>
            <?= JQueryFileUpload::widget([
            'model' => $xmlForm,
            'attribute' => 'file',
            'url' => ['xml/file-upload'],
            'appearance'=>'ui', 
            'gallery'=>false, 
            'formId'=>$form->id,
            'options' => [
                'accept' => 'dat/*'
            ],
            'clientOptions' => [
                'maxFileSize' => 3000000000,
                'dataType' => 'json',
                'autoUpload'=>false
            ]
            ]);?>
        <?php ActiveForm::end(); ?>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                    <tr class="headings">
                    <th>
                      <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </th>
                    <th class="column-title">User Id</th>
                    <th class="column-title">File Name</th>
                    <th class="column-title">File Path</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                    <th class="bulk-actions" colspan="7">
                      <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($allFileUploaded as $value) { 
                $newstring = substr($value['file_name'], -3);
                if($newstring == 'dat'){?>
                  <tr class="even pointer">
                    <td class="a-center ">
                      <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" "><?= $value['id']; ?></td>
                    <td class=" "><?= $value['file_name']; ?></td>
                    <td class=" "><?= $value['file_path']; ?></td>

                    <td class="last">
                        <button type="button" id="read_price_<?= $value['id']; ?>" class="btn btn-round btn-primary read_price">
                            Read Price
                        </button>
                       
                    </td>
                    <?php } } ?>
                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>
<?php if(\Yii::$app->request->getQueryParam('price') || \Yii::$app->request->getQueryParam('product')){
$this->registerJsFile('/js/automation.js',['depends' => [\yii\web\JqueryAsset::className()]]);
 }?>