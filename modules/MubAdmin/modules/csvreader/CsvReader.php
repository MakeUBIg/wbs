<?php

namespace app\modules\MubAdmin\modules\csvreader;

/**
 * csvreader module definition class
 */
class CsvReader extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\MubAdmin\modules\csvreader\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
