<?php

namespace app\modules\MubAdmin\modules\gallery;

class Gallery extends \app\modules\MubAdmin\MubAdmin
{   
    public $controllerNamespace = 'app\modules\MubAdmin\modules\gallery\controllers';
    
    public $defaultRoute = 'album';
    
    public function init()
    {
        parent::init();
    }
}