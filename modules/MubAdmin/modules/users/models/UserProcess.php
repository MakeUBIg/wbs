<?php 

namespace app\modules\MubAdmin\modules\users\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\models\User;
use app\models\MubUser;
use app\models\MubUserContact;
use yii\helpers\Url;

class UserProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $mubUser = new MubUser(); 
        $mubUserContacts = new MubUserContact();
        $mubUser->scenario = 'create_mub_user';
        $mubUserContacts->scenario = 'create_mub_user';
        $this->models = [
            'mubUser' => $mubUser,
            'mubUserContacts' => $mubUserContacts
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0'];
        $activeStates = $state->getAll('state_name',$where);
        return ['allStates' => $allStates,'activeStates' => $activeStates];
    }

    public function getRelatedModels($model)
    {
        $mubUser = $model;
        $mubUserContacts = $model->mubUserContacts;
        if(\Yii::$app->controller->action->id == 'update' || \Yii::$app->controller->action->id == 'view')
        {
            $mubUser->scenario = 'update_mub_user';
            $mubUserContacts->scenario = 'update_mub_user';
            $this->relatedModels = [
                'mubUser' => $mubUser,
                'mubUserContacts' => $mubUserContacts
            ];   
        }
        elseif(\Yii::$app->controller->action->id == 'delete')
        {
            $this->relatedModels = [
                'mubUser' => $mubUser,
                'mubUserContacts' => $mubUserContacts
            ];  
        }
        return $this->relatedModels;
    }

    private function saveUserData($mubUser)
    {
        if($mubUser->id)
        {
            $userModel = new User();
            $user = $userModel::findOne($mubUser->user_id);
        }
        else
        {
            $user = new User();
        }
        $user->first_name = $mubUser->first_name;
        $user->last_name = $mubUser->last_name;
        $user->username = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $mubUser->username));
        $user->password = $mubUser->password;
        $user->dob = ($mubUser->dob) ? $mubUser->dob : '1970-01-01 12:00:00';
        $user->gender = ($mubUser->gender) ? $mubUser->gender : 'Male';
        $user->status = ($mubUser->status) ? $mubUser->status : 'Inactive';
        $user->setPassword($mubUser->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if($user->save())
        {
            if(\Yii::$app->controller->action->id == 'create')
            {
                $auth = \Yii::$app->authManager;
                $subadmin = $auth->createRole('subadmin');
                $auth->assign($subadmin, $user->id);
            }
            $mubUser->user_id = $user->id;
            $mubUser->domain = ($mubUser->domain) ? $mubUser->domain : 'www.yourwebsite.com';
            $mubUser->status = ($mubUser->status) ? $mubUser->status : 'Inactive';
            $mubUser->organization = ($mubUser->organization) ? $mubUser->organization : "Your organization";
            $mubUser->username = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $mubUser->username));
            $mubUserContact = new \app\models\MubUserContact();
            if($mubUser->save())
            {
                return $mubUser->id;
            }
               throw new \yii\web\HttpException(500, 'User saved but not MubUser'); 
        }
        p($user->getErrors());
    }


    private function saveMubUserContact($mubUserContacts,$mubUserId)
    {
        $mubUserContacts->mub_user_id = $mubUserId;
        $mubUserContacts->city = ($mubUserContacts->city) ? $mubUserContacts->city : 125;
        $mubUserContacts->email = ($mubUserContacts->email) ? $mubUserContacts->email : 'youremail@domain.com';
        $mubUserContacts->pin_code = "110089";
        $mubUserContacts->landline = '023456789';
        $mubUserContacts->mobile = ($mubUserContacts->mobile) ? $mubUserContacts->mobile : '0987654321';
        $mubUserContacts->address = ($mubUserContacts->address) ? $mubUserContacts->address : 'Your complete address';
        return ($mubUserContacts->save()) ? $mubUserContacts->id : p($mubUserContacts->getErrors());
    } 

    public function saveData($data = [])
    {
        if (isset($data['mubUser'])&&
            isset($data['mubUserContacts']))
            {
            try {
                $mubUserId = $this->saveUserData($data['mubUser']);
                if ($mubUserId)
                {
                    $mubUserContact = $this->saveMubUserContact($data['mubUserContacts'],$mubUserId);
                    if($mubUserContact)
                    {
                        if(\Yii::$app->controller->action->id == 'create')
                            {
                            \Yii::$app->mailer->compose()
                            ->setFrom('admin@makeubig.com')
                            ->setTo($data['mubUserContacts']->email)
                            ->setSubject('Your Profile Created')                
                            ->setHtmlBody('<b>You have been invited by the OSMSTAYS Admin to add your property listings on OSMSTAYS PORTAL</b><br/><br/>
                                <i>Following are your login Credentials:</i><br /><b>Username : </b>'.$data['mubUser']->username.'<br /><b>Password : </b>'.$data['mubUser']->password.'<br /><b>Login URL : </b> <a target="_blank" href="'.Url::home(true).'mub-admin/'.'">Click Here</a><br />'
                                )
                            ->send();
                            }
                            return $mubUserId;
                        }  
                    }
                    else
                    {
                        throw new \yii\web\HttpException(500, 'User Contact Data not saved');
                    }
                } 
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}