<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $mubCategory app\mubCategorys\MubCategory */

$this->title = Yii::t('app', 'Update {mubCategoryClass}: ', [
    'mubCategoryClass' => 'Category',
]) . $mubCategory->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $mubCategory->id, 'url' => ['view', 'id' => $mubCategory->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mub-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'mubCategory' => $mubCategory,
    ]);?>

</div>