<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $mubCategory app\mubCategorys\MubCategory */
/* @var $form yii\widgets\ActiveForm */
    $mubCategories = $mubCategory::find()->where(['<>','del_status','1']);
    if(\Yii::$app->controller->action->id == 'update')
    {
       $id = \Yii::$app->request->get('id');
       $mubCategories->andWhere(['<>','id',$id]); 
    }
    $categories = $mubCategories->all();
    $category = [];
    foreach ($categories as $mubc) 
    {
        $category[$mubc->id] = $mubc->category_name;
    }
?>

<div class="mub-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($mubCategory, 'category_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubCategory, 'category_slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubCategory, 'category_parent')->dropDownList($category, ['prompt' => 'Select A Category']) ?>

    <div class="form-group">
        <?= Html::submitButton($mubCategory->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $mubCategory->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>