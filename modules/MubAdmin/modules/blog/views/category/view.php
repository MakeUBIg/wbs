<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MubCategory */

$this->title = $mubCategory->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $mubCategory->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $mubCategory->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $mubCategory,
        'attributes' => [
            'id',
            'category_name',
            'category_slug',
            'category_parent',
            'category_child',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>