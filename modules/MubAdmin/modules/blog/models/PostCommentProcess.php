<?php 
namespace app\modules\MubAdmin\modules\blog\models;

use app\models\PostComment;
use app\models\Post;
use app\components\Model;

class PostCommentProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $postComment = new PostComment();
        $postComment->scenario = 'create_comment';
        $this->models = [
            'postComment' => $postComment
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $post = new Post();
        $posts = $post->getAll('post_title');
        return ['posts' => $posts];
    }

    public function getRelatedModels($model)
    {
        $postComment = $model;
        $postComment->scenario = 'update_comment';
        $this->relatedModels = [
            'postComment' => $postComment
        ];
        return $this->relatedModels;
    }

    public function saveComment($postComment)
    {
        if(\Yii::$app->user)
        {
            $user = new \app\models\MubUser();
            $post = new \app\models\Post();
            $currentPost = $post::find()->where(['del_status' => '0','url' => $postComment])->one();
            $currentUser = $user::find()->where(['user_id' => \Yii::$app->user->id])->one();
            $postComment->post_id = $post->id;
            $userContact = $currentUser->mubUserContacts;
            $postComment->name = $currentUser->first_name.' '.$currentUser->last_name;
            $postComment->email = $userContact->email;
            $postComment->mub_user_id = $currentUser->id;
            $postComment->comment_id = '';
            return ($postComment->save()) ? $postComment->id : p($postComment->getErrors());
        }
    	return ($postComment->save()) ? $postComment->id : p($postComment->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['postComment']))
        {
        try {
        	$commentId = $this->saveComment($data['postComment']);
        	return ($commentId) ? $commentId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
