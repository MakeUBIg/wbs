<?php

namespace app\modules\MubAdmin\modules\blog\controllers;

use Yii;
use app\models\Post;
use app\models\PostSearch;
use app\components\MubController;
use app\modules\MubAdmin\modules\blog\models\BlogProcess;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class BlogitemController extends MubController
{
   public function getPrimaryModel()
   {
        return new Post();
   }

   public function getProcessModel()
   {
        return new BlogProcess();
   }

   public function getSearchModel()
   {
        return new PostSearch();
   }
   public function actionProfile()
    {   
        if (\Yii::$app->user->isGuest) {
            $this->redirect('/mub-admin');
        }
        else 
        {
            if(\Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/mub-admin/blog/blogitem/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }


}
