<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\MubUser;
use app\models\MubUserSearch;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;


class DashboardController extends MubController
{
	public function getProcessModel()
	{
        return new BlogProcess();
	}

	public function getPrimaryModel()
	{
        return new MubUser();
	}

    public function getSearchModel()
    {
        return new MubUserSearch();
    }

	public function actionImageUpload()
	{
		$file = \Yii::$app->getBodyParams();
		p($file);
	}

	public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function saveBookingDetails($postData)
    {
         $cart = new ShoppingCart();
         $cartItems = ArrayHelper::toArray($cart->getPositions());
         $price = $cart->getCost();
        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->name = $postData['billing_name'];
            $booking->mub_user_id = $mubUserId;
            $booking->address = $postData['billing_address'];
            $booking->city = $postData['billing_city'];
            $booking->country = $postData['billing_country'];
            $booking->state = $postData['billing_state'];
            $booking->pin_code = $postData['billing_zip'];
            $booking->lat = $postData['lat'];
            $booking->long = $postData['long'];
            $booking->alt_address = $postData['billing_address'];
            $booking->email = $postData['billing_email'];
            $booking->mobile = $postData['billing_tel'];
            $booking->status = 'Order';
            $booking->time = $postData['time'];
            $booking->amount = $postData['amount'];
            $booking->booking_item = $postData['item'];
            $booking->booking_item = $postData['quantity'];

            if(!$booking->save())
            {
               p($booking->getErrors());
            }
            $itemName = [];
            foreach ($cartItems as $value) 
            {
                $itemName[] = $value['product_name'];
            }

           $success = \Yii::$app->mailer->compose('Listhomemail',['cartItems' => $cartItems, 'price' => $price])
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send();    

                return $success;
        }
        return true;
    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            $postData = \Yii::$app->request->getBodyParams();
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($postData[$hash_var]) ? $postData[$hash_var] : '';
                $hash_string .= '|';
            }
            //add merchant salt here
          $hash_string .= 'AJi6ZxOUP2';
          $result['hash'] = strtolower(hash('sha512', $hash_string));
          $this->saveBookingDetails($postData);
          return $result;
        }
    }

}