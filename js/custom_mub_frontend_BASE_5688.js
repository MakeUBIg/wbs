
$(document).ready(function(){
	$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
var myBgFader = $('.header').bgfader([
  '/images/qwety.jpg',
  '/images/qwertyuiop.jpg',
  '/images/poiuytrewq.jpg',
], {
  'timeout': 2000,
  'speed': 2000,
  'opacity': 0.4
})

myBgFader.start()
});

$("#contact-form").on('beforeSubmit',function(e){
	e.preventDefault();
  var form = $('#contact-form').serialize();
	$.ajax({
       type:'POST',
       url:"/site/contact",
       data:form,
       success: function(result)
        {
        	if(result == '1')
        	{
        		$('#contact-form').append('<h4>Your message has been sent.</h4>');
        		location.reload();	
        	}
        }
  	});
    return false;
});
$(document).on("click", "#confirmPrice", function(event){
     $.ajax({
         type:'GET',
         url:"/site/modal",
         success: function(result)
          {
            $('#dynamic-modal').html(result);
          }
      });
});
// $("#calculate").on('beforeSubmit',function(e){
//   e.preventDefault();
//   if($('#calculate').find('.has-error').length)
//   {
//     return false;
//   }
//   else
//   {
//   var form = $('#calculate').serialize();
//   $.ajax({
//      type:'POST',
//      url:"/site/result",
//      data:form,
//      success: function(result)
//       {
//         if(result)
//         {
//            $('#result').html(result);
//         }
//         else
//         {
//           $('#calculator-ins').html(result);
//         }
//       }
//     });
//   }
//     return false;
// });
