
$(document).ready(function(){
	$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
var myBgFader = $('.header').bgfader([
  '/images/qwety.jpg',
  '/images/qwertyuiop.jpg',
  '/images/poiuytrewq.jpg',
], {
  'timeout': 2000,
  'speed': 2000,
  'opacity': 0.4
})

myBgFader.start()
});

$("#contact-form").on('beforeSubmit',function(e){
	e.preventDefault();
  var form = $('#contact-form').serialize();
	$.ajax({
       type:'POST',
       url:"/site/contact",
       data:form,
       success: function(result)
        {
        	if(result == '1')
        	{
        		$('#contact-form').append('<h4>Your message has been sent.</h4>');
        		location.reload();	
        	}
        }
  	});
    return false;
});
$(document).on("click", "#confirmPrice", function(event){
     $.ajax({
         type:'GET',
         url:"/site/modal",
         success: function(result)
          {
            $('#dynamic-modal').html(result);
          }
      });
});
$(document).ready(function() {

    $(".side_button").click(function(){       
         jQuery('#search_record').toggleClass('display_filter');
    });
     

    $('.restaurant').on('change', function() {
        $getValue = $('.restaurant').not(this).prop('checked', false);
    
        var value = $(this).val();

        $.ajax({
         type:'GET',
         url:"/site/get-items",
         data:{menuId:value},
         success: function(result)
            {   
                $('#dynamic-div').html(result);
            }
        });

     });

});

$(document).on("click", "#forget", function(event){
     $.ajax({
         type:'GET',
         url:"/site/forgetpass",
         success: function(result)
          {     
            $('#dynamic-modal').html(result);
          }
      });
});

$(document).on("click", ".btn-addcart", function(event){
    var itemId = $(this).attr('id');
    var id = itemId.replace('add-cart_','');
    var checkoutBtn = '#checkout_'+id;
     $.ajax({
         type:'GET',
         url:"/site/addtocart",
         data:{'id':id},
         success: function(result)
            {   
                location.reload();
                $('#cart-items').html(result);
                $('#'+itemId).addClass('check');
                $(checkoutBtn).removeClass('check');
                var newPrice = $('#total-price').val();
                $('#display-price').text(newPrice);
            }
        });
});

$(document).on("click", ".btn-remove-item", function(event){

    var itemId = $(this).attr('id');
    var myString = itemId.replace('remove_item_','');
    $.ajax({
         type:'GET',
         url:"/site/clear-cart",
         data:{'id':myString},
         success: function(result)
            {   
                $('#cart-items').html(result);
                var newPrice = $('#total-price').val();
                $('#display-price').text(newPrice);
                location.reload();
            }
        });
});

$(document).on("click", ".incrementcart", function(event){
    var incId =  $(this).attr('id');
    var id = incId.replace('inc_','');
    var incQuantity = $('#mgQuant_'+id).text();
    incQuantity++;
    $('#mgQuant_'+id).text(incQuantity);
});

$(document).on("click", ".decrementcart", function(event){
    var decId =  $(this).attr('id');
    var id = decId.replace('dec_','');
    var decQuantity = $('#mgQuant_'+id).text();
    decQuantity--;
    if (decQuantity == 0) {
        return true;
      }
    $('#mgQuant_'+id).text(decQuantity);
});

$(document).on("click", ".btn-updateaddcart", function(event){
    var itemId = $(this).attr('id');
    var id = itemId.replace('add-cart_','');
    var quantity = $('#mgQuant_'+id).text();

     $.ajax({
         type:'GET',
         url:"/site/update-cart-items",
         data:{'id':id, 'quantity':quantity},
         success: function(result)
            {
                location.reload();
            }
        });
});

function updateCartQuantity(){
    $.ajax({
         type:'GET',
         url:"/site/cartcount",
         success: function(result)
            {
                $(".cartcount").text(result);
            }
        });
}