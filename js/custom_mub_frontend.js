
$(document).on("submit", "#checkout-signin", function(e){
     e.preventDefault();
     var form = $('#checkout-signin').serialize();
     $.ajax({
         type:'POST',
         url:"/site/checkout-login",
         data:form,
         success: function(result)
            {
                $('#checkout-signin').html(result);
            }
        });
     return false;
});

$("#contact-form").on('beforeSubmit',function(e){
	e.preventDefault();
  var form = $('#contact-form').serialize();
	$.ajax({
       type:'POST',
       url:"/site/contact",
       data:form,
       success: function(result)
        {
        	if(result == '1')
        	{
        		$('#contact-form').append('<h4>Your message has been sent.</h4>');
        		location.reload();	
        	}
        }
  	});
    return false;
});
$(document).on("click", "#confirmPrice", function(event){
     $.ajax({
         type:'GET',
         url:"/site/modal",
         success: function(result)
          {
            $('#dynamic-modal').html(result);
          }
      });
});

$(document).ready(function() {

    $(".side_button").click(function(){       
         jQuery('#search_record').toggleClass('display_filter');
    });
     

    $('.restaurant').on('change', function() {
        $getValue = $('.restaurant').not(this).prop('checked', false);
    
        var value = $(this).val();

        $.ajax({
         type:'GET',
         url:"/site/get-items",
         data:{menuId:value},
         success: function(result)
            {   
                $('#dynamic-div').html(result);
            }
        });

     });

});

$(document).on("submit", "#frontend-signin", function(e){
     e.preventDefault();
     var form = $('#frontend-signin').serialize();
     $.ajax({
         type:'POST',
         url:"/site/client-login",
         data:form,
         success: function(result)
            {
                $('#dynamic-modal').html(result);
            }
        });
     return false;
});


$(document).on("click", "#forget", function(event){
     $.ajax({
         type:'GET',
         url:"/site/forgetpass",
         success: function(result)
            {       
                $('#dynamic-modal').html(result);
            }
        });
});

$(document).on("submit", "#frontend-forget", function(e){
     e.preventDefault();
     var form = $('#frontend-forget').serialize();
     $.ajax({
         type:'POST',
         url:"/site/forgetpass",
         data:form,
         success: function(result)
            {
                if(result === 'mailsent')
                {
                    $('#frontend-forget').html('<p style="text-align:center; padding-top: 2em;color:#ebc850; font-size:18px;line-height:35px;">You will Receive the Password <br/>for this Account on Your <br/>Registered Mail ID.</p>');
                }
                else
                {
                    $('#clientsignup-email').parent().addClass('has-error');
                    $('.help-block-error').html('<p style="font-size:18px; color:#ebc850; margin-top:5px; margin-bottom:-1em;">Please Enter Registered Email</p>');
                }
            }
        });
     return false;
});

$(document).on("click", ".btn-addcart", function(event){
    var itemId = $(this).attr('id');
    var id = itemId.replace('add-cart_','');
    var checkoutBtn = '#checkout_'+id;
     $.ajax({
         type:'GET',
         url:"/site/addtocart",
         data:{'id':id},
         success: function(result)
            {   
                location.reload();
                $('#cart-items').html(result);
                $('#'+itemId).addClass('check');
                $(checkoutBtn).removeClass('check');
                var newPrice = $('#total-price').val();
                $('#display-price').text(newPrice);
            }
        });
});

$(document).on("click", ".btn-remove-item", function(event){

    var itemId = $(this).attr('id');
    var myString = itemId.replace('remove_item_','');
    $.ajax({
         type:'GET',
         url:"/site/clear-cart",
         data:{'id':myString},
         success: function(result)
            {   
                $('#cart-items').html(result);
                var newPrice = $('#total-price').val();
                $('#display-price').text(newPrice);
                location.reload();
            }
        });
});

$(document).on("click", ".incrementcart", function(event){
    var incId =  $(this).attr('id');
    var id = incId.replace('inc_','');
    var incQuantity = $('#mgQuant_'+id).text();
    incQuantity++;
    $('#mgQuant_'+id).text(incQuantity);
});

$(document).on("click", ".decrementcart", function(event){
    var decId =  $(this).attr('id');
    var id = decId.replace('dec_','');
    var decQuantity = $('#mgQuant_'+id).text();
    decQuantity--;
    if (decQuantity == 0) {
        return true;
      }
    $('#mgQuant_'+id).text(decQuantity);
});

$(document).on("click", ".btn-updateaddcart", function(event){
    var itemId = $(this).attr('id');
    var id = itemId.replace('add-cart_','');
    var quantity = $('#mgQuant_'+id).text();

     $.ajax({
         type:'GET',
         url:"/site/update-cart-items",
         data:{'id':id, 'quantity':quantity},
         success: function(result)
            {
                location.reload();
            }
        });
});

function updateCartQuantity(){
    $.ajax({
         type:'GET',
         url:"/site/cartcount",
         success: function(result)
            {
                $(".cartcount").text(result);
            }
        });
}

$(".sortby").on("click",function(event){
    var sortId = this.id;
    var categoryId = $('#categoryId').val();
    $.ajax({
         type:'GET',
         url:"/site/get-sorted-result",
         data:{'sortId':sortId,'categoryId':categoryId},
         success: function(result)
            {   
               $('#'+sortId+'_tab').html(result);
               var heading = $('#pageHeading').text();
            }
        });
});

    
$(document).ready(function(){
     $('[data-toggle="tooltip"]').tooltip(); 
    $('#paypal-drop-div').hide();
$("#label-ship").click(function(){
    $('.shipping-address-form').toggle();
});

$("#paypal-click-con02").click(function(){
    $('#paypal-drop-div').show();
});
$('#paypal-click-con01,#paypal-click-con03,#paypal-click-con04').click(function(){
    $('#paypal-drop-div').hide();
    });
$('#continue-button01').click(function(e){
    e.preventDefault();
    $('.shipping-grid-outer02').removeClass('shipping-opacity-div');
});
$('#continue-button02').click(function(e){
    e.preventDefault();
    $('.shipping-grid-outer03').removeClass('shipping-opacity-div');
});

});

//get the click evenyt on the data toggle modal
$("a[data-toggle=modal]").click(function(ev) {
    ev.preventDefault();
    var target = $(this).attr("href");
    // load the url and show modal on success
    $("#myModal .modal-content").load(target, function() { 
         $("#myModal").modal("show"); 
    });
});


 $(document).ready(function(){
        $("input[type='button']").click(function(){
            var radioValue = $("input[name='cc']:checked").val();
            if(radioValue == 'guest'){
                window.location = "/site/payments";
            } 
            else
            {
               $('#signup-modal').click();
            }
        });
        
    });


    $(document).ready(function() {

  $(".moreboxes-section").hide(); 
$(".moreboxes-section_btn").click(function(){
    $('.moreboxes-section').slideToggle();
    $('.moreboxes-section_btn').html($('.moreboxes-section_btn').text() == 'Less' ? 'View More' : 'Less');     
});

$(".moreboxes-section02").hide(); 
$(".moreboxes-section_btn02").click(function(){
    $('.moreboxes-section02').slideToggle();
    $('.moreboxes-section_btn02').html($('.moreboxes-section_btn02').text() == 'Less' ? 'View More' : 'Less');     
});

$(".moreboxes-section03").hide(); 
$(".moreboxes-section_btn03").click(function(){
    $('.moreboxes-section03').slideToggle();
    $('.moreboxes-section_btn03').html($('.moreboxes-section_btn03').text() == 'Less' ? 'View More' : 'Less');     
});


$(".more_content").click(function(){
      $(this).hide();
    $('.minus_content').show();
    $(".more_content_show").toggle();
});

  $(".minus_content").click(function(){
    $(this).hide();
    $('.more_content').show();
    $(".more_content_show").hide();
});

$(".national-click-tab").click(function(){
$('#national-tab').show();
$("#international-tab").hide();
});

$(".international-click-tab").click(function(){
$('#national-tab').hide();
$("#international-tab").show();
});

});
    $(function(){
    $('.product-nation-main-tabs-panel li a').click(function(){
    $('.product-nation-main-tabs-panel li a').removeClass('product-nation-main-tabs-active '); // remove the class from the currently selected
    $(this).addClass('product-nation-main-tabs-active '); // add the class to the newly clicked link
    if(!$(this).addClass('product-nation-main-tabs-active ')) {
    } else {
    $(this).addClass('product-nation-main-tabs-active ');
    $(this).next().stop(true,true).removeClass('product-nation-main-tabs-active ');
    }
    });
    
    $(function(){
    $('.product-bottom-tabs-main-outer li a').click(function(){
    $('.product-bottom-tabs-main-outer li a').removeClass('product-bottom-tabs-active '); // remove the class from the currently selected
    $(this).addClass('product-bottom-tabs-active '); // add the class to the newly clicked link
    if(!$(this).addClass('product-bottom-tabs-active')) {
    } else {
    $(this).addClass('product-bottom-tabs-active ');
    $(this).next().stop(true,true).removeClass('product-bottom-tabs-active');
    }
    });
    
    
    
    
    });
        
        $('.country-select-con select').change(function(){
            $(".country-tab").show();
        });
    });


$('#search-restaurant').on('click',function(){
    var item = $('#exampleInputAmount').val();
    
    if(item === '')
    {
        $('#error-search-field').text('Please enter a Dish Name');
        
        return false;
    }
    $('#categorie-search-form').submit();
});

$('#confirm-payment').on('beforeSubmit',function(e){
    var amount = $('#booking-total').val();  
    var checked = $('input[type=checkbox]').prop('checked');
     if(!checked)
     {
        var firstname = $('#shipFirstName').val();
        $('#first_name').val(firstname);
        var lastname = $('#shipLastName').val();
        $('#last_name').val(lastname);
        var email = $('#shipEmail').val();
        $('#email').val(email);
        var mobile = $('#shipMobile').val();
        $('#mobile').val(mobile);
        var city = $('#shipCity').val();
        $('#city').val(city);
        var state = $('#shipsState').val();
        $('#state').val(state);
        var country = $('#shipCountry').val();
        $('#country').val(country);
        var pincode = $('#shipPincode').val();
        $('#pin_code').val(pincode);
        var address = $('#shipAddress').val();
        $('#address').val(address);
     }

    var form  = $('#confirm-payment').serialize();
    $.ajax({
     type:'POST',
     url:"/mub-admin/dashboard/payu-hash",
     data:form,
     async:false,
     success: function(result)
        {
        $('<input />').attr('type', 'hidden')
          .attr('name', "hash")
          .attr('value', result.hash)
          .appendTo('#confirm-payment');      
        }
        });
    return true;
});

$('#confirm-payment').bind('submit', function (e) {
    var button = $('#send');

    // Disable the submit button while evaluating if the form should be submitted
    button.prop('disabled', true);

    var valid = true;    

    // Do stuff (validations, etc) here and set
    // "valid" to false if the validation fails

    if (!valid) { 
        // Prevent form from submitting if validation failed
        e.preventDefault();

        // Reactivate the button if the form was not submitted
        button.prop('disabled', false);
    }
});
 jQuery(document).ready(function() {
    var offset = 220;
    var duration = 500;
    jQuery(window).scroll(function() {
      if (jQuery(this).scrollTop() > offset) {
        jQuery('.back-to-top').fadeIn(duration);
      } else {
        jQuery('.back-to-top').fadeOut(duration);
      }
    });
    
    jQuery('.back-to-top').click(function(event) {
      event.preventDefault();
      jQuery('html, body').animate({scrollTop: 0}, duration);
      return false;
    })
  });