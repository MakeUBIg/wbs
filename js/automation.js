var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

if(getUrlParameter('price'))
{

var id = getUrlParameter('price');
var newId = parseInt(id)+1;
var newElement = "read_price_"+newId;
setTimeout(function(){ 
    var elementExists = document.getElementById(newElement);
        if(elementExists)
        {
            elementExists.click();
        }
        else
        {
            alert("could not find element " + newElement);
        }
},3000);
}
else if(getUrlParameter('product'))
{
    var id = getUrlParameter('product');

    setTimeout(function(){ 
    var elementExists = document.getElementById(id);
        if(elementExists)
        {
            elementExists.click();
        }
        else
        {
            alert("could not find element " + id);
        }
},3000);
}
