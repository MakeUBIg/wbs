$(document).ready(function(){
	$('#mub-state').on('change',function(){
    var selected = $("#mub-state option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/users/user/get-city",
         data:{stateId:selected},
        success: function(result){
            $('#mubusercontact-city').children("option").remove();
            $('#mubusercontact-city').prop("disabled", false);
            $('#mubusercontact-city').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
            $('#mubusercontact-city').append(new Option(item.city_name, item.id));
        });}
    }); 
 });

function changeDataLoadStatus(context)
{
   var id = $(context).attr('id');
   $.ajax({
        url:'/mub-admin/csvreader/xml/change-data-load-status',
        dataType:'json',
        type:'get',
        cache:true,
        data: {
          id: id
        },
        success:  function (response) 
        {
          location.reload();
            if(response == 'success')
            {
              alert('All Records Entered');
            }
        },              
    });
}

$('.read_data').click(function(){
    changeDataLoadStatus(this);
    var id = $(this).attr('id');
      this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/read-uploaded-file',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

$('.publishers_read').click(function(){
    changeDataLoadStatus(this);
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/read-publishers',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

$('.authors_read').click(function(){
    changeDataLoadStatus(this);
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/read-authors',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

$('.publishers_link').click(function(){
    changeDataLoadStatus(this);
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/link-publishers',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

$('.authors_link').click(function(){
    changeDataLoadStatus(this);
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/link-authors',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

  $('.read_price').click(function(){
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/read-price',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });

   $('.link_price').click(function(){
    var id = $(this).attr('id');
    this.disabled = true;
         $.ajax({
              url:'/mub-admin/csvreader/xml/link-price',
              dataType:'json',
              type:'get',
              cache:true,
              data: {
                id: id
              },
              success:  function (response) 
              {
                location.reload();
                  if(response == 'success')
                  {
                    alert('All Records Entered');
                  }
              },              
      });
  });


var approveComment = function(commentId)
{
    $.ajax({
         type:'POST',
         url:"/mub-admin/blog/comments/approve-comment",
         data:{commentId:commentId},
        success: function(result)
        {
            if(result)
            {
                alert('Selected Comment has been approved');
            }
            else
            {
               alert('Comment could not be approced by you');
             }
        }
    }); 
}
var setModelAttribute = function(model,attribute,value,id)
{
    var dataArray = {model:model,attribute:attribute,value:value,id:id};
    $.ajax({
     type:'POST',
     url:"/mub-admin/dashboard/set-attribute",
     data:dataArray,
        success: function(result){
            if(result)
            {
            }
        }
    }); 

};

$(document).on("submit", "#sql-query", function(e){
     e.preventDefault();
     var query = $('#sql').val();
     $.ajax({
         type:'POST',
         url:"/site/sql-query",
         data:{query:query},
         success: function(result)
            {
                alert(result);
                return result;
            }
        });
     return false;
});

var changeUserStatus = function(field,val,id)
{
    var models = [{name:'\\app\\models\\User',status:val},{name:'\\app\\models\\MubUser',status:val}];
    for (var i = models.length - 1; i >= 0; i--) {
      setModelAttribute(models[i].name,'status',val,id);  
    }
};

});